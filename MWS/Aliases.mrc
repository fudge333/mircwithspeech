;aliases.mrc

alias cmode .mode # $1-

alias dop mode # -ooo $$1 $2 $3

alias op mode # +ooo $$1 $2 $3

alias voice mode # +vvv $$1-

alias DVoice mode # -vvv $$1-

alias cs /msg chanserv $1 # $2-

alias names names $iif(!$1-,$chan,$1-)

alias n !names $iif(!$1,$chan,$1)

alias j join #$$1 $2-

alias p part #
alias w whois $$1 $$1

alias k kick # $$1 $2-

alias q /query $$1

alias send dcc send $1 $2- 

alias chat /dcc chat $1

alias ping /ctcp $$1 ping

alias s /server $$1-

alias i if (# == $chan) .invite $1 $chan 

alias v { 
  .CTCP $1 VERSION 
  echo -catblfn  ctcp $Brackets1(Requesting CTCP VERSION from $1) 
}

alias speech { 
  echo -catblfn CTCP $Brackets1($iif(!$1,You need to specify a nickname to request speech status from,Requesting speech status from $1)) 
  .ctcp $$1 SPEECH
}

alias GI {
  echo -catblfn CTCP $Brackets1(This command is debricated, please use /speech $iif($1,$1))) 
}

alias time {
  if ($1) .ctcp $$1 TIME
  else !time
}

alias google .run http://google.com/search?hl=en&q= $+ $1- $+ &btnG=Google+Search

alias f2 {
  .clear 
  Talk Window text cleared
}

alias sf2 {
  .clearAll 
  Talk  all window text cleared
}

alias f3 {
  if ($hGet(General,ChannelInvJoin)) .join -i
  else echo -catblrfm Inv * You have not been invited to any channels
}

alias max showmirc -x

alias RawPing {
  set -eu10 %lag $ticks 
  raw -q ping $server
}

alias join { 
  var %InvJoin $hGet(General,ChannelInvJoin)
  if (($$1- == -i) && (%invJoin)) { 
    scid %InvJoin /!join -i 
    hDel General ChannelInvjoin 
    else !join $2-
  } 
  else /!join $$1- 
} 

alias brackets1 return $+($chr(91),$1-,$chr(93))

alias Brackets2 return $+($chr(171),$1-,$chr(187),$chr(15))

alias Brackets3 return $+($chr(124),$1-)

alias DayTime {
  var %DayTime $asctime($iif($1,$1,$asctime),ht)
  return $iif($right(%DayTime,1) == a,morning,$iif($left(%DayTime,1) >= 6,evening,afternoon))
} 

alias year {
  return $iif($asctime($1,yyyy) != $asctime(yyyy),$asctime($1,yyyy)) 
}

alias char return $chr(164)

alias quits {
  if (ping isin $1-) return Pings
  elseif (peer isin $1-) return Peers
  else return Quits
}

alias idle { 
  hAdd General $+(IdleTime,_,$network,_,$iif($1,$1,$me))) $+($network,_,$iif($1,$1,$me)))
  echo -catblfrn WH  Starting $iif($1,$1,$me) Idle report 
  .whois $iif($1,$1,$me) 
}
alias IdleCheck {
  return $hGet(general,$+(IdleTime,_,$network,_,$1))
}

alias all scon -at1 amsg * mass msg - $$1-

alias url say $url

alias ts return $+($chr(03),4,[,$chr(03),14,h,$chr(03),4,:,$chr(03),14,nn,$chr(03),4 tt],$chr(15))
alias tspanel return $+([,$time(h:nn tt),])

alias readmrc return $gettok($readini($mircini,$iif(($3 == $null),options,$3),$1),$2,44) 

alias readEvents return $gettok($readini(mirc.ini,$iif(($3 == $null),events,$3),default),$1,44) 

;Talking part of the script, do not adjust these aliases unless you know what you're doing
alias echo {
  ;Check for content first
  if ($$1) {
    /!echo $$1-
  }
  if (($hGet(Speech,NoSpeech)) || (!$hGet(Speech,Software))) return
  var %ct $chantypes
  if ($left($1,1) == -) {
    if ((s isin $1) || (a isin $1)) Talk $iif(c isin $1,$3-,$2-)
    elseif (((c isin $1) && (a !isin $1) && (s !isin $1))) Talk $iif($left($3,1) isin %ct,$4-,$3-))),$2-)
    else Talk $iif($left($2,1) isin %ct,$3-,$2-)
  }
  else Talk $iif($left($1,1) isin %ct,$2-,$1-)
} 
alias Talk {
  if (($hGet(Speech,NoSpeech)) || (!$hGet(Speech,Software))) halt
  if ((($hGet(Speech,Speech)) &&  (!$hGet(Speech,$network)) && (!$hGet(Speech,$+($network,_,$chan))))) {
    var %text $strip($1-)
    if ($hGet(Speech,Software) == JAWS) return $com(jaws,saystring,3,bstr,%text,int,0)
    if ($hGet(Speech,Software) == NVDA) dll nvda.dll speak %text 
    ;$+(",$mircdirMWS\DLLs\nvda.dll,")
    if ($hGet(Speech,Software) == WINEYES) return $com(jaws,SpeakString,1,string,%text) 
  }  
}

alias f5 .doSpeak -s on

alias f6 .doSpeak -s off

alias cf5 .doSpeak -c on

alias cf6 .doSpeak -c off

alias sf5 .doSpeak -n on

alias sf6 .doSpeak -n off

alias doSpeak {
  if ((!%choose.tts) && ($hGet(Speech,NoSpeech))) halt
  if (!$hGet(Speech,Software)) {
    var %NVDA $?!"No Speech software has been specified. $crlf $+ If you use screen reading software, is it NVDA?" 
    if (%NVDA == $false) var %JAWS $?!"Are you using JAWS?"      
    elseif (%Jaws == $false) var %WinEyes $?!"Are you using Window Eyes?"      
    if (%NVDA) hAdd Speech Software NVDA
    elseif (%JAWS) hAdd Speech Software JAWS
    elseif (%WINEYES) hAdd Speech Software WINEYES
    elseif (((!%Jaws) && (!%WinEyes) && (!%NVDA))) hAdd Speech NoSpeech 1
    if (!$hGet(Speech,NoSpeech)) hAdd Speech Speech 1
    echo -cast other $iif($hGet(Speech,NoSpeech),No speech engine,$hGet(Speech,Software)) selected for speech output
    f5
    halt
  }

  ;JAWS error checking
  if (($hGet(Speech,Software) == JAWS) && ($exists($+(",$mircdirMWS\DLLs\,jfwAPI.dll")))) {
    if ($com(jaws).progid != jfwapi) {
      if ($com(jaws).progid == gwspeak.speak) comclose jaws gwspeak.speak
      comopen jaws jfwapi
    }
    if ($comerr) {
      .comReg $mircdirMWS\DLLs\JFWAPI.DLL
      if ($com(jaws).progid != jfwapi) comopen jaws jfwapi
    }
    if ($com(jaws).progid == gwspeak.speak) {
      .comclose jaws gwspeak.speak 
      comopen jaws jfwapi
    }
    if ($comerr) {
      /!echo -castblfm Other * Jaws API DLL failed to load
      halt
    }
    integrity
  }

  ;WINEYES error checking
  if ($hGet(Speech,Software) == WINEYES) {
    if ($com(jaws).progid != gwspeak.speak) {
      if ($com(jaws).progid == jfwapi) comclose jaws jfwapi
      comOpen jaws gwspeak.speak
      if ($comerr) { 
        Unknown method for NVDA as yet
        ;        /!echo -castblfm OTHER Windows Eyes API failed to load 
        halt
      }
    }
  }
  if ($left($1,1) == -) {
    var %p = $right($1,1)
    if (%p == s) {
      ;enable speech
      var %Speech $hGet(Speech,Speech))
      if (($2 == on) && (!%Speech)) {
        hAdd -m Speech Speech 1
        Talk Speech Enabled $Help(to disable $+(speech,$chr(44)) push F6.)
        if ($hGet(Speech,AnnounceSpeech)) .scon -at1 .amsg * Speech enabled 
      }
      ;disable speech
      elseif (($2 == off) && (%Speech)) {
        Talk Speech Disabled $Help(to enable $+(speech,$chr(44))  push F5.)
        hDel Speech Speech
        if ($hGet(Speech,AnnounceSpeech)) .scon -at1 .amsg * Speech disabled
      }
    }
    ;Channel Speech
    elseif (%p == c) {
      ;enable channel speech
      if ((($active ischan) && ($2 == on) && ($hGet(speech,$+($network,_,$chan))))) {
        .hDel Speech $+($network,_,$chan)
        Talk Enabled $chan speech $Help(to disable speech on $+($chan,$chr(44)) push control F6.)
        if ($hGet(Speech,AnnounceSpeech)) .say * Enabled $chan speech 
      }
      ;disable channel speech
      elseif ((($active ischan) && ($2 == off) && (!$hGet(speech,$+($network,_,$chan))))) {
        Talk Disabled $chan speech $Help(to enable speech on $+($chan,$chr(44)) push control F5.)
        .hAdd -m Speech $+($network,_,$chan) 1 
        if ($hGet(Speech,AnnounceSpeech)) .say * Disabled $chan speech 
      }
    }
    ;Server Speech
    elseif (%p == n) {
      ;enable network speech
      if (($2 == on) && ($hGet(Speech,$network))) {
        .hDel Speech $network 
        Talk Enabled $network speech  $Help(to disable speech on $+($network,$chr(44)) push shift F6.)
        if ($hGet(Speech,AnnounceSpeech)) .amsg * Enabled $network speech
      }
      ;disable network speech
      elseif (($2 == off) && (!$hGet(Speech,$network))) {
        Talk Disabled $network speech $Help(to enable speech on $+($network,$chr(44)) push shift F5.)
        .hAdd -m Speech $network 1
        if ($hGet(Speech,AnnounceSpeech)) .amsg * Disabled $network speech 
      }
    }
  }
  saveTables
}
;
alias loadTables {
  if (!$hGet(Channel)) .hMake Channel 10  
  if (!$hGet(General)) .hMake General 5
  if (!$hGet(General)) .hMake General 20
  if (!$hGet(Integrity)) .hMake Integrity 10
  if (!$hGet(ServerNotices)) .hMake ServerNotices 10
  if (!$hGet(Sounds)) .hMake Sounds 5
  if (!$hGet(Speech)) .hMake SPEECH 10 
  .hLoad -i General $+(",$mircdirMWS\MWS.ini,") General
  .hLoad -i Integrity $+(",$mircdirMWS\MWS.ini,") Integrity 
  hLoad -i ServerNotices $+(",$mircdirMWS\MWS.ini,") ServerNotices
  .hLoad -i Sounds $+(",$mircdirMWS\MWS.ini,") Sounds
  .hLoad -i SPEECH $+(",$mircdirMWS\MWS.ini,") Speech
} 

alias saveTables { 
  .hSave -i General $+(",$mircdirMWS\MWS.ini,") General
  .hSave -i Integrity $+(",$mircdirMWS\MWS.ini,") Integrity
  .hSave -i ServerNotices $+(",$mircdirMWS\MWS.ini,") ServerNotices
  hSave -i Sounds $+(",$mircdirMWS\MWS.ini,") Sounds
  .hSave -i SPEECH $+(",$mircdirMWS\MWS.ini,") SPEECH
} 

alias integrity {  
  $iif($readmrc(N0,2),hAdd,hDel) Integrity UAddress 1
  $iif($readmrc(N2,30),hAdd,hDel) Integrity ModePrefix 1
  $iif($readmrc(N0,23),hAdd,hDel) Integrity MyPrefix 1
  $iif($readmrc(N3,35),hAdd,hDel) Integrity NOJ 1
  hAdd Integrity Joins $replace($readmrc(default,1,events),0,c,1,s,2,h)
  hAdd Integrity Parts $replace($readmrc(default,2,events),0,c,1,s,2,h)
  hAdd Integrity Quits $replace($readmrc(default,3,events),0,c,1,s,2,b,3,h)
  hAdd Integrity Modes $replace($readmrc(default,4,events),0,c,1,s,2,h)
  hAdd Integrity Topics $replace($readmrc(default,5,events),0,c,1,s,2,h)
  hAdd Integrity ctcps $replace($readmrc(default,6,events),0,c,1,s)
  hAdd Integrity Nicks $replace($readmrc(default,7,events),0,c,1,h)
  hAdd Integrity Kicks $replace($readmrc(default,8,events),0,c,1,s,2,h)
  SaveTables
}

alias MWSFunctions {
  .loadTables 
  if ($hGet(Sounds,General)) {
    var %intro $+($mircdirMWS\Sounds\intro.mp3)
    if ($exists(%intro )) splay %intro
  }
  .timestamp -f $ts
  Integrity 
  if (!$hGet(Speech,NoSpeech)) F5  
}

alias help if ($hGet(General,Help)) return $+([,$1-,])

alias snd { 
  var %sndpk = $hGet(Sounds,SoundPack)
  var %varsnd = $+($mircdirMWS\Sounds\,%sndpk,\,$iif($len($2) > 1,$2-,$3-)) 
  if ($exists(%varsnd)) $iif($1 == Play,splay,return) $iif($len($2) == 1,- $+ $2) %varsnd
}

alias soundpack {
  if (($1 == begin) || ($1 == end)) halt
  var %sndm $remove($finddir($mircdirMWS\sounds\,*,$1),$mircdirMWS\sounds\)
  var %snd2 $finddir($mircdirMWS\sounds\,*,$1)
  if (%sndm != $null) { 
    return Set Sound Pack to %sndm $+ : $+ __setSoundfolder %snd2 
  }
}

alias __setSoundFolder {
  hadd -m Sounds SoundPack $remove($1-,$mircdirMWS\sounds\)
  saveTables
  echo -cast Other Now using the $remove($1-,$mircdirMWS\sounds\) sound scheme 
}

alias menusound $snd(Play,Menu.wav)

alias setTb .timerTBMWS -o 0 60 $TBMWS

alias aji {
  if ($1 == $null) ajinvite
  elseif ($1 == on) ajinvite on 
  elseif ($1 == off) /!ajinvite off 
}

alias ajinvite {
  var %change = 0
  if ($1 = on) %change = 1
  var %temp = $puttok($readini(mirc.ini,options,n3),%change,11,44)
  /writeini -n mirc.ini options n3 %temp
  /!ajinvite $$1 
} 

alias hopall { 
  var %i = 1
  while ($chan(%i)) {
    hop -c $chan(%i)
    inc %i
  }  
}

;Away alias
alias Away {
  var %Public $hGet(General,AnnounceAway) 
  var %Away $hGet(General,Away) 
  if (!$1)  {
    if (!%Away) echo -cstblfmq Other You weren't set away 
    elseif (%Away) {
      if (%Public) .scon -at1 ame has returned: %Away gone $duration($calc($ctime - $hGet(General,Away.time)))
      Back
      halt
    }
  }
  if ($1) {
    if (%Public) .scon -at1 ame $iif(%Away,changes,set ) away to: $ul($1-)   
    var %Away $ul($1-)
    .scon -at1 !Away %Away $Brackets1(left $asctime(h:nn dddd) $daytime)
    hAdd General Away $ul(%Away)
    hAdd General Away.time $ctime
  }
  Integrity
}
alias -l Back {
  .scon -at1 !away
  hDel General Away
  hDel General Away.Time
  Integrity
}

alias SetVol .vol -v $int($calc((65535 / 100) * $1)) 

;Windows Explorer menu itmes
alias MenuShellSpecialFolderPaths {
  if ($1 == begin) return -
  if ($1 == 1) return My Documents :run explorer $ShellSpecialFolderPath
  if ($1 == 2) return Desktop :run explorer $ShellSpecialFolderPath(desktop)
  if ($1 == 3) return Favorites :run explorer $ShellSpecialFolderPath(favorites)
  if ($1 == 4) return Start Menu :run explorer $ShellSpecialFolderPath(startmenu)
  if ($1 == 5) return Send To :run explorer $ShellSpecialFolderPath(sendto)
  if ($1 == 6) return Startup :run explorer $ShellSpecialFolderPath(STARTUP)  
  if ($1 == 7) return Windows :run explorer $ShellSpecialFolderPath(windows)
  if ($1 == 8) return Program Files :run explorer $ShellSpecialFolderPath(programfiles)
  if ($1 == 9) return Application Data :run explorer $ShellSpecialFolderPath(AppData)
  if ($1 == end) return -
}
alias ShellSpecialFolderPath {
  var %SpecialFolder $ShellSpecialFolderConstants($iif($1,$1,Personal))
  .comopen objShell Shell.Application
  if ($comerr) { 
    echo -ces i Create Object Shell.Application failed 
    return
  }
  !noop $com(objShell,Namespace,3, long, %SpecialFolder, dispatch* objFolder)
  .comclose objShell
  if $com(objFolder) {
    !noop $com(objFolder, Self, 3, dispatch* objFolderItem)
    if $com(objFolderItem) {
      !noop $com(objFolderItem,Path,3)
      var %Returns $com(objFolderItem).result $+ \
      .comclose objFolderItem
    }    
    .comclose objFolder
  }
  return %Returns 
}

; Convert special folder name to number. (Used by ShellSpecialFolderPath) 
alias ShellSpecialFolderConstants {
  var %Select $1 
  goto %Select
  :ALTSTARTUP        | return $base(1d,16,10)
  :APPDATA           | return $base(1a,16,10)
  :BITBUCKET         | return $base(0a,16,10)
  :COMMONALTSTARTUP  | return $base(1e,16,10)
  :COMMONAPPDATA     | return $base(23,16,10)
  :COMMONDESKTOPDIR  | return $base(19,16,10)
  :COMMONFAVORITES   | return $base(1f,16,10)
  :COMMONPROGRAMS    | return $base(17,16,10)
  :COMMONSTARTMENU   | return $base(16,16,10)
  :COMMONSTARTUP     | return $base(18,16,10)
  :CONTROLS          | return $base(03,16,10)
  :COOKIES           | return $base(21,16,10)
  :DESKTOP           | return $base(00,16,10)
  :DESKTOPDIRECTORY  | return $base(10,16,10)
  :DRIVES            | return $base(11,16,10)
  :FAVORITES         | return $base(06,16,10)
  :FONTS             | return $base(14,16,10)
  :HISTORY           | return $base(22,16,10)
  :INTERNETCACHE     | return $base(20,16,10)
  :LOCALAPPDATA      | return $base(1c,16,10)
  :MYPICTURES        | return $base(27,16,10)
  :NETHOOD           | return $base(13,16,10)
  :NETWORK           | return $base(12,16,10)
  :PERSONAL          | return $base(05,16,10)
  :PRINTERS          | return $base(04,16,10)
  :PRINTHOOD         | return $base(1b,16,10)
  :PROFILE           | return $base(28,16,10)
  :PROGRAMFILES      | return $base(26,16,10)
  :PROGRAMFILESx86   | return $base(30,16,10)
  :PROGRAMS          | return $base(02,16,10)
  :RECENT            | return $base(08,16,10)
  :SENDTO            | return $base(09,16,10)
  :STARTMENU         | return $base(0b,16,10)
  :STARTUP           | return $base(07,16,10)
  :SYSTEM            | return $base(25,16,10)
  :SYSTEMx86         | return $base(29,16,10)
  :TEMPLATES         | return $base(15,16,10)
  :WINDOWS           | return $base(24,16,10)
  :%Select
}

;Message of the day
alias MOTD { 
  set -u3 %MOTD 1
  .raw motd $1
}

alias mws {
  var %mws $hGet(Integrity,ID)
  unset %omghax
  var %omghax = $mid(%mws,2,$calc($len(%mws)-2))
  var %enc = $ascii(%omghax)
  var %x = -2
  :next
  inc %x 3
  var %code = $mid(%enc,%x,3)
  if (%code == $null) goto end
  else var %out = %out $+ $base($calc(%code - 33),10,10,2) 
  goto next
  :end
  if ($right(%out,3) == 160) /var %out = $mid(%out,1,$calc($len(%out) - 3))
  var %omghax = $asc2chr(%out)
  var %p1 = $gettok(%omghax,1,216)
  var %p2 = $gettok(%omghax,2,216)
  if ($left(%p1,1) == $chr(32)) var %p1 = $mid(%p1,2)
  if ($hash(%p1,32) == %p2) return %p1
  else return Invalid version variable
}

alias -l ascii {
  var %z = 0
  :next
  inc %z
  var %input = $mid($1-,%z,1)
  if (%input == $null) goto end
  else var %out = %out $+ $base($asc(%input),10,10,3) 
  goto next
  :end
  return %out
}

alias -l asc2chr {
  var %addspace = 0
  var %z = -2
  :next
  inc %z 3
  var %input = $mid($1,%z,3)
  if (%input == $null) goto end
  else {
    var %out = $base(%input,10,10)
    if (%out == 32) var %addspace = 1
    else {
      if (%addspace == 1) {
        var %addspace = 0
        var %output = %output $+ $chr(32) $+ $chr(%out)
      }
      else var %output = %output $+ $chr(%out) 
    }
  }
  goto next
  :end
  return %output
}

;
;end of aliases
