;Raw numerics
;Dependancy aliases for raw ranges
alias -l Info-CST return $istok(200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 226 226 229 232 244 245 246 247 248 249 256 257 258 263 290 291 292 337 338 339 382 512,$1,32) 
alias -l Info2-CST return $istok(371 391 396 489 492 495 502,$1,32) 
alias -l Notify-CST return $istok(294 423 434 445 901 902 903 927 928 929 930 943 944,$1,32) 
alias -l Notify-CAT return $istok(303 437 446 455 468 618 619 620 974,$1,32) 
raw *:*: { 
  if (!$numeric) halt
  if ($numeric == 321) halt
  elseif ($numeric == 322) return
  elseif ($numeric == 364) return
  elseif ($numeric == 365) {
    if ($hGet(Sounds,General)) $snd(Play,Q,List.WAV)
    return 
  }
  haltdef
  if ($uptime(server,3) > 15) {
    if ($numeric < 6) echo -cstblfrn info2 $2-
    elseif ($numeric == 105) echo -cstblfrn info $2-
    elseif ($numeric == 252) set -u2 %lIRCops $2 $iif($2 > 1,IRCops,IRCop) $+ $chr(44)
    elseif ($numeric == 253) set -u2 %lUnknownConns $2 unknown connections 
    elseif ($numeric == 254) set -u2 %lChannels $iif($2-,$2-)
    elseif ($numeric == 255) set -u2 %lLocalServers $4 $iif($4 > 1,clients,client) and $iif($7 > 1,$7 servers,no servers) $+($chr(44))    
    elseif ($numeric == 265) set -u2 %lLocally local maximum connections $+($7,$chr(44))
    elseif ($numeric == 266) {
      echo -cstblfrn info2 $nick has %lLocalServers %lLocally global $7
      echo -cstblfrn info2 $network has %lIRCops %lUnknownConns %lChannels 
    }
    elseif ($numeric == 351) echo -cstblfrn info2 $2- 
  }
  if ($Info-CST($numeric)) echo -cstblfrn info $2-
  elseif ($Info2-CST($numeric)) echo -cstblfrn info2 $2-
  elseif ($Notify-CST($numeric)) echo -cstblfrn notify $2-
  elseif ($Notify-CAT($numeric)) echo -catblfrn notify $2- 
  elseif ($numeric == 006) {
    var %strNumeric $iif($chr(37) isin $4,Capacity,Numeric)
    echo -cstblfrn info2 $2 has $remove($3 Users,$chr(41),$chr(40)) $iif($4 != $null,%strNumeric,$null) $remove($4-,$chr(41),$chr(40),$chr(91),$chr(93)) 
  }
  elseif ($numeric == 7)     if ($hGet(General,Help)) echo -cstblfrn info2 $Brackets1($4- complete)
  elseif ($numeric == 008) echo -castblfrn m Server notice mask $remove($5,$chr(40),$chr(41))
  elseif ($numeric == 15)     echo -cstblfrn info2 $2-
  elseif ($numeric == 219)     if ($hGet(General,Help)) echo -cstblfrn info2 $Brackets1($5 $6 complete)
  elseif ($numeric == 221)   echo -castblfrm m * Your user mode is $Brackets1($mid($2-,2))
  elseif ($numeric == 223) echo -cstblfrn info2 $Brackets1($replacexcs($2,s,Shun,G,GLine,Z,GZLine,K,KLine))) for $3 Expires $iif($duration($4) == 0secs,never,$duration($4)) Reason $7- Set $+($asctime($calc($ctime - $5))  ago,$chr(44)) set By $6  
  elseif ($numeric == 225) echo -cstblfrn info2 Except throttle from $3-
  elseif ($numeric == 242) echo -catblfrn info2 $Brackets1($nick uptime) $4 $5 $duration($duration($6)) $Brackets1($int($calc($4 / 7)) weeks $calc($4 % 7) days)
  elseif ($numeric == 230) echo -cstblfrn info2 Except $Brackets1($replacexcs($2,s,Shun,G,GLine,Z,GZLine,q,QLine,Q,Global QLine))) for $3
  elseif ($numeric == 243) echo -cstblfrn info2 $Brackets1(IRCop) $5 host $3 Flags $6 Class $7- 
  elseif ($numeric == 251) {
    echo -cstblfrn info2 $network has $calc($4 + $7) $iif($7 > 1,clients,client) $+ $+($chr(44)) $iif($7 > 0,$Brackets1($7 are invisible)) across $10 $iif($10 > 1,servers,server)
    set -u2 %lServers $10 servers.
  }
  elseif ($numeric == 301) {
    var %AwayNick $hGet(General,$+(AwayNick,_,$network,_,$2))
    if ((%AwayNick) && (%AwayNick == $3-)) halt
    echo -catblfrm WH $2 is away $Brackets1($3-)
    hAdd -mu300 General $+(AwayNick,_,$network,_,$2)) $3-
  }
  elseif ($numeric == 302) {
    if (!$2) halt
    var %new = $remove($2,$chr(42),$chr(43)), %nick $gettok(%new,1,61), %ident = $gettok($replace(%new,$chr(61),$chr(64)),2,64), %host $gettok(%new,2,64)
    echo -catblfrn WH * %nick userhost, Ident %ident $iif($chr(126) isin $2,[Failed],[passed]) Address %host
  }
  elseif ($numeric == 304) echo -cstblfrm info2 $2-
  elseif ($numeric == 305) echo -cstblfrm Info $Brackets1(No longer away)
  elseif ($numeric == 306) echo -cstblfrm Info $Brackets1(Away activated)
  elseif ($numeric == 307) set -eu3 %Registered Identified to a registered NickName
  elseif ($numeric == 310) set -eu3 %Avail4Help Available for help $Help(/HelpOp)
  elseif ($numeric == 311) {
    $iif($idleCheck($2),halt)
    echo -catblfrn WH  Starting $2 Whois 
    echo -catblfrn WH $iif($chr(126) isin $3,Failed) Ident $3
    echo -catblfrn WH Virtual Host $4 $iif($right($4,3) == .IP,$help(masked IP))
    echo -catblfrn WH Real name: $6- 
  }
  elseif ($numeric == 312) {
    $iif($idleCheck($2),halt)
    set -eu3 %server $3
    set -eu3 %Desc $4-
  }
  elseif ($numeric == 313) set -eu3 %Rank Rank $5- 
  elseif ($numeric == 314) {
    echo -catblfrn WH  Starting $2 Whowas 
    echo -catblfrn WH $iif($chr(126) isin $3,Failed) Ident $3
    echo -catblfrn WH Real name $6- 
    echo -catblfrn WH Host $4 $iif($right($4,3) == .IP,$Help(masked IP))
  }
  elseif ($numeric == 315) {
    if ($hGet(General,globalWHO)) return
    elseif ($hGet(General,Help)) echo -cstblfrn wh $Brackets1($2 $5 list complete) 
  }
  elseif ($numeric == 317) {
    if ($left($duration($3),1) != 0)     echo -catblfrn WH Idle time $+($duration($3)) 
    echo -catblfrn WH Total connect time $+($duration($calc($ctime - $4)))) 
    echo -catblfrn WH Signed on $asctime($4,h:nn dddd) $daytime($4) $asctime($4,doo mmmm) 
  }
  elseif ($numeric == 318) {
    $iif($idleCheck($2),goto IdleDone)
    if (%Rank) {
      echo -catblrfn WH %Rank 
      ;$iif($hGet(Sounds,General),$snd(Play,Q,IRCOpDetected.WAV))
      if ($hGet(Sounds,General)) { .timer 1 3 snd Play Q IRCOpDetected.WAV }
    }
    if (%Avail4Help) || (%Registered) echo -catblfrn WH Status %Registered $iif(%Registered,$iif(%Avail4Help,and)) %Avail4Help 
    if (%Swhois)     echo -catblfrn WH Special Whois %Swhois
    if (%server) echo -catblfrn WH Connected %SSL to %server
    if (%desc) echo -catblfrn WH     Server Description %Desc
    if (%ssl) $iif($hGet(Sounds,General),$snd(Play,Q,SSL.WAV))
    if (%server) echo -catblfrn WH $Brackets1(whois complete)
    :IdleDone
    if ($idleCheck($2)) {
      hDel General $+(IdleTime,_,$network,_,$2)
      echo -catblfrn WH $Brackets1(Idle report complete)
    }
  }
  elseif ($numeric == 319) {
    $iif($idleCheck($2),halt)
    var %num = 0,%chans = $sorttok($3-,32)
    while (%num < $gettok($3-,0,32)) {
      inc %num 
      var %token = $gettok(%chans,%num,32)
      if ($left(%token,1) == $chr(63)) var %chan.io = %chan.io $mid(%token,2)
      elseif ($left(%token,1) == $chr(33)) var %chan.ho = %chan.ho $mid(%token,2)
      elseif ($left(%token,1) == $chr(37)) var %chan.h = %chan.h $mid(%token,2)
      elseif ($left(%token,1) == $chr(38)) var %chan.p = %chan.p $mid(%token,2)
      elseif ($left(%token,1) == $chr(43)) var %chan.v = %chan.v $mid(%token,2)
      elseif ($left(%token,1) == $chr(64)) var %chan.o = %chan.o $mid(%token,2)
      elseif ($left(%token,1) == $chr(126)) var %chan.f = %chan.f $mid(%token,2)
      else var %chan = %chan %token 
    }
    echo -catblfrn WH $Brackets1(%num $iif(%num > 1,channels,channel)) 
    if (%chan.io) echo -catblfrn WH only visible since you're an IRCop %chan.io 
    if (%chan.ho)     echo -catblfrn WH IRCop, but hidden %chan.ho 
    if (%chan.f) echo -catblfrn WH Founder on %chan.f 
    if (%chan.p) echo -catblfrn WH Protected on %chan.p 
    if (%chan.o) echo -catblfrn WH Opped on %chan.o 
    if (%chan.h) echo -catblfrn WH Half-Opped on: %chan.h
    if (%chan.v)     echo -catblfrn WH Voiced on %chan.v
    if (%chan)     echo -catblfrn WH Normal user on %chan 
  } 
  elseif ($numeric == 320) set -eu2 %Swhois $2- 
  elseif ($numeric == 323) if ($hGet(Sounds,General)) $snd(Play,Q,List.WAV)
  elseif (($numeric == 324) && ($hGet(General,ChannelStats))) echo -ctblfrm M $2 Modes $Brackets1($mid($3-,2))
  elseif ($numeric == 328) {
    if ($hGet(General,ChannelStats)) {
      if ($hGet(Integrity,Topics) != h) {
        echo $iif($hGet(Integrity,Topics) == s,-cstblrfm Topic,-ctblrfm Topic $2) $Brackets1($iif(!$1,No topic has been set,$1-)) 
      }
    }
  }
  elseif ($numeric == 329) if ($hGet(General,ChannelStats)) echo -ctblfrm Topic $2 Created $asctime($3,h:nn dddd) $DayTime($asctime) $asctime($3,mmmm doo) $year($3)
  elseif ($numeric == 332) if ($hGet(General,ChannelStats)) echo -ctblfrm Topic $2 Topic $3-
  elseif ($numeric == 333) if ($hGet(General,ChannelStats)) echo -ctblfrm Topic $2 Topic set by $3 $asctime($4,h:nn dddd)  $DayTime($4) $asctime($4,mmmm doo)  $year($4)
  elseif ($numeric == 334) echo -catblfrn info $2-
  elseif ($numeric == 335) echo -catblfrn WH $2-
  elseif ($numeric == 336) echo -catblfrn info $2-
  elseif ($numeric == 337) set -eu5 %SSL securely 
  elseif ($numeric == 468) echo -catblfrn no $2-
  elseif (($numeric == 340) && ($2)) echo -catblfrn WH IP for $gettok($gettok($2,1,61),1,42) is $gettok($2,2,64)
  elseif ($numeric == 341) echo -cat inv * $1 invites $2 $iif($right($address,3) == .IP,$help(masked IP)) to join $3 
  elseif ($numeric == 346) echo -catblfrn invite  $3 exempted by $4 $asctime($5,hh:nn dddd) $daytime($5) $asctime($5,mmmm doo) $year($5)
  elseif ($numeric == 347) if ($hGet(General,Help)) echo -catblfrn invite $Brackets1($2 invite exception list complete)
  elseif ($numeric == 348) echo -catblfrn info2  $3 exempted by $4 $asctime($5,h:nn dddd) $daytime($5) $asctime($5,mmmm dooo) $year($5)

  elseif ($numeric == 349) if ($hGet(General,Help)) echo -catblfrn info2 $Brackets1($2 ban exception list complete)
  elseif ($numeric == 352) { 
    if ($hGet(General,globalWHO)) return
    echo -cstblfrn wh $6 $iif($2 != $chr(42),Last joined $2) $iif($left($3,1) == $chr(126),Failed) Ident $remove($3,$chr(126)) Host: $4 $iif($right($4,3) == .IP,$Help(masked IP)) Real Name: $9-
    echo -cstblfrn wh $iif($7,Status $replace($replacexcs($7,r,registered.,G,is /away.,H,is not /Away.,B,a Bot.,*,IRCop.,!,Hidden IRCop.,?,can only be seen because you are an IRCop.),.,$chr(44) $chr(32))) $iif($5 != hidden,Connected $iif($5 == $server,locally to $5,to $5) $iif($8 > 0,which is $8 hops away)))
  }
  elseif ($numeric == 353) if ($hGet(Integrity,NOJ)) echo -ctblfrn Info $3 Users on $3 $4-
  elseif ($numeric == 366) {
    if (($2 ischan)            && ($hGet(General,ChannelStats)))  {
      var %Synch $hGet(Channel,$+($network,_,$2))
      if (%Synch) echo -ctblfrn Info $2  Took $iif($calc(($ticks - %Synch) / 1000) < 1,$calc($ticks - %Synch) Milliseconds,$calc(($ticks - %Synch)/1000) Seconds) to join $2
      if ($nick($2,0,a) > 1) echo -ctblfrn Info $2 Total users $nick($2,0,a) $+ , $iif($nick($2,0,o) > 0,$nick($2,0,o) Ops) $iif($nick($2,0,h) > 0,$nick($2,0,h) Half ops) $iif($nick($2,0,v) > 0,$nick($2,0,v) Voiced) $iif($nick($2,0,r) > 0, & $nick($2,0,r) regular users)
      hDel Channel $+($network,_,$2))
    }  
  }
  elseif ($numeric == 367) echo -catblfrn info2  $3 banned by $4 $asctime($5,h:nn dddd) $DayTime($5) $asctime($5,mmmm doo) $year($5)

  elseif ($numeric == 368) if ($hGet(General,Help)) echo -catblfrn info2 $Brackets1($2 ban list complete)
  elseif ($numeric == 369) {
    if (%server) echo -catblfrn WH Connected to IRC via %server     
    echo -catblfrn WH Signed on %desc
    echo -catblfrn WH $Brackets1(Whowas complete) 
  }
  elseif ($numeric == 372) {
    if (%MOTD == 1)       {
      if ($window(@MOTD) == $null) {
        .window -lv @MOTD 
        .renwin @MOTD @MOTD $network Message of the day
      }
      aline -al @MOTD $2-
    }
  }
  elseif ($numeric == 375) {
    if (%MOTD) return
    echo -cstblfrm Info $server has a message of the day, $Help(It can be viewed by typing /MOTD at anytime)
  }
  elseif ($numeric == 378) {
    $iif($idleCheck($2),halt)
    var %RealHost $gettok($$6,2,64)
    if ($7 != %RealHost) echo -catblfrn WH Real Host     %RealHost 
    if ($7) echo -catblfrn WH IP address $7-
  }
  elseif ($numeric == 379) {
    $iif($idleCheck($2),halt)
    echo -catblfrn WH User Modes $Brackets1($mid($6,2))
    if ($7) echo -catblfrn WH Server Notice Flags $Brackets1($mid($7,2))
  } 
  elseif ($numeric == 381) echo -cstblfrm info $Brackets1(Now an IRCop on $server) $iif($hGet(Sounds,General),$snd(Play,Q,Acknowledged.WAV))
  elseif ($numeric == 391) echo -cstblfrn info2 $2- 
  elseif ($numeric == 396) echo -cstblfrn info2 $2- 
  elseif ($numeric == 401) echo -catblfrn Info2 $Brackets1(No such Nick or Channel $2,$iif($activecid != $cid,$chr(32) on $network))  
  elseif ($numeric == 402) echo -catblfrn WH $2- 
  elseif ($numeric == 405) echo -castblfrm Notify $Brackets1(You are on $chan(0).a channels and have reached the specified limit)
  elseif ($numeric == 403) echo -catblfrn Info2 $Brackets1(The channel $2 does not exist) 
  elseif ($numeric == 404) echo -catblfrn Info $Brackets1($3-) 
  elseif ($numeric == 421) echo -catblfrn Info2 $Brackets1($2 unknown command) 
  elseif ($numeric == 422) echo -cstblfrm Info2 $Brackets1($nick has no Message of the day) 
  elseif ($numeric == 432) echo -castblfrm Info2 $Brackets1($2-) 
  elseif ($numeric == 433) echo -cstblfrm Info2 $Brackets1(This nickname is already being used) 
  elseif ($numeric == 436) echo -cstblfrm Info2 $Brackets1(Warning nick collision) 
  elseif ($numeric == 438) echo -catblfrn Info2 $3-
  elseif ($numeric == 441) echo -catblfrn Info2 $Brackets1($2 is not on $3) 
  elseif ($numeric == 443) echo -catblfrn Info2 $2 $4-
  elseif ($numeric == 451) echo -cstblfrn Info2 Connection not complete, choose another NickName
  elseif ($numeric == 461) {
    if ($hGet(General,globalWHO)) {
      .who *
      halt 
    }
    echo -cstblfrm info2 $2-
  }
  elseif ($numeric == 464) echo -castblfrm info2 $Brackets1(Passwored incorrect) $Help(be aware that passwords are case sensitiv)) $iif($hGet(Sounds,General),$snd(Play,Q,Denied.WAV))
  elseif ($numeric == 471) echo -catblfrm info2 you Cannot join $2 $+ , The channel is full 
  elseif ($numeric == 472) echo -catblfrn info2 Mode $2 is an unknown character 
  elseif ($numeric == 473) {
    echo -castblfrm invite You cant join $mid($2,2) $+ $chr(44) its set to invite only
    if ($hGet(General,Help)) {
      echo -castblfrm invite $Brackets1(Request an invitation by asking an op from $mid($2,2) or using /knock $2)
      .knock $2 [Automatic knock] $2 is set to invite only, please invite me
    } 
  }
  elseif ($numeric == 474) echo -catblfrm info2 You're banned from $2 
  elseif ($numeric == 475) echo -catblfrm join * You cannot join $2 $+ , you specified the wrong channel password $Help(Please try again by typing /join $2 password) $iif($hGet(Sounds,General),$snd(Play,Q,Locked.WAV))
  elseif ($numeric == 476) echo -catblfrn info2 $3-
  elseif ($numeric == 477) echo -catblfrn info2 $3-
  elseif ($numeric == 478) echo -catblfrn info2  $4- 
  elseif ($numeric == 480) echo -catblfrn notify $5 is not set to invite only
  elseif ($numeric == 481) echo -cstblfrn notify Permission denied     $Help(That command requires IRCop privellages or higher) $iif($hGet(Sounds,General),$snd(Play,Q,PermissionDenied.WAV))
  elseif ($numeric == 482) echo -catblfrn notify Permission denied, you are not a channel operator $iif($hGet(Sounds,General),$snd(Play,Q,PermissionDenied.WAV))
  elseif ($numeric == 483) echo -catblfrn info2 $2-
  elseif ($numeric == 484) echo -catblfrn info2 $3-
  elseif ($numeric == 491) echo -castblfrm hi Access denied, no o-lines matching your IP or Hostname. $Help(Be aware that usernames are case sensitive, perhaps your IP or Hostname  has changed!) $iif($hGet(Sounds,General),$snd(Play,Q,Denied.WAV))
  elseif ($numeric == 500) echo -catblfrm info2 Too many /join requests, try again shortly
  elseif ($numeric == 501) echo -cstblfrn hi $2- 
  elseif ($numeric == 512) echo -cstblfrn info $2- 
  elseif ($numeric == 519) echo -castblfrm notify only IRC Admins can join $5 
  elseif ($numeric == 520) echo -castblfrm notify only IRCops can join $5 
  elseif ($numeric == 601) echo -cstblfrn no Notify $2- 
  elseif ($numeric == 602) echo -cstblfrn info2 Notify $2- 
  elseif ($numeric == 603) echo -cstblfrn no Notify Watching $4 nicks Watched by $8 nicks
  elseif ($numeric == 604) {
    echo -cstblfrn no Notify Nick $2 Ident $3 Address $4 
    echo -cstblfrn no Notify: Signed on: $asctime($5,hh:nn dddd) $daytime($5) $asctime($5,mmmm doo) $year($5) Status: $6-
  }
  elseif ($numeric == 605) echo -cstblfrn no Notify: $2- 
  elseif ($numeric == 607)     if ($hGet(General,Help)) echo -cstblfrn no $Brackets1(Watch list complete)
  elseif ($numeric == 617) echo -catblfrn info2 $2- 
  elseif ($numeric == 659) echo -castblfrm hi $Brackets1(This message/command was processed but has been logged by $server in case of spam)
  elseif ($numeric == 671) set -eu5 %SSL securely $iif($hGet(Sounds,General),$snd(Play,Q,SSL.WAV))
  elseif ($numeric == 972) echo -catblfrn info2 $3 is protected by usermode [Q] and can not be kicked. 
  ;time check
  ;if (%time.synch) echo -catblfrn Info  $iif($calc(($ticks - %time.synch) / 1000) < 1,$calc($ticks - %time.synch) Milliseconds,$calc(($ticks - %time.synch)/1000) Seconds)
}

;
;end of raw numerics
