;mIRC with speech menus
menu status {
  ;IRCop menus
  &Hanby bits  
  .&Oper modes and flags
  ..Di&sable all connects and disconnects:.mode $me +s -cF
  ..Disable local &connects and disconnects:.mode $me +s -c
  ..Disable &far connects and disconnects:.mode $me +s -F
  ..&Enable all connects and disconnects:.mode $me +s +cF
  ..Enable local &connects and disconnects:.mode $me +s +c
  ..Enable &far connects and disconnects:.mode $me +s +F
  .&Statistics  
  ..Level of users:lusers
  ..Server uptime:stats u
  ..  $iif(o isin $usermode,Servers not currently linked):Stats X
  ..Servers linked:links
  ..Network map:map
  .&User and server info
  ..  $iif(o isin $usermode,Users IP):userip $$?="Enter nickname:"
  ..  $iif(o isin $usermode,Users host):userhost $$?="Enter nickname:"
  ..Show all IRCops:who * o
  ..Get extended user info:uwho $$?"Enter nickname:"
  ..Admin info:admin
  .&Message of the day
  ..Message of the day:motd
  ..Bot message of the day:.botmotd
  ..Oper message of the day:opermotd
  ..Rules:rules
}
menu menubar {
  $iif(o isin $usermode,&IRCop toys)
  .&server notices panel: dialog $iif(!$dialog(OperView),-mid,-ev) OperView OperView
  .A&nope
  ..&Manage Staff
  ...all sta&ff:.os staff
  ...Services &admins:os admin list
  ...Add services admin:os admin add $$?"Enter nickname:"
  ...Remove services admin:os admin add $$?"Enter nickname:"
  ...Services &opers:os oper list
  ...Add services oper:os oper add $$?"Enter nickname:""
  ...Remove services oper:os oper del $$?"Enter list entry to remove:"
  ..&Noop a server:os noop set $$?"Enter full name of server:"
  ..Remove a nooped server:os noop revoke $$?"Enter full name of server:"
  ..&Jupe a server:os jupe $$?"Enter full server name:"
  ..S&quit a server:.squit $$?"Enter server name:"
  ..Sav&e services databases:os update
  ..Re&load configuration file:os reload
  ..Fo&rce nick change:os svsnick $$?"Enter nickname:" $$?"Enter new nickname:"
  ..Su&per Admin on:os set superadmin on
  ..Sup&er Admin off:os set superadmin off
  ..Enter &Defcon mode:os set defcon $?"Choose Defcon mode to enter:"
  .S&erver announcements
  ..All users:msg operserv global $$?="Specify message to send globally:"
  ..Local ops:locops $$?"Specify message to announce:"
  ..Global ops:globops $$?"Specify message to announce:"
  ..Administrator chat:adchat $$?"Specify message to announce:"
  ..Network administrator chat:nachat $$?"Specify message to announce:"
  .&Password Retreival
  ..Get &nickname password:/msg nickserv getpass $$?="Enter nickname:"
  ..Get &channel password:/msg chanserv getpass $$?="Enter channel:"
  ..Get channel &key:msg chanserv getkey $$?"Enter channel:" 
  .$iif(a isin $usermode,&Services-Admin commands)
  ..Force user to &join a channel:/sajoin $$?"Specify the user to join:" $$?"Channel to join user to:"
  ..Force user to &part a channel:/sapart $$?"Specify the user to part:" $$?"Channel to part user from:"
  ..Set &mode on channel:samode $$?="Specify to channel to set modes on:" $$?="Specify modes to set on channel:"
  .&Network maintanence
  ..Rehash:rehash
  ..Rehash a server:rehash $?="Specify linked server?:"
  ..&Garbage collection:rehash -garbage
  ..Connect server:connect $$?="Specify server to link:"
  ..Connect remote server:connect $$?="Specify leaf server:" $$?="Specify link port:" $$?="Specify hub server:"
  .Services Stats:.os stats
}

menu nicklist {
  $iif(o isin $usermode,&IRCop toys)  
  .A&nope services
  ..Kick $1:os kick # $$1 Ordered by an IRCop
  ..Fo&rce $1 to change NickName:os svsnick $$?"Enter nickname:" $$?"Enter new nickname:"
  .$iif(a isin $usermode,Force part): sapart $$1 #
  ..Get &NickServ password:.nickserv getpass $$1
  .&Kill:kill $$1 $$?"Enter reason for kill:"
  .&shun $1:shun $$1 $$?="How long for:" $$?="Enter Reason:"
  .&Remove $1 shun:shun - $+ $$1
  .GLine $1:GLine $$1 $$?"How long for:" $$?"Enter Reason:"
}
menu menubar,status,channel,query {
  &Speech options
  .$iif(!$hGet(Speech,NoSpeech),$iif($hGet(Speech,Speech),Disable,Enable) &General speech):DoSpeak -s $iif($hGet(Speech,Speech),off,on)
  .$iif(!$hGet(Speech,NoSpeech),$iif($chan,$iif(!$hGet(speech,$network),Disable,Enable) $network network speech)):doSpeak -n $iif(!$hGet(speech,$network),OFF,ON)
  .$iif(!$hGet(Speech,NoSpeech),$iif(!$hGet(speech,$+($network,_,$chan)),Disable,Enable) $chan speech):doSpeak -c $iif(!$hGet(speech,$+($network,_,$chan)),OFF,ON)
  .$iif(!$hGet(Speech,NoSpeech),$iif($hGet(Speech,AnnounceSpeech),Disable,Enable) speech status &publically):{ 
    $iif($hGet(Speech,AnnounceSpeech),hDel,hAdd -m) Speech AnnounceSpeech 1
    echo -catblfn other [Public speech status $iif($hGet(Speech,AnnounceSpeech),Enabled,Disabled)]
  }
  .$iif(!$hGet(Speech,NoSpeech),$iif($hGet(Speech,InputText),Disable,Enable) $hGet(Speech,Software) saying what I type):{
    .$iif($hGet(Speech,InputText),hDel,hAdd -m) Speech InputText 1
    echo -cat other $Brackets1($hGet(Speech,Software)) Will $iif($hGet(Speech,InputText),now,not) say what you type]
  }
  .Select screen reader or no speech):{
    hDel speech software
    hDel Speech NoSpeech
    set -u2 %choose.tts $true
    DoSpeak
  }
  ;speech menu end
}
menu channel {
  $menusound
  $iif(o isin $usermode, &IRCop toys)
  .Get &channel password:msg chanserv getpass #
  .Get channel &key:msg chanserv getkey #
  .$iif(a isin $usermode,&Services-Admin commands)
  ..&Join user to channel:sajoin $$?="Specify the user to join:" #
  ..&Part user from channel:sapart $$?"Specify the user to part:" #
  ..Set &mode on channel:samode # $$?="Specify mode to set on channel:"
  $iif($chan == &bitlbee,&Bitlbee)
  .&Basics
  ..Identify:.msg $chan identify $$?"Please enter your Password:"
  .&Accounts
  ..Add an &Facebook account:.msg &bitlbee account add jabber $+($?"Type your facebook username",@chat.facebook.com) $?"Type your facebook password:"
  ..Add an &Msn account:.msg &bitlbee account add msn $?"Type your msn address" $?"Type your msn password:"
  ..Add an &ahoo account:.msg &bitlbee account add yahoo $?"Type your yahoo address" $?"Type your yahoo password:"
  ..Add an &ICQ account:.msg &bitlbee account add oscar $?"Type your ICQ number" $?"Type your ICQ password:" login.icq.com
  ..Add an &Aim account:.msg &bitlbee account add aim $?"Type your aim address" $?"Type your aim password:" login.aol.com
  .&Settings
  ..Display &name changes:.msg &bitlbee set display_namechanges on
  ..&D&Voice on away status:.msg &bitlbee set away_devoice on
  ..Simulate &NetSplits :.msg &bitlbee set simulate_netsplit on
  ..&Typing notice:.msg &bitlbee set typing_notice on
  ..Save settings:.msg &bitlbee save
  .$iif($hGet(Sounds,BitlbeeJoinQuits),Disable,Enable) &Bitlbee join and Quit sounds:{ 
    $iif($hGet(Sounds,BitlbeeJoinQuits),hDel,hAdd -m) Sounds BitlbeeJoinQuits 1
    echo -catblfn other [Bitlbee Join and Quit sounds now $iif($hGet(Sounds,BitlbeeJoinQuits),enabled,disabled)]
  }
  .Account o&n:.msg &bitlbee account on
  .Account o&ff:.msg &bitlbee account off
}
menu menubar,channel {
  &Sounds
  .Select sound &pack to use
  ..Selected theme is $hGet(Sounds,SoundPack):return
  ..$submenu($soundpack($1,))
  .Set system &Volume:.setvol $?"Enter a number between 0 and 100:"
  .$iif($hGet(Sounds,General),Disable,Enable) &general sounds:{ 
    $iif($hGet(Sounds,General),hDel,hAdd -m) Sounds General 1
    echo -catblfn other [General sounds now $iif($hGet(Sounds,General),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,ServicesSounds),Disable,Enable) &Services sounds:{ 
    $iif($hGet(Sounds,ServicesSounds),hDel,hAdd -m) Sounds ServicesSounds 1
    echo -catblfn other [Services sounds now $iif($hGet(Sounds,ServicesSounds),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,OpVoice),Disable,Enable) &Op and Voice sounds:{ 
    $iif($hGet(Sounds,OpVoice),hDel,hAdd -m) Sounds OpVoice 1
    echo -catblfn other [Op and Voice sounds now $iif($hGet(Sounds,OpVoice),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,Query),Disable,Enable) Private &Query sounds:{ 
    $iif($hGet(Sounds,Query),hDel,hAdd -m) Sounds Query 1
    echo -catblfn other [Private Query sounds now $iif($hGet(Sounds,Query),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,Global),Disable,Enable) &Global message sounds:{ 
    $iif($hGet(Sounds,Global),hDel,hAdd -m) Sounds Global 1
    echo -catblfn other [Global message sounds now $iif($hGet(Sounds,Global),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,JoinParts),Disable,Enable) &join and part sounds:{ 
    $iif($hGet(Sounds,JoinParts),hDel,hAdd -m) Sounds JoinParts 1)
    echo -catblfn other [Join and Part sounds now $iif($hGet(Sounds,JoinParts),enabled,Disabled)]
  }
  .$iif($hGet(Sounds,BitlbeeJoinQuits),Disable,Enable) &Bitlbee join and Quit sounds:{ 
    $iif($hGet(Sounds,BitlbeeJoinQuits),hDel,hAdd -m) Sounds BitlbeeJoinQuits 1
    echo -catblfn other [Bitlbee Join and Quit sounds now $iif($hGet(Sounds,BitlbeeJoinQuits),enabled,disabled)]
  }
  &Options
  .$iif(!$hGet(General,HelpModes),&Enable,&Disable) Help Modes:{ 
    $iif(!$hGet(General,HelpModes),hAdd -m,hDel) General HelpModes 1
    echo -catblfn other [Help modes now $iif($hGet(General,HelpModes),enabled,disabled)] 
  }
  .$iif($hGet(General,ChannelStats),Dont show,Show) extra statistics when I join channels:{ 
    $iif($hGet(General,ChannelStats),hDel,hAdd -m) General ChannelStats 1
    echo -catblfn other [Will   $iif($hGet(General,ChannelStats),now,not) show extra channel statistics on join]
  }
  .$iif($hGet(General,Talker),Disable,Enable) &talker:{ 
    $iif($hGet(General,Talker),hDel,hAdd -m) General Talker 1
    echo -catblfn Other [Talker now $iif($hGet(General,Talker),enabled,disabled)] 
  }
  .$iif($hGet(General,CapsWarning),Disable,Enable) &Warning when Caps Lock is on:{ 
    $iif($hGet(General,CapsWarning),hDel,hAdd -m) General CapsWarning 1
    echo -catblfn Other $iif($hGet(General,CapsWarning),[Enabled,[Disabled) warning when Caps Lock is on]
  }
  .$iif($hGet(General,NickHighlight),Disable,Enable) &nick highlight warning:{
    $iif($hGet(General,NickHighlight),hDel,hAdd -m) General NickHighlight 1
    echo -catblfn Hi [Nick highlight now $iif($hGet(General,NickHighlight),enabled,Disabled)]
  }  
  .  .$iif($hGet(General,NickHighlight),$iif($hGet(General,HighlightMessage),Disable,Enable)) nick highlight &Message:{
    $iif($hGet(General,HighlightMessage),hDel,hAdd -m) General HighlightMessage 1
    echo -catblfn Hi [Highlight message now $iif($hGet(General,HighlightMessage),enabled,Disabled)]
  }  
  .$iif($hGet(General,PrivateMessageAlert),Disable,Enable) Private &Message Alert:{ 
    $iif($hGet(General,PrivateMessageAlert),hDel,hAdd -m) General PrivateMessageAlert 1
    echo -catblfn other $Brackets1(Private Message Alert now $iif($hGet(General,PrivateMessageAlert),enabled,Disabled))
  }
  .$iif($hGet(General,Help),Disable,Enable) e&xtra help:{ 
    $iif($hGet(General,Help),hDel,hAdd -m) General Help 1
    echo -catblfn other [Extra help messages now $iif($hGet(General,Help),enabled,disabled)]
  }
  .$iif($readmrc(3,11) == 0,&Enable automatic join on invite):{
    aji on
    echo -catblfn other [Will now automatically join invited channels]
  }
  .$iif($readmrc(3,11) == 1,&Disable automatic join on invite):{
    aji off
    echo -catblfn other [Will no longer automatically join invited channels]
  }
  &Tools
  .$server lag
  ..Announce in Private:.RawPing    
  ..Announce Publically:{
    set -eu10 %publag $active 
    set -eu10 %lag $ticks 
    raw -q ping $server
  }
  .Active &Server & Channel list:serverlist
  .$power2:power
  .Send a message to all channels/servers:scon -at1 amsg $$?"Enter message to send to all channel/servers:"
  .&Global users in a dialog box:global
  .&Rejoin all channels on $server:.HopAll
}
;Services popups
menu menubar,status,channel,query {
  $menusound
  ;Nickname submenu
  &Nickname
  .Change &nick:nick $$?="Enter new nickname:"
  .&Identify to this nickname:.msg nickserv identify $$?"Password for this nickname:"
  .&Show me info on a nickname:.msg nickserv info $$?="Enter nickname to get info on:" all
  .$iif(guest isin $me,NickServ made me a guest $+ $chr(44) release my nick):.msg nickserv release $$?="Enter nickname to release:" $$?="Enter password for nickname to release:"
  .Someones using my nickname, recover it:.msg nickserv recover $$?="Enter nickname to recover:" $$?="Enter password for nickname to be recovered:"
  .My nick is a ghost, kill it off the server:.msg nickserv ghost $$?="Enter your ghosts nickname:" $$?="Enter password for your ghosts nickname:"
  .&Add # to auto join list:.msg nickserv ajoin add #
  .&Remove # from auto join list:.msg nickserv ajoin del #
  .&List current auto join channels:.msg nickserv ajoin list
  .Enable NickServ auto&join:.msg nickserv set autojoin on
  .&Disable NickServ auto join:.msg nickserv set autojoin off
  .List my linked &nicknames:.msg nickserv links
  .Link this nickname to my normal nickname:.msg nickserv link $$?="Enter registered nickname to link with:" $$?="Enter registered nicknames password:"
  .Drop this linked nickname:.msg nickserv unlink $$?="Nickname to unlink:"
  .&Access list
  ..Add this host:.msg nickserv access add $address($me,2)
  ..Remove this host:.msg nickserv access del $address($me,1)
  ..List current access list:.msg nickserv access list
  ..Remove a listed host:.msg nickserv access del $$?"Enter list number of access host to remove:"
  ..Wipe access list:.msg nickserv access wipe
  .&Personalise
  ..Display a URL in my nickserv info:.msg nickserv set url $$?="Enter URL to set on your nickname:"
  ..Display my age in nickserv info:.msg nickserv set age $$?="Enter your age:"
  .Register my nickname:.msg nickserv register $$?"Password to use for this nickname:" $$?"Enter a valid E-mail address:"
  ..Change my password:.msg nickserv set password $$?="Enter new password:" 
  .Drop my nickname:.msg nickserv drop $me $$?"Password for nickname to drop:"
}
;channel menu
menu menubar,channel {
  ;Channel SubMenu
  &Channel
  .Channel &basics
  ..Join:/join #$$?="Enter channel name:"
  ..Part:/part #$$?="Enter channel name:"
  ..Invite user:/invite $$?="Enter nickname to invite:" #
  &Channel access options
  ..All users
  ...Enable auto voice:.msg chanserv set # autovop on
  ...Disable auto voice:.msg chanserv set # autovop off
  ...Enable auto half op:.msg chanserv set # autohop on
  ...Disable auto half op:.msg chanserv set # autohop off
  ...Enable auto op:.msg chanserv set # autoop on
  ...Disable auto op:.msg chanserv set # autoop off
  ..Add nick to auto voice:.msg chanserv vop # add $$?="Enter nickname to grant auto voice to:"
  ..Remove nick from auto voice:.msg chanserv vop # del $$?="Enter nickname to remove auto voice from:"
  ..Add nick to auto half op:.msg chanserv hop # add $$?="Enter nickname to grant auto op access to:"
  ..Remove nick from auto half op:.msg chanserv hop # del $$?="Enter nickname to remove auto help op access from:"
  ..Add nick to auto op:.msg chanserv aop # add $$?="Enter nickname to grant auto op access to:"
  ..Remove nick from auto op:.msg chanserv aop # del $$?="Enter nickname to remove auto op access from:"
  ..Add nick to super op:.msg chanserv sop # add $$?="Enter nickname to grant super op access to:"
  ..Remove nick from super op:.msg chanserv sop # del $$?="Enter nickname to remove super op access from:"
  ..Add nick to Co-founder:.msg chanserv cfounder # add $$?="Enter nickname to grant Co-founder access to:"
  ..Remove nick from Co-founder:.msg chanserv cfounder # del $$?="Enter nickname to remove Co-founder access from:"
  .$iif($me isop $chan,&Control)  
  ..&Modes
  ...Op:op $$?"Enter nickname:" 
  ...Deop:dop $$?"Enter nickname:" 
  ...Voice:/mode # +v $$?"Enter nickname:" 
  ...Devoice:mode # -v $$?"Enter nickname:" 
  ..Kick and bans
  ...Kick:kick # $$?"Enter nickname:" 
  ...Kick (why):kick # $$?"Enter nickname:" $$?="Reason:"
  ...Ban:ban $$?"Enter nickname:" 2
  ...Ban and kick for 10 minutes:ban -ku600 $$?"Enter nickname:" 2 This is a ten minute ban.
  ...Ban and kick for 1 day:ban -ku84000 $$?"Enter nickname:" 2 This is a one day ban, you may return after the ban expires
  ...Ban and Kick:ban -k $$?"Enter nickname:" 2 
  ...Unban:ban -r $$?"Enter nickname:" 2
  ...Unban [who]:ban -r $$?"Enter nickname to unban:" 2
  ...Ban and Kick (why):ban -k $$?"Enter nickname:" 2 $$?"Reason:"
  .&Security and maintenance
  ..Identify to this channel:/msg chanserv identify # $$?"Password for this channel:"
  ..Register this channel:/msg chanserv register $chan $$?"Please provide a password for this channel:" $$?"Describe this channel:" | /msg chanserv set $chan topic $$?"Please provide a topic for this channel:"
  ..Change password for this channel:/cs set passwd $$?="Enter current password:" $$?="Enter new password:" 
  ..Drop current channel:/msg chanserv drop $chan $$?"Password for channel to drop:"
  .&Greetings
  ..Change topic on this channel:.msg chanserv set # topic $$?="Enter the topic you wish to set:"
  ..Change welcome message on this channel:.msg chanserv set # welcome $$?="Enter the welcome message you wish to set:"
  ..Change entry message on this channel:.msg chanserv set # entrymsg $$?="Enter the entry message you wish to set:"
  .Ignore user:/ignore $$?="Enter nickname:"
  .Unignore user:/ignore -r $$?="Enter nickname:"
  .Ignore list:ignore -l
  .Why does a nickname have access in this channel:.msg chanserv why # $$?="Enter nickname to query access:"
  .Get info on this channel:.msg chanserv info #
  &Memos
  .Send memo:MemoSender
  .List memos:msg memoserv list
  .Read a memo:msg memoserv read $$?="Enter number of memo to read:"
  .Delete a memo:msg memoserv del $$?="Enter number of memo to delete:"
  &Messages
  .Query user:/query $$?="Enter nickname to begin query:"
  .Send notice:/notice $$?="Enter nickname:" $$?"Enter message:"
  .Send page: ctcp $$?"Enter nickname:" $$?"Enter message to page:"

  &Away
  .$iif($hGet(General,Away),&Return from $hGet(General,Away)):.away
  .$iif($hGet(General,AnnounceAway),Dont) tell channels your away message:{ 
    $iif(!$hGet(General,AnnounceAway),hAdd -m,hDel) General AnnounceAway 1
    echo -catblfn other $Brackets1(Will $iif($hGet(General,AnnounceAway),now,not) inform open channels your away and back status)
  }
  .Predefined away messages
  ..Gone to the shop:.away Gone to the shop
  ..Back later, going to work:.away Back later, going to work
  ..Back later, going to school:.away Back later, going to school
  ..Out to lunch:.away Out to lunch
  ..Watching TV:.away Watching TV
  ..Sleeping:.away sleeping
  .Custom away message:.away $$?"Enter custom away message:"

  &DCC
  .Add nick to DCC allow list:dccallow + $+ $$?"Enter nickname:"
  .Remove nick from DCC allow list:dccallow - $+ $$?"Enter nickname:"
  .DCC allow list:dccallow list
  .Send file:send $$?"Enter nickname:"
  .DCC chat:dcc chat $$?"Enter nickname:"

  CTCP
  .P&age:/ctcp $$?="Enter nickname to page:" page $$?="Enter message to page:"
  .&Ping:/ctcp $$?="Enter nickname:" ping
  .&Speech status [if any]:/ctcp $$?="Enter nickname:" SPEECH
  .&Time:/ctcp $$?="Enter nickname:" time
  .&Version:/ctcp $$?="Enter nickname:" version
  .&Client info:ctcp $$?"Enter nickname:" clientinfo
  .&Finger:/ctcp $$?="Enet nickname:" finger

  &User Information
  .&User summary:uwho $$?"Enter nickname:"
  .&Who is user:/whois $$?="Enter nickname:"
  .W&ho was a user:/whowas $$?="Enter nickname:"

  &Quit IRC
  .Quit server with &fake message
  ..Server to lame:quit Server to lame
  ..Waset by beer:quit Waset by beer 
  ..Ping timeout:quit Ping timeout
  ..Excess duds:quit Excess duds
  ..Connection reset by beer:quit Connection reset by beer
  .Custom message:quit $$?"Enet custom quit message:"
  .&Quit now:quit
  .E&xit mIRC now:exit -n
  &Connect To New Server :/server -m $$?="Specify the new server to connect to:"
  &Windows Explorer
  .$submenu($MenuShellSpecialFolderPaths($1))

  &information
  .&What's new:.run $mircdirMWS\Information\versions.txt
  .&Frequently asked questions (FAQ):.run $mircdir $+ /mws\Information\faq.txt
  .&Appendix:.run $mircdir $+ /mws\Information\Appendix.txt
  .A&bout:.run $mircdir $+ /mws\Information\About.txt
  .Check for &updates:.MWSupdate
  &Restart mIRC:exit -rn
} 
;Popups
menu nicklist,query {
  $iif($chan == &bitlbee,&Bitlbee)
  .&Rename $1:.msg $chan rename $1 $?-"Enter new NickName $1 will now be known as:"
  .R&emove $1:.msg $chan remove $1
  .&Block $1:.msg $chan block $1
  .&Allow $1:.msg $chan allow $1
  $1 &Options
  .&spoke $duration($nick(#,$1,$snicks).idle)) ago:halt
  .Ident $remove($gettok($address($1,1),1,64),$chr(33),$chr(42)):halt 
  .Host $gettok($address($1,1),2,64):halt
  .&Whois:/whois $1 $1
  .CTCP &version:.ctcp $1 VERSION
  .&Extended info:/uwho $1
  .&Invite into a channel:invite $$1 $$?"Enter channel:"
  .I&gnore:ignore $$1 1
  .&Unignore:ignore -r $$1 1
  .&Add to DCC allow list:dccallow + $+ $$1
  .&Send file:send $$1
  .DCC &chat:dcc chat $$1
  .&Close all query windows:.close -m
  $iif($me isop $chan,&Modes)  
  .Op:op $$1
  .Deop:dop $1
  .$iif($me ishop $chan || $me isop $chan,Voice):/mode # +v $1
  .$iif($me ishop $chan || $me isop $chan,Devoice):mode # -v $1
  $iif($me isop $chan || $me ishop $chan,&Kick and bans)
  .Kick:kick # $$1
  .Kick (why):kick # $$1 $$?="Reason:"
  .Ban:ban $$1 2
  .Ban and kick for 10 minutes:ban -ku600 $$1 2 This is a ten minute ban.
  ..Ban and kick for 1 day:ban -ku84000 $$1 2 This is a one day ban, you may return after the ban expires
  .Ban and Kick:ban -k $$1 2 
  .Unban:ban -r $$1 2
  .Unban [who]:ban -r $$?"Enter nickname to unban:" 2
  .Ban and Kick (why):ban -k $$1 2 $$?"Reason:"
  &Fun Popups
  .Everything &hi
  ..You make me so Hoppy.: {
    say  12/\._./\6/\._./\11/\._./\2/\._./\9/\._./\ 8*13 You Make
    say  0*12( º )0*6 ( ° )0*11 ( ¤ )0*2 ( º )0*9 ( ° )0,0.8 *13 Me So
    say   12()~13H12~()6()~13E6~()11()~13L11~()2()~13L2~()9()~13O9~() 8*13 Hoppy !!!
    say   12(_)-(_)6(_)-(_)11(_)-(_)2(_)-(_)9(_)-(_) 8*13 $$1
  }
  ..Welcome, enjoy your stay:me 7Say's 12Hello 13WeLcoMe 9TO4 12EnJoY 7yOuR 1StAy 13 $1
  ..Enter room:me  12enters the room 4::trip:: 12sorry 4::squelch:: 12who's that on the floor? 4::crunch:: 12oops, nice shoes though..4::Bump:: 12excuse me.. 4::squish:: 12ewwwwww..4 ::shove:: 12sorry, my fault.. 4::squeeze:: 2WOW!!13 $$1 2, Gimme a Hug!!..12ahem, 4hi everyone!!!!
  ..Ooh excited:{
    me  6 Looks And Who do I see It's 12 $$1  
    me 6 Runs as fast as I can, slips, rolls across the floor 
    me 6 looks up into the dreamy eyes of 12 $$1  
    me 6 says well don't just stare at me 12 $$1  6 help me up will you...........<wink>  
  }
  ..Greeting Wave:/me  3(¯`·.¸.·<13¤3·.¸.·<8¤3·.¸.·<13¤3·.¸.·<8¤13 Hi $$1 8¤3>·.¸.·13¤3>·.¸.·8¤3>·.¸.·13¤3>·.¸.·´¯)
  ..Hi ya:me 11,2 «æ§æ» 8«æ§æ» 13«æ§æ» 9«æ§æ»8 Hi $$1 9«æ§æ» 13«æ§æ» 8«æ§æ» 11«æ§æ»
  ..Hi:me 1 0,1 1¤2¤5¤6¤3¤7¤14¤10¤12¤4¤13¤9¤¤11¤¤8¤¤08 $$1 0,18¤¤11¤¤9¤¤13¤0,14¤12¤10¤14¤7¤3¤6¤5¤2¤1¤
  ..Special Friend:me  <2S<3p<4e<5c<6i<7a<13l< 12 $$1 >2F>3r>4i>5e>6n>7d>
  ..Im Here!:me 3COMES THUMPIN DOWN THE HALL.....4<<<<RRIIPPPPPP>>>>> 7 oops,sorry about the door!......I can fix it! 4 <<hammer>> ((((bang bang)))<<hammer>> 10....(((ThUmP))) bugga...ahhh there we are!.....4RIGHTEEEO YOU LOT...13 ummm.... I'm here !!!!!!!! hehehehehe
  ..Bad Mood hi :me 4is in a bad mood today! {{{shove}}} 7 git outta the way <<<punch>>> 10that`ll teach ya (((kick))) 6 get up [[[whack]]] 3dont you start {{{slam}}} 4.....I feel better now..... 13hello!!! 
  ..Giggles HI:me 1,9 Hi 1,8:)16,4 Hi 1,8:)1,11 Hi 1,8:)1,160,4 » $$1 0,4« 1,8:)1,13 Hi 1,8:)16,4 Hi 1,8:)1,12 Hi 
  ..G'day:me says "G'day $$1,  how are ya?"
  ..What:/me says "What's happenin' $$1?"
  ..Wave:/me waves across $active   to $$1
  ..Handshake:/me does a long, complicated handshake with $1
  ..How R Ya:/say 1,1 2,2 3,3 4,4 5,5 6,6 7,7 8,8 9,9 10,10 11,11 12,12 13,13 14,14 15,15 16,16 15,15 14,14 14,1 HI4 $1 14!!!!! How are ya??? 14,14 15,15 16,16 15,15 14,14 13,13 12,12 11,11 10,10 9,9 8,8 7,7 6,6 5,5 4,4 3,3 2,2 1,1 
  ..Look:/me 4o00o1_12(c)2¿12(c)1_4o00o1_ 12 Look its $$1 1_4o00o1_ 12(c)2¿12(c)1 _4o00o1_
  ..WooHoo!:/ me says11,1 wooohooo****¶¶¶ººº13,1 $$1 11,1****¶¶¶ about time you got here!
  ..hello:say   0,12 H 0,4 e 0,3 l 1,8 l 1,9 o 0,13 . 0,12 . 0,12 $$1 !!
  ..Mwahh:say      9,1--'-<-,-{4,1@4,0* MWAHH $$1 *4,1@9,1}-'->-,-- | /say  9,1--'-<-,-{4,1@4,0* MWAHH $$1 *4,1@9,1}-'->-,-- | /say  9,1--'-<-,-{4,1@4,0* MWAHH $$1 *4,1@9,1}-'->-,-- 
  ..hello everybody:say      1,4H1,8e1,9l1,11l1,13o1,4 1,8E1,9v1,11e1,13r1,4y1,8b1,9o1,11d1,13y1,4 1,8-1,9 1,11G1,13o1,4o1,8d1,9 1,11t1,13o1,4 1,8S1,9e1,11e1,13 1,4Y1,8a1,9 1,11!1,13!
  ..Hey:say      12,1-H---=>1,12 $$1 12,1<=----=>1,12 $$1 12,1<=---Y- | /say  13,1-E---=>0,13 $$1 13,1<=----=>0,13 $$1 13,1<=---O- | /say 12,1-Y---=>1,12 $$1 12,1<=----=>1,12 $$1 12,1<=---U-
  ...ImBack:me says 3,1==4(9¯12`·.4_ 4(9¯12`·.4_3== 0,4 I'M BACK 3,1==4_12.·`9¯4)4_12.·`9¯4)3==
  ...welcome:say      0,12 W 0,4 e 0,3 l 1,8 c 1,9 o 0,13 m 0,12 e 13,1***   $$1 !!!
  ...Welcome Wacky world:me 1,1--------9 \\|//1------------4welcome to $chan 1---- | me  1,1---------3(8o o3)1,1-------------4the friendly channel1--- | me  4,1 ~~3oOOo4~3(_)4~3oOOo4~~01,1--4**8 $$1!!!! 4** | me 4,1 Welcome to our WaCkY World...Join in if you dare!!!
  ...welcome back:say      9,1 ¨°º ¨°º ¨°º WELCOME BACK $$1 º°¨ º°¨ º°¨ | /say  13,1 ¨°º ¨°º ¨°º WELCOME BACK $$1 º°¨ º°¨ º°¨ | /say  9,1 ¨°º ¨°º ¨°º WELCOME BACK $$1 º°¨ º°¨ º°¨
  ...welcome box:say     4,4 $$1  | /say  8,4 $$1 1,1!1,0 1 Welcome | /say 4,4 $$1 1,1!
  ...Ride:say    1,8 says Welcome Back $snicks $+ ...Bumpity, bump...yeee haaa!!! What a ride huh?
  ...Welcome Back!:me 12,14--11--==13>3>8>13>13 WELCOME BACK $1 13<8<3<13<11==--12--   
  ...HowdyHowdy:me 4 Howdy howdy howdy  $$1 2 Howdy howdy howdy  0,4 $$1 3 Howdy howdy howdy  0,4 $$1 7 Howdy howdy howdy  0,4 $$1 7Howdy howdy howdy  (G'day)
  ...Howdy all :say     4,4 howdy all! | /say 0,4 howdy all 1,1! | /say 4,4 howdy all 1,1! | /say 0,0 1,1 howdy all!
  ...HiAll:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 HELLO EVERYBODY 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ...HI:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 HI!! $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ...Welcome:say      7Say's 12Hello $1 13WeLcoMe 9TO4 $chan 12EnJoY 7yOuR 1StAy
  ...ChanBear:say     1,1..11,1¸,.»12¬=13椺.. 4## Welcome ##13 ..º¤æ12=¬11«.,¸1,1... | msg #  1,1.13{~0.13_0.13~} 11{~0.11_0.11~} 8{~0.8_0.8~} 9{~0.9_0.9~} 12{~0.12_0.12~}1,1. | msg #  1,1. 13( 7° 13) 1. 11( 7° 11) 1. 8(7 °8 ) 1. 9(7 °9 ) 1. 12(7 °12 )1,1.. | msg #  1,1.13()¯0H13¯() 11()¯0E11¯() 8()¯0L8¯() 9()¯0L9¯() 12()¯0O12¯()1,1. | msg #  1,1.13(_)-(_) 11(_)-(_) 8(_)-(_) 9(_)-(_) 12(_)-(_)1,1. | msg #  1,1______11,1¸,.»12¬=13椺 4 $$1 13 º¤æ12=¬11«.,¸1,1_______
  ...Hey, good to see you:say      12 Hey 9(©¿©)3{©¿©}4(©¿©)12 $$1 10(©¿©)13{©¿©}5(©¿©) 12good to see ya!
  ...G'day:say     12says G'day to 3{4{5{6{7{10{12{13{3{4{5{6{7{4 $$1 7}6}5}4}3}713}12}10}7}6}5}4}3}
  ...How r u?:say     1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3! 12HOW  ARE  YOU4 $$1 12? 1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!14!1!2!3!4!5!6!7!10!12!13!
  ...PrettyHi:say     0,1 12563714101241391180,1 Hi 0,18119130,14121014736521 | msg #  0,1 12563714101241391180,1 $$1 ! 0,18119130,14121014736521
  ...RoseHi:say     3»13®3»13®3»13®3»12{H}8*12{I}8*3»13®3»13®3»13®12{T}8*12{O}3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»12 $$1 3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»13®3»
  ...Evening all:say     1,1--13,13--1,1--13,13--1,1--13,13--1,1--11,1 Evening All 1,1--13,13--1,1--13,13--1,1--13,13--1,1-- | msg #  13,13--1,1--13,13--1,1--13,13--1,1--13,13--11,1 Evening All 13,13--1,1--13,13--1,1--13,13--1,1--13,13--
  ...YGRASS HI:say     9,1/|\/|\/|\/|\/|\/|\1 $$1 9/|\/|\/|\/|\/|\/|\ | msg #  9,1\|/\|/\|/\|/\|/\|/8 $$1 9,1\|/\|/\|/\|/\|/\|/ |  /say 1,1-8H1--8E1--8L1--8L1--8O1--8!1-1 $$1 1,1-8H1--8E1--8L1--8L1--8O1--8!1§ | msg #  9,1/|\/|\/|\/|\/|\/|\8 $$1 9/|\/|\/|\/|\/|\/|\ | msg #  9,1\|/\|/\|/\|/\|/\|/1,1 $$1 9,1\|/\|/\|/\|/\|/\|/ 
  ...RHIYA:say     4,1 §ø8Hiya4§ø§ø§ø§ø§ø1 $$1 4ø§8Hiya4ø§ø§ø§ø§ø§  | msg #   4,1 §ø§ø§ø8Hiya4§ø§ø§ø8 $$1 4ø§ø§ø§8Hiya4ø§ø§ø§  | msg #  4,1 §ø§ø§ø§ø§ø8Hiya4§ø1 $$1 4ø§ø§ø§ø§ø§8Hiya4ø§1§
  ...XHI:say     8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O 1 $$1 8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O  | msg #  1,1 4¤¤ 8E4 ¤¤¤ 8L4 ¤¤ 8 $$1 4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤  | msg #  1,1 4¤¤¤¤¤ 8L4 ¤¤¤¤¤ 1 $$1 4,1 ¤¤¤¤¤ 8L4 ¤¤¤¤¤  | msg #  4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤ 8 $$1 4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤1,1  | msg #  8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O 1 $$1 8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O1§
  ...PGRASSHI:say     9,1 \|/\|/\|/\|/\|/\|/1,1Hi. $+ $1 $+ 9,1\|/\|/\|/\|/\|/\|/  | msg #  1,1h9«13ºôº9» «13ºôº9» «13ºôº9» 13,1Hi1,1.13 $+ $1 $+  9,1 «13ºôº9» «13ºôº9» «13ºôº9»  | say 9,1 /|\/|\/|\/|\/|\/|\1,1Hi. $+ $1 $+ 9,1/|\/|\/|\/|\/|\/|\1§
  ...HIRBW:say     0,1o\4@0/oo\4@0/oo\4@0/oo\4@0/o1Hi. $+ $1 $+ 0o\4@0/oo\4@0/oo\4@0/oo\4@0/o | msg #  9,1»|«  »|« »|«  »|«  »|« 4Hi1,1.4 $+ $1 $+ 9 »|«  »|«  »|« »|«  »|« | msg #  0,1o/4@0\oo/4@0\oo/4@0\oo/4@0\o1Hi. $+ $1 $+ 0o/4@0\oo/4@0\oo/4@0\oo/4@0\o
  ..GREEN hi:say     9,1 »0|9«  »4©9«  »0|9«  »4©9«1  Hi $$1  9»4©9«  »0|9«  »4©9«  »0|9«  | msg #  9,1 »4©9«  »0|9«  »4©9«  »0|9«4  Hi $$1  9»0|9«  »4©9«  »0|9«  »4©9«  | msg #  9,1 »0|9«  »4©9«  »0|9«  »4©9«1  Hi $$1  9»4©9«  »0|9«  »4©9«  »0|9«1§
  ...RAINROLL:say     6,1©©©©© Hi $$1 !! ©©©©© | msg #  12,1©©©©© Hi $$1 !! ©©©©© | msg #  11,1©©©©© Hi $$1 !! ©©©©© | msg #  3,1©©©©© Hi $$1 !! ©©©©© | msg #  8,1©©©©© Hi $$1 !! ©©©©© |  /say 4,1©©©©© Hi $$1 !! ©©©©©
  ...HI NICKPINTURQ:say     13,1  \|/ \|/ \|/11 Hi1,1.11 $+ $1 $+ !13 \|/ \|/ \|/  | msg #  11,1  -*- -*- -*- Hi1,1.11 $+ $1 $+ ! -*- -*- -*-  | msg #  13,1 /|\ /|\ /|\ 11Hi1,1.11 $+ $1 $+ !13 /|\ /|\ /|\1 
  ...Red back hi:say     4,1©©©©©4,4 Hi $$1 4,1©©©©© | msg #  4,1©©©©©1,4 Hi $$1 4,1©©©©© | msg #  4,1©©©©©4,4 Hi $$1 4,1©©©©©
  ...GREEN BLACK HI:say     9,1©©©©©9,9 Hi $$1 9,1©©©©© | msg #  9,1©©©©©1,9 Hi $$1 9,1©©©©© | msg #  9,1©©©©©9,9 Hi $$1 9,1©©©©©
  ...Rose hi:say     0,1 »9}4®9{0«»«»«»«9}4®9{0«1,1 Hi $$1 0»9}4®9{0«»«»«»«9}4®9{0«  | msg #  0,1 »9}4®9{0«»«»«»«9}4®9{0«4 Hi $$1 0»9}4®9{0«»«»«»«9}4®9{0«  | msg #  0,1 »9}4®9{0«»«»«»«9}4®9{0«1,1 Hi $$1 0»9}4®9{0«»«»«»«9}4®9{0«1§
  ...RWCHEK HI:say     4,1 §04§§04§§04§§04§1,1 Hi $$1 4,1§04§§04§§04§§04§  | msg #  4,1 §04§§04§§04§§04§0,1 Hi $$1 4,1§04§§04§§04§§04§  | msg #  4,1 §04§§04§§04§§04§1,1 Hi $$1 4,1§04§§04§§04§§04§1§
  ...HI BUTTERFLY:say      8,1 (\o/) 9»4®9« 8(\o/)4  Hi $$1 8(\o/) 9»4®9« 8(\o/)  | msg #   8,1 (/|\) 9»4®9« 8(/|\)4 Hi $$1 8(/|\) 9»4®9« 8(/|\)1§
  .Everything Bye
  ..dreams:me 06wishes01 $$1 06a 13Good Night 06and 13Sweet Dreams06! 
  ..Hate U to leave!:me HãTè$ Tö sèè $$1 lèãVè!!! ߥèèè {{{{ $$1 }}}} !!!
  ..Do you have to go:me  12grabs4 $$1 12by the ankles and says 9,12 I am crying my eyes out over you...do you really have to GO??? 12,8:5ö13(
  ..Bye, stay safe:me > 13,1:) 9:) 8:) 12:) 13:) 9:) 8:) 12:) 8Bye! Stay Safe $$113:) 9:) 8:) 12:) 13:) 9:) 8:) 12:)
  ..AFTernoon:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 GOOD AFTERNOON $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ..Bye:/me runs over to $$1 and gives Big Goodbye Huggles!! ;)
  ..Bye All:/me runs around the room.. giving huggles to all.. bye bye ;)
  ..Evening:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 GOOD EVENING $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ..NITE:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 GOOD NIGHT $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ..goodnight-:say     11,2G10*11O10*11O10*11D10*11N10*11I10*11G10*11H10*11T10*11 -{4 $$1 $2 $3 $4 $5 $6 $7 $8 $9 $10 11}-
  ..Good Night:say     12says 9,12Good night $$1 12May you have pleasant dreams!!!  9ø12¤°`°¤9øø12¤°`°¤9øø12¤°`°¤9ø12  $$1  9ø12¤°`°¤9øø12¤°`°¤9øø12¤°`°¤9ø
  ..Nitey nite:say     4,1©©©©©©8,4  $$1 4,1©©©©©© 4 Nitey nite!!!
  ..spell poka dot lampshade:say     waves at $$1s with complex motions and changes his appearance. | say     turns $$1s into a really cool purple lamp shade with pink polka dots.
  ..cow suit horny bull:say     gags $$1s and stuffs $$1s in a Cow suit, then tosses $$1s into a corral with a horny bull.  <<Mmmmmoooooo!>>
  ..Sweet Dreams:{ describe  $active  14¤ 2Sweet Dreams 6for10 $$1 $+ 14 ¤ }
  ..Live Long And Prosper:{ describe  $active  14¤ 2Live Long And Prosper6,10 $$1 $+ 14 ¤ }
  ..Good Night:{ say 14¤ 2Good Night,10 $$1 $+ 14¤ }
  ..Goodnight:/me 11,2G10*11O10*11O10*11D10*11N10*11I10*11G10*11H10*11T10*11 -{4 $$1 $2 $3 $4 $5 $6 $7 $8 $9 $10 11}-
  ..Bye, come back soon:me  15,15-14,14-1,1.8,124,17says17,17 25,17bye17,17 11,17and17,17 13,17¤°`°¤17,17 32,17I'll17,17 24,17ø¤°`°¤ø17,17 25,17Miss17,17 11,17ø¤°`°¤ø17,17 13,17Ya17,17 20,17ø¤°`°¤ø17,17 24,17sob17,17 32,17ø¤°`°¤øsobø¤°`°¤17,17 25,17Come17,17 13,17back17,17 20,17soon17,17 11,17soon17,17 24,17!!!17,17 25,17ø¤°`°¤1,1.14,14-15,15-
  ..CYA:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 CYA!! $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ..BYE:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,6 BYE!! $$1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,11 0,12 0,13 0,14 0,15 
  ..Nighty Night:me 4,1 Ñ7ï8G9H11T13ÿ 4Ñ7ï8G9H11T13,1 $$1 4,1§7W8é9ê11T13 Ð4R7è8å9M11§   
  ..Don't Leave Me:me looks at 4 $1 $+ !!! 13PLEASE 12don't leave me here all alone!!!!!!!!!!!!7 this room12 seems so 3vacant12 without you!!!!!!!!6 If you leave, I won't have6 ANYBODY 12to talk to!!!!11 Ohhhhhhhhhh,13 I am sooooooo 4scared 12now. I have this fear of 7abandonment. 6PLEASE HELP!!!! 4 12, I need ya! 4 12I implore you to 4STAY!!!! 12(BUT HEY, IF YOU GOTTA GO, SEE YA LATER)4,8 :)
  ..G'nite my love:me 12walks slowly over to 4 $1 ...7 sadness on my face and a 13small tear falling from my eye...10 I reach out and hold your face in my hands looking into your eyes...12" watching you leave always breaks my heart"...13 I lean forward and kiss you softly as the tear rolls down my cheek...12 goodnight my love... 4:
  ..real World Calling:say     13AWWWW 12 $1 has gota go...........The  real world is calling........13,15WISH IT WOULDN'T DO THAT!!!!!!
  ..goodbye:me 12G5®5®5Ð3B12¥6ë4 $$1 6§6ë6ë12¥2å
  ..Poof:{
    say    0--------12 \0--4 * 12/ 
    say    0-------12.0--4* * *12 . 
    say    0-----13 -=* 12P O O F!13 *=- 
    say    0------12.0--12.4* * * 12 . 
    say    0--------12/0---4*0--12\
  }
  ..Wont Miss Ya:say    4,1Have fun13 $$1 4Dont hurry back lol  
  ..Scram:me 12Dont let the door smack ya on the butt on the way out4 $$1 $+ 12!!!! 6hehehe 
  ..WavesBye:say     9 (¯`·.,¸¸.·´¯`·.¸¸.->2 Waves 4 <-.¸¸.·´¯` ·.¸¸.·´¯) 10 Goodbye 12 ¸.·'´¯)¸.·'´¯) 14 to13 (¯`'·.¸(¯`'· . 4 Everyone3¸.·'´¯)¸.·'´¯)¸.·'´¯)8 (_¸.·'´(_¸.·'´ 3and 12`'·.¸_)`'·.¸_)¸)¸.·' ´¯) 13Says 9(_¸.·'´(_¸.·'·'12 I'm 4(¯`'·.¸ ¸.·'´¯)¸.·'´¯) `'·.¸_)6gonna 9`'·.¸_) * (¯`'·.¸(¯`'¸ 10 Miss 13¸.·'´¯) (¯`'·.¸(¯`'·.¸ 4~~you~~ 9 .·'´¯)¸.·'´¯) 3Bye-Bye 12¸.·'´¯)¸.·'´¯)* 6(_¸.·'(¯`' ·.¸ ¸.·'´¯) 4 for Now 13`
  ..BackSoon:say     15,15-14,14-1,1.8,124,17says17,17 25,17bye17,17 11,17and17,17 13,17¤°`°¤17,17 32,17I'll17,17 24,17ø¤°`°¤ø17,17 25,17Miss17,17 11,17ø¤°`°¤ø17,17 13,17Ya17,17 20,17ø¤°`°¤ø17,17 24,17skewll17,17 32,17ø¤°`°¤ø((Huggers))ø¤°`°¤17,17 25,17Come17,17 13,17back17,17 20,17soon17,17 11,17skewll17,17 24,17!!!17,17 25,17ø¤°`°¤1,1.14,14-15,15-
  ..WrapsU:say     13>>>>>>>>>>>>>>-----Wraps-----<<<<<<<<<<<<<< | say     4(¯`'·.¸(¯`'·.¸10 _____________ 4¸.·'´¯)¸.·'´¯) | say     4(¯`'·.¸(¯`'·.¸10 4 ¸.·'´¯)¸.·'´¯) | say     12---==>>>>------>13 $$1 12<------<<<<==--- | say     4(_¸.·'´(_¸.·'´ 104 `'·.¸_)`'·.¸_) | say     4(_¸.·'´(_¸.·'´10 ¯¯¯¯¯¯¯¯¯¯¯¯¯4 `'·.¸_)`'·.¸_) | say     13>>>-----In a Warm Friendly Embrace!-----<<<
  ..GoodNi:say     0,1 ø . : /1,1ii0~.° \1,1ii0GOOD NITE0,1 8¤·:. | say     0,1 2*.6¤1,1ii0 \1,1iii0O1,1ii0/0,* ³0 $$1 !!! 1,1ii | say     0,10,1 ³ ° ¤· 0°,¸¸,°0,1 5:.*0 . 7¤ø0 °· 2³ 4,0*
  ..ComeBack:say     say's "Thanks for chatting with us  $$1 ! Come back again real soon !"   :)
  ..NiteNite:say     4,1©©©©©©8,4 $$1 4,1©©©©©© 4 Nitey nite!!!
  ..BackSoons:say     15,15-14,14-1,1.8,124,17says17,17 25,17bye17,17 11,17and17,17 13,17¤°`°¤17,17 32,17I'll17,17 24,17ø¤°`°¤ø17,17 25,17Miss17,17 11,17ø¤°`°¤ø17,17 13,17Ya17,17 20,17ø¤°`°¤ø17,17 24,17 $$1 17,17 32,17ø¤°`°¤ø((Huggers))ø¤°`°¤17,17 25,17Come17,17 13,17back17,17 20,17soon17,17 11,17 $$1 17,17 24,17!!!17,17 25,17ø¤°`°¤1,1.14,14-15,15-
  ..SeeYa:say     3hugs 9{11 [9 {11 [9 {11 [9 {11 [13 $$1 11]9 }11 ]9 }11 ]9 }11 ]9 }3 and says, take care....13I'll miss ya!
  ..SweetDreams:say     12says 9,12Good night $$1  12May you have pleasant dreams!!! 9ø12¤°`°¤9øø12¤°`°¤9øø12¤°`°¤9ø12 $$1 9ø12¤°`°¤9øø12¤°`°¤
  ..Niters and god bless:say     1says Niters12 $$1 !!!1 Hugs and GodBless !!!
  ..okBye:say     4~*{%{*{ 12ok...I luv ya..buh,bye $$1 !! 4 }*}%}*~ 
  ..Nite everyone:say     1,1*********************************************** | say     1,1*****13,1{~11.13_11.13~}1,1**********************************   | say     1,1******13,1( 0Y13 )1,1****9 Night everyone !1,1*************** | say     1,1*****13,1()9~11*9~13()1,1*****9Take care......God Bless !1,1**** | say     1,1*****13,1(_)-(_)1,1*********************************** | say     1,1*********************************************** 
  ..Bye Miss:say     3hugs 9{11 [9 {11 [9 {11 [9 {11 [13 $$1 11]9 }11 ]9 }11 ]9 }11 ]9 }3 and says, take care....13I'll miss ya!
  ..Bye Bye:say     0,12 0,15 0,1 0,1518=4=2=2»4»8»12»8=4=2=2»4»8»12»14 1Bye Bye $$1 412«8«4«2«2=4=8=12«8«4«2«2=4=8=0,150,1 0,15 0,12 0
  ..Block Bye:say     0,3    0,7    0,8    0,9    7,3 Byeee! $$1 0,9    0,8    7,7...       0,3      at $$1 2(*)(*)
  .Sweet Stuff
  ..Toss confetti
  ...Confetti:me tosses Confetti around the room4" '~`*+,. 3`*`'" ~ :, 9, ~ ' " -;. 11, : ' ` ` * 12- _ + ^ : 13,, .+ * ^ * - ,14.:" '~`*+,. 15`*`'" ~ :, 6, ~ ' " -;. 7, : ' ` ` * 8- _ + ^ : 9,, . + * ^ * - , 10.:" '~`*+,.Just For $$1
  ...balloons:me tosses  CONFETTI & BALLOONS in the air for $$1  1~'O~~~*`;.' 2O~~~~*`;.'`~;`~`3O~~~~*` ;.'4O~~~~*`;.'`~;`~`5O~~~*`;.'6O~~~~*`;.'`~;`~`7O~~~~*`;.' 4O~~~~*`;.'`~;`~`9O~~~*`;.'10O~~~~*`;.'`~;`~`11O~~~~*`;.'12O~~~*`;.'`~;`~`13O~~~~*`;.'4O~~~~*`;.'`~;`~`1O~~~~*`;.'4O~~ ~~*`;.'`~;`~`5O~~~~*`;.'`~3O~~~~*`;.'`~;`~`4O~~~~*`;.'`~3O
  ...Showers confetti:me showers $$1 with confetti4~;``*~`;.~``~`* $$1 3~`;. ~`;.*`;.`~`~`*~` ;.~ $$1 12`;.*`;.`~;.*`;.`~;`~ $$1 4`*~` ;.~` $$1 ;.*`;.``~`*~`;. $$1 3~``~` *~`;.~`;.*`;. $$1`~`~`*~` ;.~`;.*`~``;.~`;.*```* 12 $$1 ~`; ~``~`*~`;
  ...Star Dust:me pulls out a bag of stardust and sprinkles $$1 $+ 15 `,',',',',',','.',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',','
  ...glitter:me 8,12 sprinkles glitter all over $$14,12 ¯°¹ª°¯°¹ª°¯°¹ª°¯°¹ª°¯°¹8,12 $$1 4,12ª°¯°¹ª°¯°¹ª°¯°¹ª°¯°¹ª°¯°¹8,12 $$1 4,12ª°¯°¹ª°¯°¹ª°¯° ¹ª°¯°¹ª°¯°¹8,12 $$1 4,12ª°¯°¹ª°¯°¹ª°¯°¹ª°¯°¹ª°¯°¹ 
  ...Fireworks:me shoots fireworks for $$1  4*15~~~~~5$active  $active  $active  $active  $active  2> 4*15~~~~~5$active  $active  $active  $active  $active  2> 4*15~~~~~5$active  $active  $active  $active  $active  2> 4*15~~~~~5$active  $active  $active  $active  $active  2> 4*15~~~~~5$active  $active  $active  $active  $active  2> 4*15~~~~~5$active  $active  $active  $active  $active  2>
  ...Shower with roses:me showers you with roses 4@3}-12>--·´¯`·.¸¸.·´¯`· .¸.·´¯`·.¸.--12<-3{4@ 4@3}-12>--.·´¯`·.¸¸.·´¯`·.¸--12<-3{4@ 4@3}-12>--¸.·´¯`·.¸. ,  .,,.·´¯`·.--12<-3{4@ 4@3}-12>--`·.¸¸.·´¯`·.¸ .·´¯`·.¸--12<-3{4@ 4@3}-12>-- .¸¸.·´¯`·.¸.·´¯`·.¸. ¸.·´¯`·.¸¸.·´¯`·.¸--12<-3{4@ 4@3}-12>--.·´¯`·.¸¸.·´ ¯`·.¸.·´¯`·.¸¸.·´¯`·.--12<-3{4@ 4@3}-12>--¸.·´¯`·.¸ :)
  ...LetSnow:say     says let it snow....0,12:;~`:.,:*~ $$1 ~:;`:.,:*~:Let;~It~Snow~`::*~:;`:.,:*~:;~`:. $$1 ,:*~:;~`:.Let~It~Snow,:*~:;~`:.,:*~:;~`:.,:*~:;~Let~It~Snow:.,:*~:;~`:.,:*~:;~`:. ,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.,:*~:;Let~It~Snow:.,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.Let~It~Snow,:*~:;~`:.,:*~:;~`:.,:*~:;~`:. $$1,:*~:;~`:.,:*~~:;`:.,:*~:;~`:.,:*~:;Let~It~Snow:.,:*~:;~`:.,:*~:;~
  ...snowbound:say     0,15;`~,.*'',,'*,;*'~,'*"^~' ;* `,~;`*,`^';`~,.*`":,` `,~;`~`:,.`*':', `;* `,~;~`:13 $$1 13you are snowbound, now put your feet up and here's your blankie and your 5hot cocoa8!0,15 , .`*':',`;* ',~;`*,`^';`~,.* '`" :,` `,~;`~`:,.`*':',`;* `,~;~`:,.`  *'`":,` .*',`;*`,~;`*,` ^';`~,.*'',` ~~ ;* `,~;`*,`^';`~,.*' `":,` `,~;`~`:,.`*':',`;* `,~;~
  ...Lights:me 12drapes2 $$1 12with Party lights 2°*.¡.*°*.¡.*°6°*.¡.*°*.j.*°7°*.¡.*°*.¡.*°9°*.¡.*°*.¡.*°10°*.¡.*°*.¡.*°13°*.¡.*°*.¡.*°4°*.¡.*°*.¡.*°8°*.¡.*°*.¡.*°3°*.¡.*°*.¡.*°11°*.¡.*°*.¡.*°12°*.¡.*°*.¡.*°2°*.¡.*°* .¡.*°6°*.¡.*°*.¡.*°7°*.¡.*°*.¡.*°9°*.¡.*°*.¡.*°10°*.¡.*°*.¡.*°13°*.¡.*°*.¡.*°4°*.¡.*°*.¡.*°8°*.¡.*°*.¡.*°3°*.¡.*°*.¡.*°11°*.¡.*°*.¡.*°
  ...CarpetPetals:me 12 lays down a carpet of rose petals for4`~,.*',`~,.*',`;* `,~;`*,`^';`~,.*'`":,``,~;`~`:,.`*':',`;* `,~;~`:,`*':',`;* ',~;`*,`^';`~,.*'`":,`~,.*',`;* `,~;`*,`^';`~,*'`":,` `,~;`~`:,.`~,.*',`;* `,~;`*,`^';`~,.*'`":,` `,~;`~`:,.`12 $$1 4`,~;`~`:,.`*':',`;* `,~;~`:,.`*':', `;* `,~;`*,`^';`~,.*'`":,` `,~;`~`:,.`*':  `~,.*',`;* `,~;`*,`^';`~,.*'`":,` `,~;`~`:,.`~,.*',`;* `,~;`*,`^';`~,.*'`":,` `,~;`~`:,.',.*'`":,` `,~;`~`:,.'
  ..Aww cute!
  ...arw heart!:me 9,1  »»---13(¯`°´¯)-9 $$1 13-(¯`°´¯)9---13»» | say      9,1  »»---13(¯`°´¯)-4 $$1 13-(¯`°´¯)9---13»» | say      9,1  »»---13(¯`°´¯)-11 $$1 13-(¯`°´¯)9---13»» 
  ...Color Nick:say     4,144 7_8_9_11.13*4°7.12*1,1 $$1 12*7.4°13*11.9_8_7_1. | say      4,144 7.8*9´11.13*4´4 - $$1 - 4`13*11.9`8*7.1. | say      4,144 7:7 8:8 8 8 12 - $$1 - 8 8 8 8:9 7:1. | say      4,144 7`8*9.11`13*4.9 - $$1 - 4.13*11´9.8*7´1. | say      4,144 7¯8¯9¯11`13*4.7`12*1,1 $$1 12*7´4.13*11´9¯8¯7¯1.l   
  ...Chains:me 14®=®=®=®=®=®=®=®=®=® 12 chains 14®=®=®=®=®=®=®=®=®=® 12 herself 14®=®=®=®=®=®12 to 14®=®=®=®=®=®12 $$1! 14®=®=®=®=®=®12 any 14®=®=®=®=®=®12 objections ?
  ...squirrel:say     5 __0 5 (\_ 4 I am | say     5(_ \ (1 '5> 4Just sooo | say     0 5  ) \/_)=  4NuTz about | say     0 5 (_(_ )_ 4YoU12 $$1
  ...Heart rose:say     1,14 (¯`'·.¸(¯`'·.¸  9---{4@1. $+ $1 $+ .4@9}---  4¸.·'´¯)¸.·'´¯)1- | say     4,1 (¯`·._(¯`·._(¯`·._1,1...4 $+ $1 $+ 1,1...4_.·´¯)_.·´¯)_.·´¯)1- | say     4,1 (_.·´¯(_.·´¯(_.·´¯1,1...4 $+ $1 $+ 1,1...4¯`·._)¯`·._)¯`·._)1- | say     1,14 (_¸.·'´(_¸.·'´  9---{4@1,1.  $+ $1 $+ .4@9}---  4`'·.¸_)`'·.¸_)1§
  ...candles:me 13 lights up the room 4Ì8Ì11Ì6Ì13Ì12Ì5Ì9Ì14Ì10Ì2Ì4Ì8Ì11Ì6Ì13Ì12Ì5Ì9Ì14Ì8Í11Í6Í13Í12Í5Í9Í14Í10Í2Í4Í8Í11Í6Í13Í12Í5Í9Í14Í  13 awww aint they pretty!!!
  ..Angels
  ...Fairy dust:me   4welcomes10 $$1 6with 13a 12shower 9of 4fairy 2dust 4` ~ ,. * ' 8, ` ; * ` , ~11 ; ` * , ` ^ `13 ; ' ~, . ' ` " 2: ` ' , ; ~ ` * ^12 ; ' , . ~ ; * ' 6. ; , ~ ` ^ * ~ 15` ^ ; . , ` * 9; , . ` * ' ; ` ~ *11 * ~ ` ; . , ' ; ' .13 , ` * ~ * ` ~ 8; . , ' ' ` ~ . , ~ * 6and 13says 9make 7a 5wish !!!!!
  ...AdjustHalo:me 13adjusts her halo 14`~12Å14~.:**.:`~12Å14~.:* *.:`~12Å14~.:**.:`~12Å14~.:*~12Å14~.:`**.:`~12Å14~,.:`**.:` ~12Å14~. :`*13basically looking angelic...14*.:`~12Å14~.: `* `~12Å14~.:`**.: ~12Å14~.:`**.:~12Å14~,`~12Å14~.:**.:`14`~12Å14~.:**.:`14`~12Å14~.:**.:`14`~12Å14~.:**.
  ...Send angels:me sends angels to watch over $$1 14~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ ~Å~ 
  ...Angelhug:me  10 angel hug 13 ^Å^^Å^^Å^{{{*10 $$1 13*}}}  ^Å^^Å^^Å^
  ...Angels:me 13places smiling peaceful 12Angels12 all around4 $1 $+ 's neck 10~7Å10~~7Å10~~7Å10~ ~7Å10~~7Å10~~7Å10~ ~7Å10~~7Å10~7Å10~
  ..Bubbles
  ...Champagne:me showers  $$1  with champagne bubbles * * ,,ºº " ¤¤ ·· ºº ** ^^ ,, ¤¤¤ ** ^^ ,, « % »   $$1    ** ,, ^^ ¤¤¤* * ,,ºº " ¤¤ ·· ºº ,, ¤¤¤ * * ,,ºº " ¤¤ ·· ºº ,, ¤¤¤ * * ,,
  ...Bubbles:me blows bubbles at $$1 11© ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ®
  ...Blow bubbles:me 12 blows bubbles at 11°*°*°*°*°*°*°13°*°*°*°*°*°*°2°*°*°*°*°*°*°9°*°*°*°*°*°*°12°*°*°*°*°*°*°12 12°*°*°*°*°*°*°9°*°*°*°*°*°*°2°*°*°*°*°*°*°13°*°*°*°*°*°*°11°*°*°*°*°*°*°12 11°*°*°*°*°*°*°13°*°*°*°*°*°*°2°*°*°*°*°*°*°9°*°*°*°*°*°*° 12°*°*°*°*°*°*°12 12°*°*°*°*°*°*°9°*°*°*°*°*°*°2°*°*°*°*°*°*°13°*°*°*°*°*°*°11°*°*°*°*°*°*°
  ..Roses
  ...1 Dozen Mixed:me  16,6 hands $$1- a dozen Roses!   13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}---
  ...1 Dozen red:me 16,6 hands $$1- a dozen 4Red16,6 Roses!   4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}---
  ...1 Dozen Blue:me 16,6 hands $$1- a dozen 12Blue16,6 Roses!   12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}---
  ...1 Dozen black:me 16,6 hands $$1- a dozen 1Black16,6 Roses!   1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}---
  ...1 Dozen white:me 16,6 hands $$1- a dozen 0,6White16,6 Roses. 6,6 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'--
  ...1 Dozen yellow:me 16,6 hands $$1- a dozen 8Yellow16,6 Roses!   8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}---
  ...2 Dozen mixed:me 16,6 hands $$1- 2 dozen Roses!   13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}--- 13@9-}--}--- 8@9-}--}--- 4@9-}--}---
  ...2 Dozen red:me 16,6 hands $$1- 2 dozen 4Red16,6 Roses!   4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}--- 4@9-}--}---
  ...2 Dozen blue:me 16,6 hands $$1- 2 dozen 12Blue16,6 Roses!   12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}--- 12@9-}--}---
  ...2 Dozen black:me 16,6 hands $$1- 2 dozen 1Black16,6 Roses!   1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}--- 1@9-}--}---
  ...2 Dozen white:me 16,6 hands $$1- 2 dozen 0,6White16,6 Roses. 6,6 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 6,6 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'-- 16@9-}-,-'--
  ...2 Dozen yellow:me  16,6 hands $$1- 2 dozen 8Yellow16,6 Roses!   8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}--- 8@9-}--}---
  ..His Dream:me 4 slips into the sleeping part of the mind of714 $$1 4to4 weave together the kisses of his most passionate dreams and bring4 them into reality before slipping back into the shadows like a dream4 lover...To wait again till he sleeps.....
  ..I Love you:me 4 and $1 = Two Hearts Joined as One. 4»»--(¯`°´¯)-(¯`°´¯)---»» 4 I £ô¥É You Baby ;)
  ..Love!:say     9,1^ -_-^¤°`°¤ø14,1  ï £ðvê ¥ðü §ððð mü©h9,1ø¤°`°¤ø^-_-^ 
  ..thanks:me 0,1°º®§®º°4 Thanks! 0°º®§®º°4  $$1 0°º®§®º° 
  ..thank you:me 9,1 ---,-'-{4@ 8Thanks! $$1 4@9}-'-,--- 
  ..Hearts:say     4(¯`·._7(¯`·._10(¯`·._(12 $$1 10)_.·´¯)7_.·´¯)4_.·´¯) | say     4(_.·´¯7(_.·´¯10(_.·´¯(12 $$1 10)¯`·._)7¯`·._)4¯`·._)
  .Hugs
  ..hugs && kiss 1:me 12 ~*}{Üǧ13*Ñ*12kè§*~ 0,12 $$1  12 ~*kè§13*Ñ*12}{Üǧ*
  ..Hugs && Kiss:me 9,1§4«««9H8U13G11S4»»»9§4 $$1 9§4««13K8I4S11S7E9S4»»9§
  ..hugs:me 12,0 Hµg§ 13,0 Hµg§ 12,0 Hµg§ 13,0 Hµg§^  0,13 {{ $$1 }}  12,0 Hµg§ 13,0 Hµg§ 12,0 Hµg§ 13,0 Hµg§
  ..MegaHugs:me 4,1 M 1,4 E 4,1 G 1,4 A 4,1 §®º° $$1 °º®§ 1,4 H 4,1 U 1,4 G 4,1 S  | me 11,1 M 1,11 E 11,1 G 1,11 A 11,1 §®º° $$1 °º®§ 1,11 H 11,1 U 1,11 G 11,1 S  | me  10,1 M 1,10 E 10,1 G 1,10 A 10,1 §®º° $$1 °º®§ 1,10 H 10,1 U 1,10 G 10,1 S  | me  13,1 M 1,13 E 13,1 G 1,13 A 13,1 §®º° $$1 °º®§ 1,13 H 13,1 U 1,13 G 13,1 S  . 
  ..Special Person:say     9,2hugs this very SPECIAL person11,2 § § § § §13,2 $$1 11,2§ § § § § ¤4,2 ¤ § § § § §12,3 $$1 4,2§ § § § § ¤0,2¤ § § § § §4,2 $$1 0,2§ § § § § ¤ ¤
  ..Hugs and Kisses:say     3,1.4º©º3..4º©º3.0sends3.4º©º3.0hugs3.4º©º3.0and3.4º©º3 .0kisses3*.4º©º3.0to sweet3.4º©º3.0lovable3.4º©º3.0 $$1
  ..squeeze:me 12 loves to squeeze ¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^ 13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._¬ª*^ 13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12^*ª¬ tight!!!
  ..beary snuggles!:me 13 snuggles withoneofthose tickletackleslamyouupagainstthewallalmosbreakyourribsknockyoudowntotheflooranddareyoutogetuptillyourteethcomeflingoutofyourhead kinds of snuggles can you BEAR it!!!!
  ..keep U Foreva Hug:me 13{14=12^13;12^14=13} 4©13 $$1 4© 13{14=12^13;12^14=13} 10 You're So CUTE!!! | me 10 I'm gonna take you home and 13HUG YOU 10and 13SQUEEZE YOU | me 10and 13LOVE YOU 10and 13CAGE YOU 10and 13keep you 13FOREVER 10!! 
  ..Cuddles Close:me 6,15Cuddles closely in a sleeping bag on the beach with1,15 $1 . 6,15As we Watch the stars together we feel our love grow stronger between us as waves of passion and desire fill our hearts, We know our Love will never end.
  ..Snuggles:me 13snuggles up closely, wraps their loving Arms around4 $1 5and massaging 7 Bronzed Tanned Firm Body in all the 13Right Places!!!
  ..hugskiss:me 12 ~*}{Üǧ13*Ñ*12kè§*~ 0,12 $$1  12 ~*kè§13*Ñ*12}{Üǧ*
  ..bear!:me 13 {~._.~} | me 13 _( Y )_ | me 13 ()~*~()  12<------Here is a Teddy Hug for you $1 | me 13  (_)-(_) 
  ..BigHuggle:me 4,1 sends a9,1 Big...4,1Giant......12,1Extra 13,1Super 0,1Large.....8,1and 4,1Really 9,1nifty 12,1Huggle 0,1your 13,1way 11,1 $$1 9,1 :)
  ..smilehug:me 4©3©12©4©3©12©4©3©12©4©3©12©4©3©12©13 $$1 4©3©12©4©3©12©4©3©12©4©3©12©4©3©12©
  ..Hugs With Names:me 8,1 Hugs $1  11,1 $1 9,1 $1  13,1 $1  9,1 $1  11,1 $1  8,1 $1  11,1 $1  13,1 $1 *2 9,1 $1  11,1 $1  8,1 $1  11,1 $1  9,1 $1  13,1 $1  8,1 $1  11,1 $1  9,1 $1  
  ..HUG!:me 2 gives big hugs to 6(13©6¯`\.(13©6¯`·._(13©6)_.·´¯13©6./´¯13©6) (13©6¯`\.(13©6¯`·._(13©2 $$1 13©6)_.·´¯13©6)./´¯ (13©6¯`\.(13©6¯`·._(13© ^ ^ 13©6)_.·´¯13©6)./´¯13© 6(13©6¯`\.(13©6¯`·._(13©2 $$1 13©6)_.·´¯13©6)./
  ..Lady hugzz:say     9,1(¯`·._13,1(¯`·._4,1( {{ $$1 }} )13,1_.·´¯)9,1_.·´¯) | say     9,1(¯`·._13,1(¯`·._4,1( {{ $$1 }} )13,1_.·´¯)9,1_.·´¯) | say     9,1(_.·´¯13,1(_.·´¯4,1( {{ $$1 }} )13,1¯`·._)9,1¯`·._) | say     9,1(_.·´¯13,1(_.·´¯4,1( {{ $$1 }} )13,1¯`·._)9,1¯`·._)
  ..Circle Hug: me      13___11.13*5°4.9*11°12  °13*5.9°4*12.13___ | me     12.5*13´7.11*10´4  $+ $1 $+  10`11*7.13`5*12. | me 12 :13  :4   $1   13:  12: | me 4  `5*12.13`7*3.4 $+ $1 $+  3.7*13´12.5*4´ | me 13 ¯¯¯11`13*5.4`9*11.  11.9*4´5.13*11´13¯¯¯
  ..FITS ALL:say     4,144 7_8_9_11.13*4°7.8*9°9 9 9 9 11°8*4.7°13*9.11_13_4_1. | say     4,144 7.8*9´11.13*4´8 $$1 13*9.11`8*4.1. | say     4,144 7:7 8:8 8 8 8 $$1 8 8 9:9 11:1. | say     4,144 7`8*9.11`13*4.8 $$1 13*9´11.8*4´1. | say     4,144 7¯8¯9¯11`13*4.7`8*9.9 9 9 9 11.8*4´7.13*9´11¯13¯4¯1.

  ..Hug:{ say 3¤ 6A 2Big Beary Huuuuuuug 6For10 $$1 $+ 3 ¤ }
  ..Hug:/me gives4 $$1 12a13 HUGE 12:: 4HUG 12::4!
  ..Welcome:/me gives $$1 Big Welcome Huggles ;) 
  ..Missed:/say I've 13ø,¸¸,ø Missed 13ø,¸¸,ø You 13ø,¸¸,ø $$1 4*hug*
  ..Hugkiss:/me <h><u><g><s> $$1 <k><i><s><s><e><s>
  ..Squeeze:  /me 12 loves to squeeze ¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._ _.¬ª*^ 13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12 ^*ª¬._¬ª*^ 13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12^*ª¬._ _.¬ª*^13,8 $$1 12^*ª¬ tight!!!
  ..Love Bug:/me 1(4:1Ï4:1) 1(4:1Ï4:1)4 $$1 1(4:1Ï4:1) 1(4:1Ï4:1) 3 Love Bug Hugs 
  ..ColorHugz:/me 2øHugsø,¸¸,ø¤°`6° $$1  `°¤ø,¸¸,øHugsø,¸¸,ø¤10°`° $$1 `°¤ø,¸¸,ø¤°5`° $$1  `°¤ø,¸¸,ø¤°
  ..Hugz:/me Hugz $$1 *hug*
  ..Hug: /me gives $$1 a BIG BEAR HUG
  ..Hugs:/me . . . Heres Your 9,3 {H}*{U}*{G}  $$1 4 I sure do like you!
  ..Huggle:/me {{{{huggles}}}} $$1
  ..Cuddle:/me cuddles up closely to $$1
  ..SexyHug:/me 4,8thinks that you $$1 are very  6,1§èץ---§èץ §èץ-13,1- --§èץ §èץ---11,1 §èץ §èץ--8,1 --§èץ §èץ---§èץ §èץ--4,1 --§èץ §èץ--9,1-§èץ §èץ--12,1 --§èץ §èץ---§èץ §èץ--4,1 --§èץ §èץ ---§èץ §èץ--4,12  NO MATER HOW YOU COlOR IT!!!!
  ..Bear!:/me {~._.~} | /me _( Y )_ | /me ()~*~() <------Here is a Teddy Hug for you *1 | /me (_)-(_)
  ..Hµg§:  /me 12,0 Hµg§ 13,0 Hµg§ 12,0 Hµg§ 13,0 Hµg§^  0,13 {{ $$1 }}  12,0 Hµg§ 13,0 Hµg§ 12,0 Hµg§ 13,0 Hµg§
  ..Run Up:/me runs up to $$1 and gives him a warm {{{ hug }}}
  ...Breathe:/me hugs $$1 so tight and warm that $$1 has to tap $me on the shoulder and mention that $$1 has to breathe!  ..EMBRACE:me 13>>>>>>>>>>>>>>-----Wraps-----<<<<<<<<<<<<<<  | say     4(¯`'·.¸(¯`'·.¸10 _____________ 4¸.·'´¯)¸.·'´¯) | say     4(¯`'·.¸(¯`'·.¸10 4 ¸.·'´¯)¸.·'´¯) | say     12---==>>>>------>13 $$1 12<------<<<<==--- | say     4(_¸.·'´(_¸.·'´ 104 `'·.¸_)`'·.¸_) | say     4(_¸.·'´(_¸.·'´10 ¯¯¯¯¯¯¯¯¯¯¯¯¯4 `'·.¸_)`'·.¸_) | say     13>>>-----In a warm friendly embrace!-----<<<
  ..lites:say     We're hanging out the lights for $$1 !12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.! 12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*13°8*3.12¡4.9*
  ..hearts:say     * »»---4(¯`v´¯)= $$1 ==4(¯`°´¯)----»  | say     * »»----4`·¸·' - $$1 -- 4`·¸·' ----»  | say     * »»--.9*4°9***4°9*.  $$1 -.9*4°9***4°9*----»
  ..3 Line Hug:say     13°º©o11¿3,4@3,11¿10o©º°¨¨°º©13 $$1  10©º°¨¨°º©o11¿3,4@3,11¿13o©º° | say      13°º©o11¿3,4@3,11¿10o©º°¨¨°º©4 $$1  10©º°¨¨°º©o11¿3,4@3,11¿13o©º° | say      13°º©o11¿3,4@3,11¿10o©º°¨¨°º©13 $$1 10©º°¨¨°º©o11¿3,4@3,11¿13o©º°.
  ..Hug 4 all:me  12loves 13giving 3hugs 4to 7¬ª*^ $1 ^*ª¬._ _.2¬ª*^ $2 ^*ª¬._ _.4¬ª*^ $3 ^*ª¬._ _.14¬ª*^ $4 ^*ª¬._ _.5¬ª*^ $5 ^*ª¬.__9¬ª*^ $6 ^*ª¬._ _.2¬ª*^ $7 ^*ª¬._ _.13¬ª*^ $8 ^*ª¬._ _.10¬ª*^ $9 ^*ª¬._ _.14¬ª*^ $10 ^*ª¬._  10cuz 6you're 3all 7so 12special  1,13:)
  ..Hug In A Hug:say     1/14ºº13ñ14º13ñ14º1< 1,13H 1>14º13ñ14º13ñ1/1 $$1 1\13ñ14º13ñ14º1< 1,13H 1>14º13ñ14º13ñ14º14º1\ | say     1|14ºº13ñ14º13ñ14º1< 1,13U 1>14º13ñ14º13ñ1|13 $$1 1|13ñ14º13ñ14º1< 1,13U 1>14º13ñ14º13ñ14º114º1| | say     1\14ºº13ñ14º13ñ14º1< 1,13G 1>14º13ñ14º13ñ1\1 $$1 1/13ñ14º13ñ14º< 1,13G 1>14º13ñ14º13ñ14º114º1/ | say    1\14ºº13ñ14º13ñ14º1< 1,13S 1>14º13ñ14º13ñ1\1 $$1 1/13ñ14º13ñ14ºº13ñ14º13ñ14º1< 1,13S 1>14º1/
  ..Lots a Hugs:say     1loves giving hugs1 to12 ¬ª*^10 $$1 12^*ª¬._ _.¬ª*^4 $$1 12^*ª¬._ _.¬ª*^7 $$1 12^*ª¬._ _.¬ª*^9 $$1 12^*ª¬._ _.¬ª*^13 $$1 12^*ª¬._¬ª*^6 $$1 12^*ª¬._ _.¬ª*^3 $$1 12^*ª¬._ _.¬ª*^11 $$1 12^*ª¬._ _.¬ª*^5 $$1 12^*ª¬._ _.¬ª*^
  ..Special Frnd:say     12<13S12<13p12<13e12<13c12<13i12<13a12<13l12<  12,13 $$1 12 >13F12>13r12>13i12>13e12>13n12>13d12> 
  ..Light Kiss:say     12hugs 9{3-<-8@9{3-<-8@9{3-<-8@12 $$1 8@3->-9}8@3->-9}8@3->-9}12 and places the lightest kiss upon your cheek!
  ..Loops:say     10(¯`·._13(¯`·._6(¯`·._10(13 $$1 10)6_.·´¯)13_.·´¯)10_.·´¯) |  /say 10(¯`·._13(¯`·._6(¯`·._10(13 $$1 10)6_.·´¯)13_.·´¯)10_.·´¯)
  ..Loops2:say     13**1`'·.¸_)13*11(_¸.·'´13**1 $$1 13**11`'·.¸_)13*1(_¸.·'´13** |  /say 13**11`'·.¸_)13*1(_¸.·'´13**1 $$1 13**1`'·.¸_)13*11(_¸.·'´13** 
  ..Stars:say     9¤3{8¤¤9{3¤¤8{9¤¤3{8¤¤9{3¤¤8{3 $$1 8}3¤¤9}8¤¤3}9¤¤8}3¤¤9}8¤¤3}9¤ |  /say 8,8¤3,3¤¤¤9,9¤¤¤8,8¤¤3,3¤¤¤9,9¤¤8,8¤¤3,3{8 $$1 3}8,8¤¤9,9¤¤3,3¤¤¤8,8¤¤9,9¤¤¤3,3¤¤¤8,8¤ |  /say 9¤3{8¤¤9{3¤¤8{9¤¤3{8¤¤9{3¤¤8{3 $$1 8}3¤¤9}8¤¤3}9¤¤8}3¤¤9}8¤¤3}9¤
  ..Pink/Grey !'s:say      15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡14 $$1 13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15! |  /say  15,15!¡!¡14,14!¡!13,13¡!15,15¡!¡13,13!¡!13,13¡!14,14. 13,13.15,13 $$1 13,13.14,14. 13,13!¡13,13!¡!15,15¡!¡13,13!¡14,14!¡!15,15¡!¡! |  /say 15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡14 $$1 13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!13¡15!
  ..Y Hug:say     13~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~1 $$1 13~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~ |  /say 1~¥~13{{{1~¥~13{{{1~¥~13{{{1 $$1 13}}}1~¥~13}}}1~¥~13}}}1~¥~ |  /say 13~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~1 $$1 13~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~~1¥13~
  ..Fish Hug:say     2¸13.·´2°13`·.2¸11°°° 12><((((13º12>2 $$1 13<12º13))))><  11°°°2¸13.·´2°13`·.2¸  |  /say 2¸13.·´2°13`·.2¸11°°° 13><((((12º13>2 $$1 12<13º12))))><  11°°°2¸13.·´2°13`·.2¸
  ..Hug with 3's:say     12313~6>12313~6>12313~6>12313~6>13 $$1 6<13~12313~6<12313~6<1236<13~123 |   /say 13{{{12*13{{{12*****6 $$1 12*****13}}}12*13}}} |  /say  12313~6>12313~6>12313~6>12313~6>13 $$1 6<13~12313~6<12313~6<1236<13~123 
  ..HugZ:say      8!! 2@@@ 4HuGZ2 @@@ 8!!3 $$1 8!! 2@@@ 4HuGZ2 @@@ 8!! | say      8!! 2@@@ 4HuGZ2 @@@ 8!!3 $$1 8!! 2@@@ 4HuGZ2 @@@ 8!! | say      8!! 2@@@ 4HuGZ2 @@@ 8!!3 $$1 8!! 2@@@ 4HuGZ2 @@@ 8!!
  ..Flowers:say     0..13@@@@0....6@@@@0....3 $$1 0....6@@@@0....13@@@@ |  /say 3-13@@3()13@@3--6@@9()6@@3---9 $$1 3---6@@9()6@@3--13@@3()13@@3- |  /say 0..13@@@@0....6@@@@0....3 $$1 0....6@@@@0....13@@@@ 
  ..Thanks:say     15,6«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«1511¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15» |  /say 5,1313,13iiiii11THÄNK ¥ÕÚ $$1 !!! 15:->13,13iii |  /say 15,6«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»«11¤10¥11¤15»
  ..MEGARAIN:say     4,1 M 1,4 E 4,1 G 1,4 A 4,1 §®º° $$1 °º®§ 1,4 H 4,1 U 1,4 G 4,1 S  | say     8,1 M 1,8 E 8,1 G 1,8 A 8,1 §®º° $$1 °º®§ 1,8 H 8,1 U 1,8 G 8,1 S  | say     11,1 M 1,11 E 11,1 G 1,11 A 11,1 §®º° $$1 °º®§ 1,11 H 11,1 U 1,11 G 11,1 S  | say     12,1 M 1,12 E 12,1 G 1,12 A 12,1 §®º° $$1 °º®§ 1,12 H 12,1 U 1,12 G 12,1 S  | say     6,1 M 1,6 E 6,1 G 1,6 A 6,1 §®º° $$1 °º®§ 1,6 H 6,1 U 1,6 G 6,1 S1§
  ..RAINBOWHUGS2:say      4,1 H 1,4 U 4,1 G 1,4 S 4,1 §®¤º° $$1 °º¤®§ 1,4 H 4,1 U 1,4 G 4,1 S  | say     8,1 H 1,8 U 8,1 G 1,8 S 8,1 §®¤º° $$1 °º¤®§ 1,8 H 8,1 U 1,8 G 8,1 S  | say      11,1 H 1,11 U 11,1 G 1,11 S 11,1 §®¤º° $$1 °º¤®§ 1,11 H 11,1 U 1,11 G 11,1 S  | say     12,1 H 1,12 U 12,1 G 1,12 S 12,1 §®¤º° $$1 °º¤®§ 1,12 H 12,1 U 1,12 G 12,1 S 
  ..MEGAHUG:say     4,1 m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§4,8 $$1 4,1m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§  |  /say 4,1 m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§4,8 $$1 4,1m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§  |  /say 4,1 m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§4,8 $$1 4,1m8*4ê8*4g8*4ª8*4h8*4µ8*4g8*4§1§
  ..RAINROLL:say     6,1©©©©© Hi $$1 !! ©©©©© | say     12,1©©©©© Hi $$1 !! ©©©©© | say     11,1©©©©© Hi $$1 !! ©©©©© | say     3,1©©©©© Hi $$1 !! ©©©©© | say     8,1©©©©© Hi $$1 !! ©©©©© |  /say 4,1©©©©© Hi $$1 !! ©©©©©
  ..YGRASSHI:say     9,1/|\/|\/|\/|\/|\/|\1 $$1 9/|\/|\/|\/|\/|\/|\ | say     9,1\|/\|/\|/\|/\|/\|/8 $$1 9,1\|/\|/\|/\|/\|/\|/ |  /say 1,1-8H1--8E1--8L1--8L1--8O1--8!1-1 $$1 1,1-8H1--8E1--8L1--8L1--8O1--8!1§ | say     9,1/|\/|\/|\/|\/|\/|\8 $$1 9/|\/|\/|\/|\/|\/|\ | say     9,1\|/\|/\|/\|/\|/\|/1,1 $$1 9,1\|/\|/\|/\|/\|/\|/ 
  ..RHIYA:say     4,1 §ø8Hiya4§ø§ø§ø§ø§ø1 $$1 4ø§8Hiya4ø§ø§ø§ø§ø§  | say      4,1 §ø§ø§ø8Hiya4§ø§ø§ø8 $$1 4ø§ø§ø§8Hiya4ø§ø§ø§  | say     4,1 §ø§ø§ø§ø§ø8Hiya4§ø1 $$1 4ø§ø§ø§ø§ø§8Hiya4ø§1§
  ..RYHowdy:say     9,1 <8W9>1---9»4®9«1---9<8O9>1------ $+ $1 $+ 9<8B9>1---9»4®9«1---9<8!9>  | say     1,1 ---9<8E9>1---9<8C9>1---9<8M9>1---8 $+ $1 $+ 1---9<8A9>1---9<8K9>1---  | say      1,1 ------9<8L9>1---------9<8E9>1 $+ $1 $+ 1------9<8C9>1--§--- 
  ..XHI:say     8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O 1 $$1 8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O  | say     1,1 4¤¤ 8E4 ¤¤¤ 8L4 ¤¤ 8 $$1 4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤  | say     1,1 4¤¤¤¤¤ 8L4 ¤¤¤¤¤ 1 $$1 4,1 ¤¤¤¤¤ 8L4 ¤¤¤¤¤  | say     4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤ 8 $$1 4,1 ¤¤ 8E4 ¤¤¤ 8L4 ¤¤1,1  | say     8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O 1 $$1 8,1 H4 ¤¤¤¤¤¤¤¤¤ 8O1§
  ..PGRASSHI:say     9,1 \|/\|/\|/\|/\|/\|/1,1Hi. $+ $1 $+ 9,1\|/\|/\|/\|/\|/\|/  | say     1,1h9«13ºôº9» «13ºôº9» «13ºôº9» 13,1Hi1,1.13 $+ $1 $+  9,1 «13ºôº9» «13ºôº9» «13ºôº9»  | say 9,1 /|\/|\/|\/|\/|\/|\1,1Hi. $+ $1 $+ 9,1/|\/|\/|\/|\/|\/|\1§
  ..HIRBW:say     0,1o\4@0/oo\4@0/oo\4@0/oo\4@0/o1Hi. $+ $1 $+ 0o\4@0/oo\4@0/oo\4@0/oo\4@0/o | say     9,1»|«  »|« »|«  »|«  »|« 4Hi1,1.4 $+ $1 $+ 9 »|«  »|«  »|« »|«  »|« | say     0,1o/4@0\oo/4@0\oo/4@0\oo/4@0\o1Hi. $+ $1 $+ 0o/4@0\oo/4@0\oo/4@0\oo/4@0\o
  ..GREEN:say     9,1 »0|9«  »4©9«  »0|9«  »4©9«1  Hi $$1  9»4©9«  »0|9«  »4©9«  »0|9«  | say     9,1 »4©9«  »0|9«  »4©9«  »0|9«4  Hi $$1  9»0|9«  »4©9«  »0|9«  »4©9«  | say     9,1 »0|9«  »4©9«  »0|9«  »4©9«1  Hi $$1  9»4©9«  »0|9«  »4©9«  »0|9«1§
  ..RYScalesHi:say     4,1|8Ü4|8Ü4|8Ü4|1-8H1-4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|1-8H1-4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|1-8E1-4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|1-8E1-4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|1-8L1-4|8Ü4|8Ü4|8Ü4|4 $$1 4|8Ü4|8Ü4|8Ü4|1-8L1-4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|1-8L1-4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|1-8L1-4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|1-8O1-4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|1-8O1§4|8Ü4|8Ü4|8Ü4|
  ..RYHugs:say     4,1 4§ø§ø§ø§ø8Hugs4§ø1 $$1 4ø§8Hugs4ø§ø§ø§ø§  | say      4,1 §ø§ø8Hugs4§ø§ø§ø8 $$1 4ø§ø§ø§8Hugs4ø§ø§  | say     4,1 §ø§ø§ø§ø8Hugs4§ø1 $$1 4ø§8Hugs4ø§ø§ø§ø§1§
  ..RYDIA:say     4,1 «»9»8®®9«4«»«»«»«»8 $1 8 $1 4«»«»«»«»9»8®®9«4«»  | say     4,1 «»«»«»«»«»«»8 $1 4«»«»8 $1 4«»«»«»«»«»«»  |  | say     4,1 «»9»8®®9«4«»«»8 $1 4«»9»8®®9«4«»8 $1 4«»«»9»8®®9«4«»  | say     4,1 «»«»«»«»«»«»8 $1 4«»«»8 $1 4«»«»«»«»«»«»  | say     4,1 «»9»8®®9«4«»«»«»«»8 $1 8 $1 4«»«»«»«»9»8®®9«4«»1§
  ..XX:say     11,1 \/\/1=13(¯`°´¯)1=11\/\/1 $$1 11\/\/1=13(¯`°´¯)1=11\/\/  | say     11,1 /\/\1.=13`·.·`1= 11/\/\13 $$1 11/\/\1 =13`·.·`1.=11/\/\  | say     11,1 \/\/1=13(¯`°´¯)1=11\/\/13 $$1 11\/\/1=13(¯`°´¯)1=11\/\/  | say     11,1 /\/\1.=13`·.·`1= 11/\/\1 $$1 11/\/\1 =13`·.·`1.=11/\/\1§
  ..PTHUGS:say     13,1§ø§ø§ø§ø§ø11HUGS!13§ø1 $$1 13ø§11HUGS!13ø§ø§ø§ø§ø§ | say      13,1§ø§ø§ø11HUGS!13§ø§ø§ø11 $$1 13ø§ø§ø§11HUGS!13ø§ø§ø§ | say     13,1§ø11HUGS!13§ø§ø§ø§ø§ø1 $$1 13ø§ø§ø§ø§ø§11HUGS!13ø§ |  /say  13,1§ø§ø§ø11HUGS!13§ø§ø§ø11 $$1 13ø§ø§ø§11HUGS!13ø§ø§ø§ | say     13,1§ø§ø§ø§ø§ø11HUGS!13§ø1 $$1 13ø§11HUGS!13ø§ø§ø§ø§ø§
  ..Hug In A Hug:say     11,1/13º11º13ñ11º13ñ11º13< 11H 13>11º13ñ11º13ñ11/13 $$1 11\13ñ11º13ñ11º13< 11H 13>11º13ñ11º13ñ11º13º11\ | say     11,1|13º11º13ñ11º13ñ11º13< 11U 13>11º13ñ11º13ñ11|13 $$1 11|13ñ11º13ñ11º13< 11U 13>11º13ñ11º13ñ11º13º11| | say     11,1\13º11º13ñ11º13ñ11º13< 11G 13>11º13ñ11º13ñ11\13 $$1 11/13ñ11º13ñ11º13< 11G 13>11º13ñ11º13ñ11º13º11/
  ..REDW:say     0,1 \4@0/oo\4@0/oo\4@0/4 $$1 0\4@0/oo\4@0/oo\4@0/  | say     0,1 /4@0\oo/4@0\oo/4@0\4 $$1 0/4@0\oo/4@0\oo/4@0\  | say     0,1 \4@0/oo\4@0/oo\4@0/4 $$1 0\4@0/oo\4@0/oo\4@0/  | say     0,1 /4@0\oo/4@0\oo/4@0\4 $$1 0/4@0\oo/4@0\oo/4@0\1§
  ..10Nick:say     9,1 8 $$1 9<4©9><4©9><4©9>4©9<4©9><4©9><4©9>8 $$1 1. | say     9,1 <4©9><4©9>8 $$1 9<4©9>4©9<4©9>8 $$1 9<4©9><4©9>  | say     1,1-8 $$1   9<4©9><4©9>8 Hello 9<4©9><4©9>8 $$1 1- | say     9,1 <4©9><4©9>8 $$1 9<4©9>4©9<4©9>8 $$1 9<4©9><4©9>  | say     1,1.8 $$1 9<4©9><4©9><4©9>4©9<4©9><4©9><4©9>8 $$1 1§ | 
  ..YRSCAL:say     4,1|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|1---4|8Ü4|8Ü4|8Ü4|4 $$1 4|8Ü4|8Ü4|8Ü4|1-§-4|8Ü4|8Ü4|8Ü4| | say     4,1|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|1 $$1 4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|8Ü4|
  ..!PTHug:say     1,1-13§ø§ø§ø§ø§ø11HUGS!13§ø1 $$1 13ø§11HUGS!13ø§ø§ø§ø§ø§1- | say      1,1-13§ø§ø§ø11HUGS!13§ø§ø§ø11 $$1 13ø§ø§ø§11HUGS!13ø§ø§ø§1- | say     1,1-13§ø11HUGS!13§ø§ø§ø§ø§ø1 $$1 13ø§ø§ø§ø§ø§11HUGS!13ø§1- |  /say 1,1-13§ø§ø§ø11HUGS!13§ø§ø§ø11 $$1 13ø§ø§ø§11HUGS!13ø§ø§ø§1- | say     1,1-13§ø§ø§ø§ø§ø11HUGS!13§ø1 $$1 13ø§11HUGS!13ø§ø§ø§ø§ø§1§
  ..BWX:say     0,11 $$1 0 | say     0,1  |-x - x - x - x -14 $$1 0- x - x - x - x-|1- | say      1,1-0|---|---|---|---|1 $$1 0|---|---|---|---|1-  |  /say 0,1 |-x - x - x - x -14 $$1 0- x - x - x - x-|1§ | say     0,11 $$1 0   
  ..BWCheck:say     1_________________0 $$1 1_________________ |  /say 1,1.1,00,11,00,11,00,11,00,11,00,11,04,1 $$1 1,00,11,00,11,00,11,00,11,00,11,01,1. | say     1,1.1,00,11,00,11,00,11,00,11,00,11,01,1 $$1 1,00,11,00,11,00,11,00,11,00,11,01,1. | say     1,1.1,00,11,00,11,00,11,00,11,00,11,04,1 $$1 1,00,11,00,11,00,11,00,11,00,11,01,1§ | say     1¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯0§ $$1 1¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
  ..Owl:say     1_____________0 $$1 1_____________  |  /say 1,00,11,00,11,00,11,00,11,01,1 $$1 1,00,11,00,11,00,11,00,11,0 | say     1,01,1....0,_,1....1,04,1 $$1 1,01,1....0,_,1....1,0  | say     1,01,1...0(4¤0v4¤0)1...1,01,1 $$1 1,01,1...0(4¤0v4¤0)1...1,0  | say     1,01,1.0 ~{`-´}~1..1,01,1 $$1 1,01,1.0 ~{`-´}~1.§1,0  |  /say 1,00,1---`"-"´---1,04,1 $$1 1,00,1---`"-"´---1,0  |  /say 1,00,11,00,11,00,11,01,10,11,01,1 $$1 1,00,11,00,11,00,11,00,11,0 | say     1¯¯¯¯¯¯¯¯¯¯¯¯0§ $$1 1¯¯¯¯¯¯¯¯¯¯¯¯¯
  ..PlantHug:say     1,1......9«4®©©®9»1.....1 $$1 .....9«4®©©®9»1...... | say     1,1....9«4®©®9«»4©®©9»1...4 $$1 1...9,1«4®©®9«»4©®©9»1.... | say     1,1...9«4®©®9«4®©9»4©®©9»1..1 $$1 1,1..9«4®©®9«4®©9»4©®©9»1... | say     1,1...0/~|~~|~~|~~\1,1..4 $$1 1..0/~~|~~|~~|~\1,1... | say     1,1...0\__________/1..  $$1 1..0\__________/1.§.
  ..orgAHug:say     0,12*******4,4-0,0-4,4-0,0-4,4-0,0-4,4-4,4 $$1 4,4-0,0-4,4-0,0-4,4-0,0-4,4-0,12******  | say     0,12*******4,4-0,0-4,4-0,0-4,4-0,0-4,4-0,4 $$1 4,4-0,0-4,4-0,0-4,4-0,0-4,4-0,12******  | say     0,12*******4,4-0,0-4,4-0,0-4,4-0,0-4,4-4,4 $$1 4,4-0,0-4,4-0,0-4,4-0,0§4,4-0,12******
  .Kisses
  ..Smooch:me 4,8*2Sm14ô4¡14ô2ChErS4 $$1 2Sm14ô4¡14ô2ChErS4*
  ..WHOA:me 1says after kissing4 $$1 1...5I climbed up the door, 6and shut the stairs.....9I said my clothes, 3and pulled off my prayers.....2I shut off my bed, 11and climbed in to the light.....13All because4 $$1 12kissed me !!!5
  ..Heart2bar:say      4,1(7¯8`9'11·13.4¸7(8¯9`11'13·4.7¸ 12,19,18,191113478911138124 ¸7.8·9'11´13¯4)7¸8.9·11'13´4¯7) | say      4,1<7-8-9-11<13<4<7<8=9=11-13-4-7-4>11 $$1!!!! 4<-7-8-9-11=13=4>7>8>9>11-13-4-7> | say      4,1(7_8¸9.11·13'4´7(8_9¸11.13·4'7´ 12,19,18,191113478911138124,1 `7'8·9.11¸13_4)7`8'9·11.13¸4_7)
  ..Kiss Woohoo:me > 13,1RuÑs OvèR   tõ   .....    4 $$18   .....     8YeLLiÑg .........  9"WöõôðHöõôð ...........    4Kϧ§ Mê      13 Kϧ§ Mê       7 Kϧ§ Mê !!!!!!! 
  ..Hot Kiss:say     gives 9,1 $1  a Heart stopping, expolsion starting,Really Long..and extra deep..super Duper..and really hot...peck on the cheek
  ..Hot Kiss 2:say     8,1 gives 9,1 $1 8,1 a *Super Tight And Really Super Erotic Groping Fondling Earth Shattering Sonic Boom Oh Gawd If You Stop I'll Kill You Better Than Heaven Yet Hotter Than Hell Watch Where Ya Stick Yer Hands Super Huge Oh Gawd Oh Gawd Oh Gawd Don't Ya Dare Stop Touching Me Huggeroonies And A Knees Are Shaking Earth Is Trembling My Heart Has Stop Beating Passionit Knees Shaking Lots A Goosebumps Was That The Ground Moving Passionate Wet Dee....
  ..000 kiss:me 10gives7 $$1 10a lip ripping, tongue snatching, suck the eyeballs right out of the socket, hot flaming, breath-taking, spine tingling, make you weak at the knees, better call 000, to die for KISS!!!!!!!!!
  ..Kiss really about:me 2gives $$1 a kiss that does not shake the ground or curl his toes..13 it does not even rock his world or make him weak in the knees....12.it just makes him smile and feel kind of good inside when he feels the softness of my lips on his...and he sees that twinkle in my eyes.... :o) 4Now that's what a *kiss* is really all about .....isn't it?   :o)
  ..Good Morning Kiss:say     KiSsEs 4 $$1  GeNtLy On dA LiPs        ؤ¤¤((((((Good Morning Baby))))))¤¤¤Ø  
  ..LifeGuard Kiss:say     SaVeZ  $$1  FrOm DrOwNiNG AnD GiVeS SoMe LoVeRs  MoUtH 2 MoUtH -(FoR HoUrS)-.....mmmmmmmmmmmm BaBe YoU TaStE So GoOd...FeElS LiKe HeAvEn.........
  ..Heavy Kiss:say     gives12 $$1 1a4 SLoW..2LoNG..13DeeP..10PeNeTRaTiNG..7ToNSiL-TiCKLiN'..4HaiR-fRiZZin'..3Toe-CuRLiN'..7NeRVe-JaNGLiN'..4LiFe-aLTeRiN'..5FaNTaSY-CrEaTiN'..12I JuST SaW sTaRs..13HeY, Am I sTiLL dReSsEd?..2CoMe To Me..6KiSS oN Da LiPS!!!
  ..Wet Kiss:say     4 gives 12 $$1 4a13 *Mega12Super11Dooper10Extra9Sqeeeezy8Squishy3Nuzzle4Snuggle5Huggle6Knock7You8Down7Plow6You5over13Smoochy12Sloppy11Wet10Kiss*
  ..Oh my god kiss because:me 4 gives $$1 a Really 4Hot..Long Super And Really Erotic..Earth Shattering, 4Soul Burning, Oh My Gawd If I Stop Now I'll Die, Better Than Heaven, 7Yet Hotter Than Hell, Groping, Don't Stop Touching Me, 4OhOhOhOhGawdOhGawdOhGawd Soul Stealing, Dream Making, Close To XRated, Hand Trembling, Knee Buckling,12 Heart Stopping, Body Tingling,Earth Quake Making, 13Passion Exploding, Wet, Deep, Hard, Long, Lingering, Kiss Because $$?="why?"
  ..Your eyes twinkle:me 0,1* ·.*11,1Says0,1* . + . * : 11,1 $$1 0,1+ * . : . : +. * .*11,1your 0,1· * . + . * : . * .+ *.·..11,1eyes0,1 * . +. * .: +. .* +. ·. + * . * . ·. . · 11,1twinkle0,1*. +· . · * . * +. : . · * . * ·. * .* +. * · . 11,1like0,1+.**. . * ·.·*.+ * .* · ·*.* : · *.11,1stars 0,1* * . + ·. ·*. .*·.· *+. *·.·*.11,1in the 0,1*.: *. .·**. *+. · *.* + * ·. * . * +.*·.11,1nightsky0,1* * + · . ·* : +*.* . * +:. . *.·*.
  ..CutiePaPootie:me 4© 12You Are Such a 6 CutiePaTootie and I just4 love6 you12!!4 $$1 4© 
  ..Shutup && kiss me:me 0,144 © 0tells8 ©0 $$1 9© 0shut11 © 0up13 © 0and4 © 0kiss8 ©0me9 © 
  ..LifeGuard Kiss:say     SaVeZ  $$1  FrOm DrOwNiNG AnD GiVeS SoMe LoVeRs  MoUtH 2 MoUtH -(FoR HoUrS)-.....mmmmmmmmmmmm BaBe YoU TaStE So GoOd...FeElS LiKe HeAvEn.........
  ..BlowKiss:say     13 blows a kiss to 3 $$1 ... 12floating your way hun!
  ..Kiss:me 2R3u4N13s6s12s13 14O2v3e4R5 6a12N13D14 2G3i4V5e6S 4©4©12 $$1 4©4© 1a 0,14 *0 B 8*0  I 9*0 G 11* 0 13 *0 K 4*0 I 8*0 S 9*0 S 11* 

  .Laughter
  ..lol:me 4 ø(ô¿ô)ø 2 LOL 5 ø(ô¿~)ø 10 $$1 6 ø(~¿~)ø 13 LOL 7 ø(~¿ô)ø 12 LOL 3 ø(ô¿ô)ø 
  ..Bellylaff:me gets a  4BIG BELLY LAUGH ! ! !  from $$1 
  ..Giggle:me thinks your so funny      13«« Giggle, Giggle, Giggle »»  
  ..Hehe:me 2hee3hee4hee5 $snick 6hee7hee8hee9 $snick 10hee11hee12hee13 $snick 14hee15hee2hee  
  ..Laughin out loud:me 13««LãÛghÍñ ØùT løÛл»1,11 ««LãÛghÍñ ØùT løÛл»13 ««LãÛghÍñ ØùT løÛл» 
  ..HaHa:me 8,1HA!13:)8,1HA!13:)8,1HA!13:)13  $$1 13(:8,1HA!13(:8,1HA!13(:8,1HA!
  ..Laughing:me 13 is busy having a laughter attack.......6hahahahahahaha..........13hehehehehehehehe........7woohhooooooo!!!!!!10 oopsy weee walk.......
  ..Smiles:me 8(1,8:)8)4 $1 8(1,8:)8)11 $1 8(1,8:)8)12 $1 8(1,8:)8)3 $1 8(1,8:)8)7 $1 8(1,8:)8)9 $1 8(1,8:)8)13 $1 8(1,8:)8)14 $1 8(1,8:)8)10 $1 8(1,8:)8)1 $1 8(1,8:)8)4 $1 8(1,8:)8)11 $1 8(1,8:)8)12 $1 8(1,8:)8)3 $1 8(1,8:)8)7 $1 8(1,8:)8)9 $1 8(1,8:)8)13 $1 8(1,8:)8)14 $1 8(1,8:)8)10 $1 8(
  ..Break Out Smiles:me 8(1,8:)8)4 breaks out in smiles 8(1,8:)8)11 breaks out in smiles 8(1,8:)8)12 breaks out in smiles 8(1,8:)8)3 breaks out in smiles 8(1,8:)8)7 breaks out in smiles 8(1,8:)8)9 breaks out in smiles 8(1,8:)8)13 breaks out in smiles 8(1,8:)8)14 breaks out in smiles 8(1,8:)8)10 breaks out in smiles 8(1,8:)8)1 breaks out in smiles 8(1,8:)8)
  .mm innocent I don't think so!!
  ..innocent:me 7stares at the ceiling and whistles innocently....12Who me???? | me 4 Nahh...........Not me.....4,14,8p:)
  ..Frozen:say     drops a cold ice cube down 12 $+ $$1 $+ 's shirt!
  ..Tunnel:say     13,1©}æ}»®©®«®©`®®»0"Due to the current financial restraints, the light at the end of the tunnel will be turned off until further notice." (this means that the tunnel back to reality is closed!!! you can't go back now!! hehehe!!! )13,1«®©`®®»©®®«{æ{©
  ..bite me {
    say     1,1-----8(1-8(1-8(1-----
    say     1,1---8'.1-0___ 8.'1---
    say     1,1---8'8-0,1(12O1-12O0)8-'1---
    say     1,1-0ooO1--0(_)1--0Ooo1-
    say     1,1---4O3h 9B13i7t11e 6M8e1--
  }
  ..bite me more:say      0-----1!!! | me 0--1`0--1` ´0--1´ | me 0--0--13(1ò^ó13)0--0- | me 13ooO1--13(_)1--13Ooo- | me 12 Just BiTe Me! 
  ..bitehard:say     12,1 4I 13wönt 11bîtë 9"Hå®d" 12 
  ..isthinking:me 4,1 LOOK OUT! 8,1 $$1 4,1 is THINKING!!
  ..crayon:me 12 gives $$1 a pack of 8,1Crayola® Crayons12..13 now sit, color..4and keep outta trouble6..10 or I'll deal with ya 13myself!!
  ..Did stupid:me 13,1 $$1 12 did 9 something 8,1STUPID!!!!
  ..brainlag:me 13,2 Hey $$1 ... 9,2 Brain lag is different than puter lag, you know!! 
  ..How Embarrassing:me 13,1 says  11OOPSY!!! 9 That's  8embarrassing 11!!!!!
  ..No Comment:me 13leans forward towards the mic | me 13Um, no comment | me 13sits back down 
  ..out loud:me  0,14·0I'm8·0sorry9·0hehehe11·0was13·0that4·0out8·0loud9·0??  
  ..5th:me  8,2 9pleads the11 5th!!!!!  
  ..1800:me 0,13 <2>3<4>5<6>7<8>9<10>13 0-8 for a Hot and Sexy time call 1 800 SomeOneElse.
  ..Sexy-Big:me 12,1§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥--->13 $$1 12<---§èx¥!
  ..Talent/Trouble:me says aaahhhhhhh4 ..... $1 .....12 A splendid combination of 7talent 1 and 4 trouble.
  ..PMS && Gun:me is 13Female.......4has PMS and a gun.......6any Questions??!!........7Becomes a4 B.I.T.C.H7....in 0-2 Seconds....3My Age doesn't matter........12experience does!!!!....13I also have an attitude.........any Questions??!!....... 3 
  ..Stats:say     12¯13°12·13.12¸13¸12.13·12°13¯ 4 $me is fascinated by other chatters 6ObSeSsIoN  4 with knowing her "stats" 12¯13°12·13.12¸13¸12.13·12°13¯ . | me 12¯13°12·13.12¸13¸12.13·12°13¯  4 The question now, of course, is am I going to tell you or not? 12¯13°12·13.12¸13¸12.13·12°13¯ 
  ..Bored Bored:me is 4bored 7bored8 bored9 bored 3bored10 bored 2bored 11bored 12bored6 bored13 bored1 bored  4 bored 7bored8 bored9 bored 3bored10 bored 2bored 11bored 12bored6 bored13 bored1 bored  4 bored 7bored8 bored9 bored 3bored10 bored 2bored 11bored 12bored6 bored13 bored1 bored 4 bored 7bored8 bored9 bored 3bored10 bored 2bored 11bored 12bored6 bored13 bored1 bored  4 bored
  ..This Room:me 4NOTICE: 2This Channel Requires No Physical Fitness Program.  12Everyone gets enough exercise Jumping to Conclusions, 10Flying Off The Handle, 7Running Down The Hosts, 6Knifing Friends In The Back, 3Dodging Responsibility, 5And Pushing Their Luck. 2hehehehehe
  ..Lil Devil..: {
    say     2,4\\_//9,13 You 2,4\\_//9,13 Little 2,4\\_//9,13 Devil 2,4\\_//  $$1
    say     2,4 o o 9,13 You 2,4 o o 9,13 Little 2,4 o o 9,13 Devil 2,4 o o   $$1
    say     2,4 \-/ 9,13 You 2,4 \-/ 9,13 Little 2,4 \-/ 9,13 Devil 2,4 \-/   $$1
  }
  ..Peek A Boo..: {
    say     11,1 Look who's in $active    ! ! ! !
    say     1,1.......0,1 \\|//0,1 8,1 Peek-a-boo 1,1.....
    say     0,1 1,1......11 (4o o11,1)8,1 I See You!!!! 1,1...
    say     0,111,1oOOo8,1~11,1(_)8,1~11,1oOOo8,1~~~~1,1..........
  }
  ..Take picture:me 1,1------13,1\|/1,1 13,1____1,1 13,1\|/1,1-----0,0...10takes hidden camera | me 1,1------13,1~@-/1,1 12,1oO1,1 13,1\-@~1,1-----0,0...10and gets a Portrait | me 1,1------13,1/13,1_13,1(1,1 13,1\__/1,1 13,1)_\1,1-----0,0...10of 6 $1 
  ..Typo monster:me 1  glares evilly at the horrifying 10typo-monster. 1 $me picks it up and strangles it by it's scrawny little neck. When the horrifying Typo Monster is 4destroyed, 1she smiles as a 6new calm 1sweeps over her. 
  .Only a lil norti!!!
  ..DevilWoman:say     says4 Watchout,1 $$1 4 is a Devil Woman with 1Evil4 on her mind!!!!!!!! 
  ..DinnerXX:say     1 *  4SayZ 3to2 $$1 13BabY, 10YoUz 5A2 WiNnER.. | say      0,0...6       WoulDn'T 1mInD 14HavEn 3YoU 15FoR 12DiNNeR... | say     0,0... 5 <grin>
  ..Pee!:me  1,9 Bath 1,4 Room 1,8 Break .. .(_¸.l.¸_) .. 20ohh 9 aahh 13 Yeess!! 1,8 My Bladder!  :p   
  ..wesson oil:me 6 breaks out the whips, chains, rubber mattress cover and wesson oil...4look out 13 $$1 , 4 I am feeling mean and kinky tonight !!!!!! Evil Grin....
  ..Annoying Man:say     12 SAYS 4 ******EXTREME CAUTION***** 12 Man with extreme inclinations to mate with any breathing...walking...(alive?) woman in the channel has now entered this room....4** $$1** ...3 WOMAN 12....pretend to be dead...3 MEN 12....give him a WORLD ROCKING WEDGIE!!!!!! 
  ..Impressed:me 5 says 4 ********EXTREME CAUTION******* 12 A sweet, intelligent, sexy man has come into our channel..9***** $$1***** 12 BEWARE of women melting into piles of 4 LOVE DUST!!4 (watch where ya step!!)
  ..Such a Babe:say     12 SAYS 8,1 ****EXTREME CAUTION**** 12  Man with tight jeans...9 a white T-shirt...and a 4,1 "I GOTTA PINCH DAT BUTT!!"  9 kinda look.. 12,1 just walked into the channel...4,0 $$1 8,1 :) :):):):)
  ..etAttn:say     tries very 4 *shyly*  to get 7 $$1  attention: 9 "YeeeOOooww!!! 12WooOOHooOO!!!| 13*Wheet-WheEEEew!!!* 9Hubba-HUBBA !!! 7<WOOF!!!>GRRrrooowWWL!!! 10Heyyyy, 13BAAABY 9!!!
  ..Not that kind:me would like to state at this point in time ...that i am not that kind of girl .....in public....6NOW in private.........hehehehe!!!!!! 
  ..bed errr bad:me 4 $$1 You're always there for me, when I feel bed, er, bad, 10in sad times or horney, um, happy times. 4You always seem to read between the sheets, I mean lines, 12and get right to the heat, uh, heart of the mattress, uh, matter. 7That's why it's so nice being on, er, with you, just gazing lazily into a glowing sunsex, I mean sunset. 13In fact, it's just wonderful...
  ..Rules:say     2 The RULES of this chat room are as follows: | say     2 Rule $active  113 - Women in $active   are always right. ;-) | say     2 Rule $active  212 - When in doubt, refer to Rule $active  1.
  ..trsp:me 12 hangs a sign on 13 $$1 | .timer 1 2 say     ---> 8,1 //// 4,0 NO Trespassing 8,1 \\\\ | .timer 1 2 say   13 $$1 is 4 Private Property!!!!!! | .timer  1 5 uni say 1 Violators 4 WILL BE prosecuted...... 1got it4 $1 ??????
  ..ckhands:me grabs 12 $$1 4 A haaaaa 12 I caught you!! | .timer 1 2 say     I have dusted $$1 with special powder and I see it on 4 YOUR 1 hands 4 $$1 ....12  paws 4off  12the merchandise.... 14 }:[
  ..whine:me 12 Do you want some cheese to go with that 4whine 12 $$1 ??? 
  ..tongue:me 8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8  4,8 $$1 8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8(1,8:oÞ8)8
  ..am not a thief:me 1,8is not a 2Thief! Thief is so 4ugly!  | me 1,8prefers to be called a 12Creative 3Acquisition 4Specialist!
  ..trouble:me 9,2 you're in 13 Big Trouble!!!8,2 $$1 !!! 
  ..go Girl:me 13 stands up and shouts, "Preach on sister!" | me 13 stands up and shouts,12 "You go girl!"
  ..Promise:me 13 ÔÔ 1,11 O13h13,1,11 1,0P1,11R13o13M1,11i1,0S1,11e13 13M1,11e1,0 1,11t13h13i1,11s1,0 1,11i13s13n1,11'1,0t1,11 13l13e1,11a1,0d1,11i13n13g1,11 1,0t1,11o13 13s1,11o1,0m1,11e13t13h1,11i1,0n1,11g13 13r1,11e1,0a1,11l13l13y1,11 1,0e1,11m13b13a1,11r1,0r1,11a13s13s1,11i1,0n1,11g 13 ÔÔ  
  ..where?:me 8,1 í11°9¿11°8ý 00,1 I08,1 09,1w11,1a00,1s08,1 09,1j11,1u00,1s08,1t09,1 11,1h00,1e08,1r09,1e11,1,00,1 08,1w09,1h11,1e00,1r08,1e09,1 11,1d00,1i08,1d09,1 11,1I00,1 08,1g09,1o11,1? 8í11°9¿11°8ý 
  ..Bad4Me:me 13 Prefers the things in life that are 4REALLY REALLY BAD 13for her 
  ..Surgeons Warning:me 4SURGEON GENERAL'S WARNING: 12CHATTING has been known to consume time, cause insanity, wreak havoc,inspire hopes and dreams, and cause the user to sit hunched over12a computer, 4bloodshot 12eyeballs staring at the monitor until the wee hours of the morning! Further,12chatting causes a poor diet, flat butt, and ability to avoid going to the bathroom for unbelieveable amounts of time.
  .Drinks
  ..Alcaholic drinks
  ...Illusion Mix...:me 1 mixes up a batch of 3 Midori Illusions 1to share with $$1
  ...Vodka Raspberry...:me 1pours 12 $$1 1a 4 Raspberry Vodka 1...enjoy!!
  ...Pitcher:me 1mixes up a frosty pitcher of 4Strawberry Margaritas 1to share.....who wants some?
  ...ColorBar:say     1,0 0  0,1  1 0,2  2 0,3   3 0,4   4 0,5   5 0,6   6 1,7   7 1,8   8 1,9   9 0,10  10 1,11 11 1,12  12 1,13  13 0,14  14 1,1515 
  ...Cooler:say     10pops open $me $+ 's cooler..... here take your pick..... 15,15!1,148,3MtDew 15,15!...15,15!1,140,3 Sprite 15,15!.. 1,15 0,4 Coke 1,15... 15,15!1,140,3 74,3°0,3UP 15,15! ... 14,14!4,15P12,15e0,15p4,15s12,15i 1
  ...Vodka:say     serves 4,1 $1  a refeshing glass of vodka and lime.
  ...Tequila:say     makes a nice tequila-pop for 13,1 $1 .
  ...Sub-Zero:say     passes a bottle of Sub-Zero to 12,11 $1 
  ...Bacardi:say     serves 15,1 $1  with a nice bacardi-coke.
  ...Bourbon:say     serves 15,1 $1  a Jimmy Beam n Coke.
  ...Scotch:say     passes 15,1 $1  a Joh:nny Walker on Ice.
  ...Margarita:say     pulls out a 8,12beach chair6,0 for12 $$1, 6and hands over a 12,8Margarita6,0 and a pair of shades!
  ...Beer:say     throws 12,11 $1  a can of beer.
  ...GinTonic:say     offers9 $$1 6,0a tall cool gin and tonic with a very thin slice of 9lime.
  ...Molson: /say hands4 $1 a nice cold 4Molson 2C4a2n4a2d4i2a4n 
  ...MGD:say     offers a cold 1,1 1,8 MGD 1,1 6,0 to 1,8 $1 
  ...MillerHiLife:say     tosses7 $1 6a 3Miller 3,3 3,5HILIFE3,3 
  ...Corona:say     serves9 $1 6a 5Corona6 with a 9lime
  ...Heinekin:say     3Only the Best for the Best! A 15,15 15,3HEINEKIN3,0 for 15,3 $1 
  ...Screwdriver:say     mixes5 $1 6a7 screwdriver6.. a little 7OJ 6and 5Vodka6 for ya!
  ...Tequila Sunrise:say     A little5 Tequila,7 a little OJ,3 a some grenadine..6 Here ya go $1 1- One 7T5e7q5u7i5l7a 5S7u5n7r5i7s5e3!
  ...Champagne:say     says lets celebrate $1 & breaks out the 10o12o11OO12o10o11 Champagne! 10o12o11OO12o10o
  ...Peach Wine Cooler:say     offers 7 $1 6a cold 7peach wine cooler!
  ...Strawberry WineCooler:say     rushes to the fridge and gets4 $1 6a cold 4strawberry wine cooler!
  ...Bloody Mary:say     puts on the bartendar hat and starts mixing.. 5vodka, 4tomato juice, 1,8lemon juice,5,0 Worcestershire sauce, 4Tabasco, 6& 9lime 6.. Voila! 4A Bloody Mary 6for5 $1
  ...Strawbry Daiquri:say     gets out the blender and whips up a 4strawberry daiquri 6for4 $1 6.. Would you like whipped cream on top?
  ...Martini:say     offers $$1 a double martini with3 2 olives,6 a cute little13 pink umbrella,6 and a big chunk of 8,6pineapple6,0!
  ...Fuzzy Navel:say     feels a little 1,8fuzzy6,0 & wants to share with7 $1 1.. 1,7 Peach Schnapps, 7,0 Orange Juice,1,0 & 1,8 Lemonade 0,0 7,1F8u7z8z7Y 8N7a8v7e8L
  ..Underage drinks
  ...Enter a drink:{ var %drink $$?="Which drink will you share?" | say     passes 15,1 $1  a %drink }
  ...Soda for All: /say asks soda anyone?6??? 15,15 8,3 MtDew 15,15 ...15,15 0,3Sprite15,15 ...15,15 0,4 coke~ 15,15 ...15,15 0,3 74,3°0,3UP 15,15 ...1,15 0,4 DIET ... 15,15 8,5DrPepper15,15 ...15,15 0,6Grape15,15 ...15,15 4,12P0e4P0s4I15,15 ...15,15 15,5RtBeer15,15 
  ...Coke:say     hands $1 a cold 15,150,4Coke15,15  Enjoy!!!
  ...Coke:say     offers Ziggy7 an ice cold Can of Classic 0,15 0,4CoKe 
  ...Pepsi:say     2hands14 $$1 2a 4nice 12cold pepsi ! 2Cheers !!! 
  ...Pepsi: /say 4hands $1 an ice cold 15,15!4,12Pepsi15,15!
  ...7up:say     0,3tosses  $1  a 74,3°0,3up 0,0   1,8:)
  ...7up:say     passes out icey cold cans of 15,15!1,140,3 74,3°0,3UP 15,15!
  ...Prune Juice:say     Pucker Up $1 - Here's a nice cold glass of 6prune juice1.
  ...Tomato Juice:say     $1 .. You could of had a 4V3-8 1but you'll have to settle for 4tomato juice!
  ...Apple Juice:say     5An 4apple 5a day keeps the doctor away! So here's a glass of 4apple juice3 $1 5becuz I luvs ya!
  ...Orange Juice:say     hands7 $1 6a freshly squeezed glass of 7orange juice!
  ...Carrot Juice:say     gets out the 7j3u4i5c8e 6blender and makes7 $1 6a healthy glass of 7carrot juice.
  ...Grape Juice:say     stomps some grapes 1,6ðððððð6,0 and hands 1,6 $$1 6,0 a nice glass of grape juice!
  ...MountainDew:say     hands4 $1 a cold caffeine boosting 4Mountain 15,153,3.4,3DEW3,3.15,15
  ...DrPepper:say     1pours5 $1 1a cold, fizzy 4,4.0,5DrPepper®4,4.
  ...Root Beer:say     tells5 $$1 6they aren't old enuf for real beer and hands them a 4,4.0,5RtBeer4,4.
  ...Orange Slice:say     hands7 $$1 a refreshing and cold 7Orange 15,15 0,7Slice
  ...Sprite:say     gives3 $$1 6a refreshing 15,15!1,140,3 Sprite 15,15!
  ...Mr.Pibb:say     tosses5 $$1 6a cold 4,4.0,5MRPIBB®4,4.
  ...Sharps:say     offers7 $1 6a cold 7Miller Sharps
  ...O'douls:say     offers3 $1 6a cold 3O'douls
  . ..ShirleyTemple:say     12Because of your age6 $1 12.. I can only offer you a 6Shirley Temple12 to drink
  ...Budweiser:say     1hands4 $1 1a cold 0,4Budweiser
  ...Long Island IceTea:say     offers5 $1 6an 5Ice Tea 1-4OH WAIT1- 5Vodka, 7Tequila, 3Rum, 5Gin, 0,4Coca-Cola,1,0 & 1,8a twist of lemon1,0- 5Long Island Ice Tea
  .  .Hot drinks
  ...HotCoco:say     4,4.. 12,12... 0,1Going for
  ...Cappuccino!:say     serves up capp  ..Coffee:say     8,5__5P 8,5__5P  8,5__5P   8,5 Coffee for $1   8,5__5P 8,5__5P 8,5__5P 
  ...Budlite:say     Gets up and gets a beer for $1 4,4.2,14BUDlite4,4.
  ...Coors:say     gets $$1 a 14,14r1,8COORS®14,14r
  ...Coffee :say     makes a cup of nice coFfeE and takes it to 13,1 $1 ...... | say     Sugar fer ya ? 
  ...Tea:say     hands $$1 a cup of tea 12[  ]) 
  ...Espresso:say     gives $$1 a 4HOT 6shot of 4Espresso! 5,5[_]5,0]
  ...Hot Buttered Rum:say     3hands 7,5 $$1 3 a cup of Hot Buttered Rum 5,7[_]D3,0 mmmmm!
  ...Cocoa:say     hands $$1a mug of 5hot chocolate 0,4"""4]6
  ...Mochachino:say     5How's about a mochachino? 8,5 $$1 5,0 C[__]
  .Snacks
  ..Pretzels:say     packs a bowl of chips and pretzels and sends it across $active   to 11,12 $1 
  ..Offer JuicyFruit:say     offers $$1 a 1,8Juicy Fruit
  ..Hersey:say     Heres a 15,15.5,5.415,5HERSHEY'S4,45,5.15,15.1 for you $$1
  ..skittles:say     1Here's some0,0...15,15.1,9S0,10K0,4I1,8T0,11T0,4L0,10E10,9S15,15.0,0...1 $$1
  ..McDonald's:say     takes $$1 to 8,4/\/\McDonald's
  ..Skittles:say     offers 8,1 $$1  some 4<S>13<K>6<I>7<T>8<T>9<L>11<E>10<S>  Taste the rainbow
  ..M Ms red best:say     gives $$1 a pack of 4,1 :8,1:3,1:2,1:13,1:4,1:8,1:3,1:2,1:13,1:8,5M&M's4,1 :8,1:3,1:2,1:13,1:4,1:8,1:3,1:2,1:13,1:1 enjoy, my friend
  ..Give juicyfruit:say     3gives $$1 4a SuniSide1 stick of 15,15o1,8<~Juicy-Fruit~>15,15o
  ..Mint:say     offers 4 $$1 10,0 a stick of 15,15l4,9<~ DoubleMint ~>15,15l
  ..Big Red:say     offers $$1 a piece of 15,15l00,04<~ Big Red ~>15,15l
  ..Pepper Mint:say     gives $$1 a stick of 15,15.0,2 PEP-O-MINT 15,15.
  ..Juicy Fruit:say     gives $$1 a stick of 15,15l4,8<~Juicy-Fruit~>15,15l 
  ..Double Mint:say     Offers $$1 some 14,14.1,90<~1,9Double-Mint0,9~>14,14.
  ..Heshey's:say     Hey $$1 wanna share my 15,15.5,5.415,5HERSHEY'S4,45,5.15,15.10 bar with me?
  ..Sausage Roll:{ say 14¤6 $me 2Offers10 $$1 $+ 2 A Sausage Roll14 ¤ }
  ..Sandwich:{ say 14¤ 6$me 2Offers10 $$1 $+ 2 A Sandwich14 ¤ }
  ..Fish & Chips:{ say 14¤6 $me 2Offers10 $$1 $+ 2 A Portion Of Fish & Chips14 ¤ }
  ..M&M's:/me 4G3Ï4V3Ê4§3 $$1  1M1&1M1'1s1 4(4m4)4 3(3m3)3 8(8m8)8 12(12m12)1 14(14m14)1 4m4m4m4m4 4t4h4e4y4'4r4e4 4g4o4o4d4 4!4!4!4
  ..M&M's2:/me  gives  $$1  some M & M 's 2(0,2m2)1,0 9(1,9m9)1,0 3(1,3m3)1,0 8(1,8m8)1,0 4(1,4m4)1,0 9(1,9m9)1,03(1,3m3)1,02(0,2m2)10 they melt in your mouth, not in your han
  ..Gum:/me offers 4 $$1 10,0 a stick of 15,15l4,9<~ DoubleMint ~>15,15l
  ..£ô££îPôp:/me gives $* a yummy £ô££îPôp 4O12--
  ..Pepmint:/me hands $$1 a 0,2 PEP-O-MINT2,2.
  ..Lifesavers:say     shares his 15,15.16,2L1,9I16,3F16,4E1,8S16,10A1,4V16,10E1,9R16,5S15,15. with13 $$1!!!
  .Flirties
  ..Wiggle:me says ....4ShOw mE ThE MoVeS BaBy! Lets Go CuTiE !!  10cAn I tAkE U hOmE To mUmMa ??!! 13WiGgLe WiGgLe !! GiVe It  Me BiG tImE !!!! 3WiGgLe WiGgLe !!12 $me holds her boobies cause her butt aint the only thing that wiggles an)))LOUD(((( 3...... and moves her body to the beat of the music...7jiving her hips.....13and shaking her butt....10like a woman possessed.............!!!!!!!!!!!
  ..Rock with ya Baby:me 12jum..HotCoco:say     4,4.. 12,12... 0,1Going for HotCoco..brb:)12,12....4,4...
  ..Table Dance:me 10does a 12complicated dance step and 3hears the crowd go wild.....7so $me climbs onto the table....9and does a few more...7after all 13she love centre stage!!!!!!!!
  ..Slow dance:me 7steps up slowly to $1... 10and wraps mah arms around his strong body...4pulling him in close and whispers in his ear.... 9mmmmmmm baby... 12hold me close and let's slow dance!!!!!!!
  ..Shakes Butt:me shakes mah butt ~ (_/_) ~ (_I_) ~ (_\_) ~(_I_) ~ (_/_) ~ as she dances around the room ... ChA cHa ChA! $$1 less do it! Move your butt this way ~ (_/_) ~ (_I_) ~ (_\_) ~(_I_) ~ (_/_) ~ WOO HOO, shake it baby!!!
  ..shakin Booty!:me 13,1 is shaking mah bootie0,1 (_I_)13,1~0,1(_\_)13,1~0,1(_I_)13,1~0,1(_/_)13,1~0,1(_I_) 
  ..Boy Scout: {
    say     1,0~1»7}4®7{1«1~5 $$1 1,0~1»7}4®7{1«1~
    say     7 Were you in Boy Scouts?...
    say     10 Because you sure have tied me in a knot!!!!
  } 
  ..Elmo: {
    say     1,0~1»7}4®7{1«1~10 $$1 1,0~1»7}4®7{1«1~
    say     7 My NaMe ISN't 2ELmO,
    say     10 BuT YoU CaN  TiCKLe Me  AnyTimE  12:7-4)  12:7-4)
  }
  ..Genie: {
    say     1,0~1»7}4®7{1«1~4 $$1 1,0~1»7}4®7{1«1~
    say     7 A MaN OnCE AsKED A GeNiE To MaKe HiM A BiLLiON TiMeS SmaRTeR
    say     7 ThAn WhAt He ALrEaDy WaS... ThE  GeNiE  MaDe HiM A WoMaN!!!!
  }
  ..hereAfter.. {
    say     1,0~1»7}4®7{1«1~2 $$1 1,0~1»7}4®7{1«1~
    say     7 Do you believe in the hereafter  ?? 
    say     2 Yes....Then you know what I'm here after   12:7)4~
  }
  ..Homeless: {
    say     1,0~1»7}4®7{1«1~7 $$1 1,0~1»7}4®7{1«1~
    say     2 HeLp ThE HoMeLeSS!!
    say     10 TaKe Me HoMe WiTH YoU!!!  2:7}4~
  }
  ..I'm Lost: {
    say     1,0~1»7}4®7{1«1~10 $$1 1,0~1»7}4®7{1«1~
    say     7 i'M LoST....CaN YoU GiVe Me DiReCTiONs
    say     10 To YoUR © heart © ??....  2:7=4) 
  }
  ..R U Religious: {
    say     1,0~1»7}4®7{1«1~2 $$1 1,0~1»7}4®7{1«1~
    say     7 Are you religious?...
    say     2 Cause I'm the answer to all your prayers!!!  12:7/4~
  }
  ..Teddy Bear: {
    say     1,0~1»7}4®7{1«1~7 $$1 1,0~1»7}4®7{1«1~
    say     4 I LoSt My TeDDY BeAr...
    say     10 WiLL YoU SLeeP WiTH Me  ???  2:7}4~
  }
  ..Vegas: {
    say     1,0~1»7}4®7{1«1~10 $$1 1,0~1»7}4®7{1«1~
    say     7 Ya WaNNa Go To2 VeGas?? 
    say     10 CuZ..I KnoW I CouLD GeT  LuCKY  WiTH YoU!!!  2:7=4) 
  }
  ..Tear in my Eye: {
    say     1,0~1»7}4®7{1«1~10 $$1 1,0~1»7}4®7{1«1~
    say     7 If YoU WeRE A TeaR In My EyE...
    say     10 I WouLD NoT CrY FoR FeaR Of LoSinG You!!!  2:7=4) 
  }
  .miscellaneous
  ..Little Gray Robe:say     sneaks up behind sweetie 11,12 $$1  and crawls into that cute little gray robe for some fun.......  
  ..Blue Teddy:say     12,1 {~.9,1_12,1.~} *13,1 $$1 12,1* {~.9,1_12,1.~} | say     12,1 -( 9,1Y12,1 ) --13,1 $$1 12,1-- ( 9,1Y12,1 )- | say      12,1 ()~4,1*12,1~() *13,1 $$1 12,1* ()~4,1*12,1~() | say     12,1  (_)-(_) -13,1 $$1 12,1- (_)-(_)
  ..Love Heart:say     16...,20.d88b.d88b, |  say     16....208888818I2088888 |  /say 16....20`Y8818LUV2088Y'  |  say     16......20`Y818U208Y' |  /say 16........20`Y' | say         20 luvs18 $$1 
  ..Just 4 U:say      0--------12.13.12.13.0---13.12.13.12. | say     0------12.13.0----12.13.12.0----13.12. | say     0-----12.13.0------12.0------13.12. | say     4=>>==12.13.0---4=======0---13.12.4===>> | say     0------12.13.0----------13.12. | say     0--------12.13.0-------13.12. | say     0----------12.13.0---13.12. | say     0-------------12. | say     14Just for you3 $$1 2:4)
  ..SendsLove:say     3,1.»º4,1©3,1º«.3,1.»º4,1©3,1º«.0,1sends3,1.»º4,1©3,1º«.0,1hugs3,1.»º4©3º«.0,1and3,1.»º4©3º«.0,1kises*3,1.»º4©3º«.0,1to sweet3,1.»º4©3º«.0,1loveable3,1.»º4©3º«.0,14 $$1 ***
  ..On Empty:say     is 6running on 4,1E\15-----15----15--4,1F 8time to 8hit the sack.....10see ya laters12~ 12~ 12
  ..BlowKiss:say     13 blows a kiss to 3 $$1 ... 12floating your way hon!
  ..CoolName:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,1 $$1 0,1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,10 0,11 0,12 0,14 0,15 
  ..tripleheart:say     2(¯`·._4(¯`·._3(¯`·._(2*10 $$1 2*3)3_.·`¯)4_.·`¯)2_.·`¯) | say     2(_.·´¯4(_.·´¯3(_.·´¯(4*10 $$1 4*3)3¯`·._)4¯`·._)2¯`·._)
  ..Angels:say     sends angels to watch over  ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~ * ~Å~* $$1 !!! * ~Å~ * ~Å~*
  ..BlueHeart:say     12,1(¯`·._(¯`·._(¯`·._1,12( $$1 )12,1_.·´¯)_.·´¯)_.·´¯) | say     12,1(_.·´¯(_.·´¯(_.·´¯12,1( $$1 )12,1¯`·._)¯`·._)¯`·._) | say     12,1(¯`·._(¯`·._(¯`·._12,1( $$1 )12,1_.·´¯)_.·´¯)_.·´¯) | say     12,1(_.·´¯(_.·´¯(_.·´¯1,12( $$1 )12,1¯`·._)¯`·._)¯`·._)
  ..PinkHeart:say     13,1(¯`·._(¯`·._(¯`·._1,13( $$1 )13,1_.·´¯)_.·´¯)_.·´¯) | say     13,1(_.·´¯(_.·´¯(_.·´¯13,1( $$1 )13,1¯`·._)¯`·._)¯`·._) | say     13,1(¯`·._(¯`·._(¯`·._13,1( $$1 )13,1_.·´¯)_.·´¯)_.·´¯) | say     13,1(_.·´¯(_.·´¯(_.·´¯1,13( $$1 )13,1¯`·._)¯`·._)¯`·._)
  ..LetSnow:say     says let it snow....0,12:;~`:.,:*~ $$1 ~:;`:.,:*~:Let;~It~Snow~`::*~:;`:.,:*~:;~`:. $$1 ,:*~:;~`:.Let~It~Snow,:*~:;~`:.,:*~:;~`:.,:*~:;~Let~It~Snow:.,:*~:;~`:.,:*~:;~`:. ,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.,:*~:;Let~It~Snow:.,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.,:*~:;~`:.Let~It~Snow,:*~:;~`:.,:*~:;~`:.,:*~:;~`:. $$1,:*~:;~`:.,:*~~:;`:.,:*~:;~`:.,:*~:;Let~It~Snow:.,:*~:;~`:.,:*~:;~
  ..snow:say     0,15;`~,.*'',,'*,;*'~,'*"^~' ;* `,~;`*,`^';`~,.*`":,` `,~;`~`:,.`*':', `;* `,~;~`: $$1 8you are snowbound, now put your feet up and here's your blankie and your 5hot cocoa8!0,15 , .`*':',`;* ',~;`*,`^';`~,.* '`" :,` `,~;`~`:,.`*':',`;* `,~;~`:,.`  *'`":,` .*',`;*`,~;`*,` ^';`~,.*'',` ~~ ;* `,~;`*,`^';`~,.*' `":,` `,~;`~`:,.`*':',`;* `,~;~
  ..Swims:say     0,2swims around the room ! ! ! 11,2¸.·´¯`·.¸ 7,2>8,2<4,2(9,2(7,2(4,2(8,2º9,2> 11,2.·´¯`·.¸.·´¯`·.¸ 7,2<8,2º4,2)9,2)7,2)4,2)8,2>9,2<11,2 .·´¯`·.¸.·´¯`·.¸ 9,2<8,2º4,2)7,2)9,2)4,2)8,2>7,2<11,2 ¸.·´¯`·.¸.·´¯`·.¸ 9,2>8,2<4,2(7,2(9,2(4,2(8,2º7,2>11,2 .·´¯`·.¸.·´¯`·.¸ 7,2>8,2<4,2(9,2(7,2(4,2(8,2º9,2>11,2 .·´¯`·.¸.·´¯`·.¸ 7,2<8,2º4,2)9,2)7,2)4,2)8,2>9,2<11,2 .·´¯`·.¸.·´¯`·.¸ 9,2<8,2º4,2)7,2)9,2)4,2)8,2>7,2<
  ..PrettyHi:say     0,1 12563714101241391180,1 Hi 0,18119130,14121014736521 | say     0,1 12563714101241391180,1 $$1 ! 0,18119130,14121014736521
  ..PrettyName:say      10{4*10{4*10{4*10{4*10{4*10{4*10{ 2+ $$1 + 10}4*10}4*10}4*10}4*10}4*10}4*10}
  ..CoolName:say     0,15 0,14 0,13 0,12 0,11 0,10 0,9 0,8 0,7 0,6 0,5 0,4 0,3 0,2 0,1 $$1 0,1 0,2 0,3 0,4 0,5 0,6 0,7 0,8 0,9 0,10 0,10 0,11 0,12 0,14 0,15 
  ..ColorArrow:say     13-------------> $1 | say     12------------> $1  | say     11-----------> $1  | say     10----------> $1  | say     9---------> $1  | say     8--------> $1  | say     7-------> $1  | say     6------> $1  | say     5-----> $1  | say     4----> $1  | say     3---> $1  | say     2--> $1  | say     1-> $1 | say      $1
  ..bunny!:say     10Bunny0,0. 12. (Y) 0,0... 13(Y)0,0..2 $$1 0,0. 13(Y)0,0.... 12(Y)0,0..10 Hugs | say     10Bunny 12. (°.°) 0,0. 13(°.°)0,0.2 $$1 0,0.13(°.°)0,0.. 12(°.°)0,0.10 Hugs | say     10Bunny 12.()¯°¯() 13()¯°¯()2 $$1 13()¯°¯() 12()¯°¯()10 Hugs | say     10Bunny 12.(_)-(_) 13(_)-(_)2 $$1 13(_)-(_) 12(_)-(_)10 Hugs
  ..MacroAlert1:say     12,9warns MACRO ALERT<<<>>MACRO ALERT<<<>>>coming up ! | say     9,12warns MACRO ALERT<<<>>MACRO ALERT<<<>>>coming up ! | say     12,9warns MACRO ALERT<<<>>MACRO ALERT<<<>>>coming up !
  ..rapsU:say     13>>>>>>>>>>>>>>-----Wraps-----<<<<<<<<<<<<<< | say     4(¯`'·.¸(¯`'·.¸10 _____________ 4¸.·'´¯)¸.·'´¯) | say     4(¯`'·.¸(¯`'·.¸10 4 ¸.·'´¯)¸.·'´¯) | say     12---==>>>>------>13 $$1 12<------<<<<==--- | say     4(_¸.·'´(_¸.·'´ 104 `'·.¸_)`'·.¸_) | say     4(_¸.·'´(_¸.·'´10 ¯¯¯¯¯¯¯¯¯¯¯¯¯4 `'·.¸_)`'·.¸_) | say     13>>>-----In a Warm Friendly Embrace!-----<<<
  ..H&Kwav:say     12¸,øHugsø,¸,ø¤°`°^kiss,¸,ø¤°`°9 $1 12¸,ø¤°`°Hugsø,¸,ø¤°`°^ kiss¸,ø¤°`°9 $1 12¸,øHugsø,¸,ø¤°`° 
  ..angel dust:say     12Sprinkles Angel Dust All Over9 $$1 13 **.:`~Å~.:**.:`~Å~.:**.:`~Å~.:* *.:`~Å~.:**.:`~Å~.:**.:~Å~.:9 $$1 13`**.:`~Å~.:`**.:` ~Å~.:`**.:`~Å~.:`**.:`~Å~.:`**.:~Å~.:`**.:9 $$1 13~Å~ .:`**.:~Å~.:`**.:~Å~.:`**.:~Å~.:`**.:~Å~.:`** .:~Å~.:`**9 $$1 13.:~Å~.:`**.:~Å~.:`**.:~Å~4 Cause I think you're special 1,8:)
  ..SunGlasses:say     says me needs 1,8~©¡©~1,16 because everyone is so bright
  ..rdcrpt:say     13Rolls out the Red Carpet4 __________________(O) 13 for12 $$1
  ..rainbowGold:say     7,7 8,8 9,9 10,10 11,11 12,12 13,13 14,14 15,15 8,4 $$1 15,15 14,14 13,13 12,12 11,11 10,10 9,9 8,8 5,10 11You are the 8GOLD 11in my rainbow! 4,4 5,5 6,6 7,7 
  ..FaceAt:say     9makes 13funny 12faces 2*@¿@*0-3 *ô¿ô*0 -4 =^.^= 0-6 -ô.ô- 0-7 <ö.ö> 0-9 @¿@0 -10 º.º 10-11 ö.ô 0-12 ©¿© 0-13 -©¨©-0-14~¿ô0-2 ~©¿©)~0-3 @ԸÔ@0-4{*¡*}0-6~\_пÐ_/~0-7@{ԿÔ}@0-0 *0-13(^>^)0-4(^_^)0-3:c)0-13 1,14@~1,8ô4¿1,8ô~1,14@0-7=12:4c5)0-1~12@4_12@1~0 - 4 §3â4¿3â4§0 - 7§1-¿12â7§0 -7 (3¤¿3¤7)0.14 ~~(,,12°°14>4 at12 $$1 !!!!
  ..Sleepy:say     12 $$1 , you are getting... | say     9,1SLEEPY6HORNY9SLEEPY6HORNY9SLEEPY | say     9,13SLEEPY4HORNY9SLEEPY4HORNY9SLEEPY | say     9,2SLEEPY10HORNY9SLEEPY10HORNY9SLEEPY | say     9,10SLEEPY2HORNY9SLEEPY2HORNY9SLEEPY | say     9,1SLEEPY6HORNY9SLEEPY6HORNY9SLEEPY | say     9,13SLEEPY4HORNY9SLEEPY4HORNY9SLEEPY
  ..Dead:say     12thinks $$1 is dead to the World 14,1 _____________   | say     13thinks $$1 is dead to the World 1,1 14,14 9,1/\/\/\_____14,14 1,1  | say     4thinks $$1 is dead to the World 1,1 14,14 14,14 14,14 14,14 1,14E14,14 1,14K14,14 1,14G14,14 14,14 14,14 14,14 1,1  | say     10thinks $$1 is dead to the World 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 1,1 
  ..Bow:say     3sticks a shiny 4red bow 3on $$1 's head!!! 4"Ahhhh 3doesn't it look cute !!!"
  ..PingJoke:say     [  $$1 PING Reply] : 6 months 27 days 12 hours 16 minutes 55 seconds 1 millisecond
  ..Java:/me 12Hands $$1 One HoT cUp o JavA
  ..Joint:/me 12passes $$1 a nice Fat Joint 14</ / / / / / / / / / / / / / / / / /4$active  14~~~~~ ~~~~ ~~ ~~ ~
  ..Bowl:/me packs a bowl and sends it across to $$1 \    /====~~~ ~~
  .Insults
  ..Slimy Walleye:say     10slaps13 $$1 10around a bit with a large 13bug-eyed, 14stinky smellin', 12slimy feelin' 2Walleye!!
  ..Summer Sausage:say     2slaps4 $$1 2around a bit with a 7large piece of summer sausage..
  ..Feather Duster:say     10tickles 8,13 $$1  10with a big, sloppy, 13wet feather duster!!!
  ..FryingPan:say     3BOPS9 $$1 6over the head, with a 10cast iron frying pan 13(Kaboink!!)6...and tells9 $$1 6...3"You better behave yourself!!"
  ..grabs tongue and pulls:say     grabs $$1s's tongue and pulls it wayyyyy out. | say     takes out locking ring and loops it through $$1s's tongue. | say     then fastens the ring to a Monster truck and drives off.
  ..Wet Noodle:say     2,8slaps4,8 $$1 2,8around a bit with a 4,8slimy wet noodle,2,8 and asks4,8 $$1 2,8..."Would you like some 4,8sauce2,8 to go with that?!?!
  ...boob:say     Flashes mah BOOBS 
  ..Slap with...:say 14¤10 $me 2Slaps10 $$1 $+ 2 Around A Bit With A6 $$?="What Would You Like To slap $$1 with?:" 14¤
  ..wall:/me grabs $$1 by the hair and smashes $$1's head against the nearest wall. You awake now $$1 ?
  ..Glass Punch:/me wraps one hand with cloth, dips it in resin and then shards of broken glass, and punches $$1 in the face.  
  ..Attention:{ say 14¤ 2Sometimes You Have To Slap10 $$1 $+ 2 In The Face To Get Attention!! 14¤ }
  ..Chicken:{ say 14¤10 $me 2Slaps10 $$1 $+ 2 Around A Bit With A 14Rubber Chicken 14¤ }
  ..Wedge:/me grabs $$1's underware. pulls it over $$1's Head.....   Now you look much better. The brown stain acsentuates the shoes. 
  ..Paint:/me thinks the only way $$1 can look better is with a bucket of Bondo and a coat of Paint. 
  ..LikeSlap:/me slaps $$1 around a bit and $$1 likes it... 
  ..Lamer:/say $$1 is a lamer :)  
  ..Bath:/me thinks that $$1 REALLY needs a BATH..... :) 
  ..Cactus:/me slaps $$1 around a bit with a large cactus!
  ..Whale:/me slaps $$1 around a bit with a small whale!
  ..Snot:/me grabs a snotty tissue and rubs it all over $*
  ..Toss In River:/me tosses $$1 in da river ¸.·´¯`·.¸.·´¯`·.¸¸.·´¯`·.¸ ><((((º>  $$1 <º))))><
  ..Rat:/me 13says $$1 you dirty 9,1"Rat! Rat!Rat!13~~(¸¸ ¸¸ºº> 6~~(¸¸ ¸¸ºº> 10~~(¸¸ ¸¸ºº>
  ..Attention:{ say 3¤ 12Sometimes You Have To Slap10 $$1 $+ 12 In The Face To Get Attention!! 3¤ }
  ..Chicken:{ say 3¤10 $me 12Slaps10 $$1 $+ 12 Around A Bit With A 14Rubber Chicken 3¤ }
  ..Thwap:/me *Thwaps* $$1
  ..Bap:/me *Baps* $$1
  ..Hi5:/me slaps $$1 a Hi 5
  ..Feather:/me beats $$1 senseless with a feather 
  ..Moon:/me moons $$1 $+ ! (_|_)
  ..Pool:/me tosses $$1 into a pool! *SPLASH*
  ..Steel Kick:{ say 14¤10 $me 2Puts On A Steel-Toed Boot And Kicks6 $$1 $+ 2 Right Outta The Channel! 14¤ }
  ..Shotgun:{ say 14¤10 $me 2Takes A Shotgun And Blows6 $$1 $+ 's2 Head Off To Another Channel ... You Can Pick It Up There 14¤ }
  ..Frisbee:{ say 14¤10 $me 2Takes A Frisbee With Extremely Sharp 15Diam14<>15nd 2Tipped Edges ... And Flings It At6 $$1 $+ 's2 eyes ... 14¤ }
  ..Balls:{ say 14¤10 $me 2Takes A Sturdy Golf Club And ... Repeatedly Whacks6 $$1 $+ 's2 Balls With It ... I Can Feel Your Pain ... I Lie ... No I Can't 14¤ }
  ..Bat:/me steps up to the plate and takes a practice swing... | /say Here's the pitch! | /say Oops!  $1 got his head in the way!  | /say Ouch!  I bet that hurt!
  ..Fork:/me Takes Out A Fork... | /me Approaches $1... | /me Rams The Fork Up $1's Nose.
  ..stone:/me has been turned to stone cause of $1 $+ 's uglyness!
  ..Match:/me sticks a lit match between $1 $+ 's toes!
  ..Bat:/me gets a Steel Bat and smashes $* with it " BATTER UP!! "
  ..Surprise:/me sneaks up behind $1 and puts a live tarantula down $1 's pants!! " HeHeHeHe!
  ..Block Attack:/me 4blocks $$1 's futile and hopeless attack!
  ..Glare at User:/me 4glares sharply at $$* $+ !
  ..Punch User:/me 4punches $$* sharply in the nose!
  ..Shoot User:/me 4blows $1 away with a double barrel shotgun!
  ..Grenade User:/me 4takes out a grenade ... pulls the pin ... 4 ... 3 ..2 ... tosses it at $$*! | /say 4Damn...what a BLAST!!!
  ..Hang User:/me 4puts a tight rope around $$1 's neck and opens the channel trapdoor! | /say 4hung!!!
  ..Cannon user:/me 4shoves a cannon up $$1 's arse and gives an evil grin and lights the fuse!
  ..Sabre user:/me 4backstabs with a light sabre then begins to slice and dice $$1 
  .Faces
  ..Sad:/me is really sad     11ú4¿11ù
  ..Sleep:/me needs sleep... b.a.d..l..y... *yawnnnn*    14/4¿14\
  ..Depress:/me is depressed       1-4¿1-
  ..Angry:/me is pissed      4ò1¿4ó
  ..Confused:/me is purty darned confused        13+4¿13+
  ..Spy:/me is spying          15¬4¿15¬
  ..Think:/me is thinking *hard*     7>4¿7<
  ..What:/me goes " WHAT ?!?!?! "             8Õ4ç8Õ
  ..Puzzled:/me is puzzled     10*4¿10*
  ..Wink:/me winks     2ô4¿12~
  ..Boingface:/me goes "BOING!"    6Ô4¿6Ô
  .Sprinkles
  ..Fireworks 1:/say shoots fireworks for $$1 3*15~~~~~7$active  $active  $active  $active  $active  12> 5 ~`;.~`;13.*`;.`~3;`~`   $$1 3*15~~~~~7$active  $active  $active  $active  $active  12> 5 ~`;.~`;13.*`;.`~3;`~` $$1 3*15~~~~~7$active  $active  $active  $active  $active  12> 5   ~`;.~`;13.*`;.`~3;`~` $$1 3*15~~~~~7$active  $active  $active  $active  $active  12> 5 ~`;.~`;13.*`;.`~3;`~`
  ..Bubbles1:/me blows 4,8 festive bubbles  at 13 $$1 11© ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ° ©12 catch them4 if you can!..Bubbles2:/me 12blows13bubbles for 9©º°¨¨°º©13©º°¨¨°º©9©º°¨¨°º©13©º°¨¨°º©12 $$1 13©º°¨¨°º©9©º°¨¨°º©13©º°¨¨°º©9©º°¨¨°º©
  ..Angel dust:/say Sprinkles Angel Dust All Over7 $$1 13 **.:`~Å~.:**.:`~Å~.:**.:`~Å~.:* *.:`~Å~.:**.:`~Å~.:**.:~Å~.:7 $$1 13`**.:`~Å~.:`**.:` ~Å~.:`**.:`~Å~.:`**.:`~Å~.:`**.:~Å~.:`**.:7 $$1 13~Å~ .:`**.:~Å~.:`**.:~Å~.:`**.:~Å~.:`**.:~Å~.:`** .:~Å~.:`**7 $$1 13.:~Å~.:`**.:~Å~.:`**.:~Å~ Cause i think your special 1,8:)
  ..Angels2watch:/say 4sends angels to watch over  13 $$1 14  ~Å~ ~Å~ ~Å~ 7 $$1 14 ~Å~ ~Å~ ~Å~ 3 $$1 14 ~Å~ ~Å~ ~Å~ 9 $$1 14 ~Å~ ~Å~ ~Å~ 
  ..Magic dust:/me Sprinkles magic dust over $$1 ,8``~`:,.`*':',`;* $$1 9`,~;`~`:, $$1 10.`*':',`;`,~;~`: $$1 12,.`*':',`;*`,~ $$1 13; `*,`^';` ~,. $$1 14*'`": ,`~`:,.`*`,~;`*,` ^ $$1 7 ';`~,.*'`":,` ~`:,.` $$1 4 *':',`;* ~;`*,`^';`~,.*'`":,` $$1 8~`:,.`*`,~;,``~`:,.`*':',`;*` $$1 15,~;`~`:,.`*':',`;`,
  ..Star dust:/me 12pulls out a bag of 4stardust12 and sprinkles15 `,',',',',',','.',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',',' $$1 13... $$?="Enter Message" 4!!!..Brighten world:/me 1,8  1,4 1,8  1,7 1,8  1,9 1,8  1,10 1,8           $$1,   You Brighten Up My World!!         1,10 1,8  1,9 1,8  1,7 1,8  1,4 1,8  
  ..Color band:/say 4 * (¯`'·.¸(¯`'·.¸ _______________ 4¸.·'´¯)¸.·'´¯)  | /say 4 * (¯`'·.¸(¯`'·.¸12 x8x9x10x11x12x13x4x12x7x4x13x8x15x12x 4¸.·'´¯)¸.·'´¯) | /say 3 ----==>>>>------>14 $$13  <-------<<<<==---- | /say 4 * (_¸.·'´(_¸.·'´13 x8x9x10x11x12x13x4x9x7x10x11x12x13x15x 4`'·.¸_)`'·.¸_)  | /say 4 * (_¸.·'´(_¸.·'´1 ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯4 `'·.¸_)`'·.¸_)
  ..Background band:/say 4,11 * (¯`'·.¸(¯`'·.¸13 !!!!!!!!!!!!!!! 4¸.·'´¯)¸.·'´¯) *   | /say 4,11 * (¯`'·.¸(¯`'·.¸12 x8x9x10x11x12x13x4x12x7x4x13x8x15x12x 4¸.·'´¯)¸.·'´¯) *  | /say 3,11 ----==>>>>------>12 $$1 3<------<<<<==----  | /say 4,11 * (_¸.·'´(_¸.·'´13 x8x9x10x11x12x13x4x9x7x10x11x12x13x15x 4,11`'·.¸_)`'·.¸_) *  | /say 4,11 * (_¸.·'´(_¸.·'´ 13,11¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡4,11 `'·.¸_)`'·.¸_) * 
  ..Waves  name:/me  11- $$1-_¸,.»¬=椺²°`¯`°²º¤æ=¬¤æ=¬«.,¸_- $$1- | /me 9- $$1-,_¸,.»¬=椺²°`¯`°²º¤æ=¬¤æ=¬«.,¸_- $$1-  | /me 10- $$1-=,_¸,.»¬=椺²°`¯`°²º¤æ=¬¤æ=¬«.,¸_- $$1- | /me 3- $$1-`=,_¸,.»¬=椺²°`¯`°²º¤æ=¬¤æ=¬«.,¸_- $$1- 
  ..Confetti:/me  Celebrates $$1 by Tossing Confetti all round the room4" '~`*+,. 3`*`'" ~ :, 9, ~ ' " -;. 11, : ' ` ` * 12- _ + ^ : 13,, .+ * ^ * - ,14.:" '~`*+,. 15`*`'" ~ :, 6, ~ ' " -;. 7, : ' ` ` * 8- _ + ^ : 9,, . + * ^ * - , 10.:" '~`*+,. 11`*`'" ~ :,12_ + ^ : 13,, .+ * ^ * - , 14.:" '~`*+,. 15`*`'" ~ :, 4, ~ ' " -;. 3, : '" '~`*+,. 2`*`'" ~ :, 9, ~ ' " -;. 6, : ' ` ` * 5- _ + ^ : 4 ,, .+ * ^ * - , 10.:" '~`*+,. " '~`*+,. 13`*`
  .Erotic' n'Sexy
  ..Women Pick These
  ...Boxer Inspection:/me whips out her boxer inspectors badge... handing out free badges to all females in the room
  ...Ice Cream:/me gets $$1 a bowl of ice cream and covers him in the cold cocotion.. then starts licking the cool ice cream off of $$1 , biting him here and there.. mmmmmmmmmm
  ...Passionate Kiss:{ say 14¤10 $me 2Slowly Walks To6 $$1 $+ 2 Looks Deep Into His Eyes Gently Traces His Lips With Her Finger Before Leaning Towards Him And Gently Sucking And Nibbling His Lips Driving Him Wild With Desire 14¤ }
  ...Neck Kiss:{ say 14¤10 $me 2Gently Holds6 $$1 $+ 's2 Face While Giving Him A Hot Passionate Kiss Before She Starts Softly Kissing His Neck 14¤ }
  ...Heart Capture:{ say 14¤10 $me 2Whispers Into6 $$1 $+ 's2 Ear That He Is The Most Handsome Man Ever To Capture Her Heart 14¤ }
  ...Boob Jump:{ say 14¤10 $me 2Rips Off All Clothes, Climbs On 6 $$1 $+ 's2 Chest And Starts Jumping Up And Down ...10 (.)(.) (')(') (.)(.) (')(') (.)(.) (')(') (.)(.) (')(') (.)(.) (')(') (.)(.) (')(')2 ... Yippeeeeeee ... 14¤ }
  ...WakeUp Stroke:{ say 14¤10 $me 2Caresses6 $$1 $+ 2 As He Sleeps Slowly Stroking Up And Down Your Inner Thigh As She Gently Kisses Around His face ... Wake Up Sweety!! 14:2)) 14¤ }
  ...Butt Wiggle 1:{ say 14¤10 $me 2Slowly Wiggles Her Butt In The Direction Of6 $$1 $+ 214 ¤ }
  ...Butt Wiggle 2:{ say 14¤10 $me 2Dances Closer To6 $$1 $+ 2 And Wiggles Her Butt In His Face. 14¤ }
  ...Butt Wiggle 3:{ say 14¤10 $me 2Wiggles Her Butt Side To Side Being A Bit Naughty Thinking6 $$1 $+ 2 Needs To Spank Her. 14¤ }
  ...Strip:{ say 14¤10 $me 2Takes Off Her Skirt And Dances Around The Room Shaking Her10 (*)(*)2 And10 (_!_)2 To All Who Want To See. 14¤ }
  ...Snuggle:{ say 14¤10 $me 2Starts To Play Some Romantic Music ... Lights A Few Scented Candles And Snuggles Up Warm And Cosey With6 $$1 $+ 214 ¤ }
  ...Dessert:{ say 14¤10 $me 2Makes A Surprize Romantic Dinner With6 $$1 $+ 2 As Dessert... 14¤ }
  ...Shower Share:{ say 14¤10 $me 2Wants To Share A Hot, Erotic Shower With6 $$1 $+ 214 ¤ }
  ...Play Doctors and Nurses: {say 14¤10 $me 2Would Like To Play Doctors And Nurses With6 $$1 $+ 2. Wanna Be My Patient? 14:2D 14¤ 
  ...Ice Cube Tease:{ say 14¤10 $me 2Slides Up Close To6 $$1 $+ 2 And Starts Teasing His Hot Body With An Ice Cube. 14¤ }
  ..Men Pick These
  ...Hand:/me walks over to $$1 takes her hands in his n kisses them softly
  ...Giggles:/me listens to the cute giggles n blows softly onto $$1 's neck
  ...Panties Inspection:/me whips out his panties inspectors badge... handing out free badges to all men in the room
  ...Passionate Kiss:{ say 14¤10 $me 2Slowly Walks To6 $$1 $+ 2 Looks Deep Into Her Eyes Gently Traces His Lips With His Finger Before Leaning Towards Her And Gently Sucking And Nibbling Her Lips Driving Her Wild With Desire 14¤ }
  ...Neck Kiss:{ say 14¤10 $me 2Gently Holds6 $$1 $+ 's2 Face While Giving Her A Hot Passionate Kiss Before He Starts Softly Kissing Her Neck 14¤ }
  ...Heart Capture:{ say 14¤10 $me 2Whispers Into6 $$1 $+ 's2 Ear That She Is The Most Beautiful Woman Ever To Capture His Heart 14¤ }
  ...Butt Shake:{ say 14¤10 $me 2Seductively Walks Over To6 $$1 $+ 2 And Shakes His Hot, Oiled ...10 (_Y_(  (_Y_(  (_Y_)  )_Y_)  )_Y_)2 ... Ohhh Yeahhh ... 14¤ }
  ...Body Caress:{ say 14¤10 $me 2Carries6 $$1 $+ 2 To The Sofa.. Lowers Her Gently And Loses Himself In A Hot, Passionate Kiss As He Caresses Her Silky Body. 14¤ }
  ...Belly Lick:{ say 14¤10 $me 2Gets6 $$1 $+ 2 Comfortable On A Bed With Lots Of Cushions And A Silk Sheet. Then10 $me 2 Gently Sucks Around6 $$1 $+ 2 Belly Button. 14¤ }
  ...Strip:{ say 14¤10 $me 2Slowly Takes Off His Shirt Exposing His Hard Tanned Upper Body. 14¤ }
  ...Snuggle:{ say 14¤10 $me 2Starts To Play Some Romantic Music ... Lights A Few Scented Candles And Snuggles Up Warm And Cosey With6 $$1 $+ 214 ¤ }
  ...Desert:{ say 14¤10 $me 2Makes A Surprize Romantic Dinner With6 $$1 $+ 2 As Desert... 14¤ }
  ...Shower Share:{ say 14¤10 $me 2Wants To Share A Hot, Erotic Shower With6 $$1 $+ 214 ¤ }
  ...Ice Cube Tease:{ say 14¤10 $me 2Slides Up Close To6 $$1 $+ 2 And Starts Teasing Her Hot Body With An Ice Cube. 14¤ }
  ..Hula:{ me does a romantic Hula dance with $$1 6\o/ 14<o> 4<o/ 10\o> | me )) | me (( | me )) | me 6\o/ 14<o> 4<o/ 10\o> }
  ..Sauce:/me covers $$1 's in carmel sauce mmmmmmmmmmmm YummY!
  ..Bites:/me bites $$1 's back playfully.. :D *rrrrrrrrrrrrr*
  ..Whistle:/me looks at $$1 then drools starts losing control. Enjoying a 6Yummy butt grope as I give a sexy whistle.. whoooooooooa! :)
  ..Whisper:/me walks over and gives a little kiss on $$1's neck and then stays there like $me is whispering something seductively in $$1's ear. | /say i wonder what that could be
  ..Cream:/me pours whipped cream all over $$1 and asks shyly, Do you want me to lick it off?
  ..Dance:/me 12dances slowly and seductively with6 $$1!
  ..Massage:{ say 14¤10 $me 12longs to give 6 $$1 12a sensual massage 14¤ }
  ..Towel:{ say 14¤10 $me 12waits for 6 $$1 12 for a tongue drying after a steamy, hot shower.. mmmm }
  ..Butt Grope:{ say 14¤ 12(((((((((((((((10 $$1 $+ 12 ))))))))))))))12, I 10Love 12Groping Your Butt 14¤ }
  ..Eye Brow Lick:{ say 14¤10 $me 12Licks6 $$1 $+ 6's 12Eyebrows 14o.O 14¤ }
  ..Birthday Suit:{ say 14¤10 $me 12Slides Up Close To (((((((((((10 $$1 $+ 12 )))))))))) 12Wearing Her Birthday Suit 14¤ }
  ..Passionate Hug:{ say 14¤ 12(((((6 $me 12))))) Pulls10 $$1 $+ 12 Into6 $me 's 12Arms And Hugs10 $$1 $+  12 In A Mind Blowing, Spine Tingling, Gotta Run To The Emergency Room, Fallen And Can't Get Up, Make The Phone Ring 6Way 14¤ }
  ..Miss Me?:{ say 14¤ 12((((( 6 $me 12))))) Walks Up To10 $$1 $+  12, Turns And Spread Eagles10 $$1 $+  12 Against The Wall, Gently Pressing 6My12 Body Against10 $$1 $+ 's12 Back, Then Slowly Reaches Around, Softly Draws The Right Hand Up10 $$1 $+ 's12 Inner Thigh, Kisses10 $$1 $+  12 Softly On The Neck And Whispers In A Low Sultry Voice 10" 6Did you miss me? 10" 12*wink* 14¤ }
  ..Orgasm:{ say 14¤10 $me 12Has Multiple 10Facial 12Orgasms 14¤ }
  ..Magic:{ say 14¤10 $me 12Puts On The Cerimonial Robe, Gets Out The Magic Dildo ... Err Wand ... Waves It In Pattern At6 $$1 $+  12 ...10 $me 12Is To Sexy To Leave 14¤ }
  ..Horny:/me 6 $$1, you are getting.....9,1SLEEPY6HORNY9SLEEPY6HORNY9SLEEPY9,13SLEEPY4HORNY9SLEEPY4HORNY9SLEEPY9,2SLEEPY10HORNY9SLEEPY10HORNY9SLEEPY 9,10SLEEPY2HORNY9SLEEPY2HORNY9SLEEPY9,1SLEEPY6HORNY9SLEEPY6HORNY9SLEEPY
  ..Lips:/me 1,1 ---4 .. .. 1 ---4 A | /me 1,1 -4 .' 1 4` 1 4`. 1 -4 kiss | /me 4,1 .'_.-...-._`. 4 for | /me 1,1 4`. 1 ----- 4 .'1 4 you | /me 1,1 --4 `-...-' 1 --4 $$1
  ..Deep kiss:/me Gives $$1  3a 4SLoW..5LoNG..6DeeP..7PeNeTRaTiNG..9ToNSiL-TiCKLiNG..10HaiR 11STRaiGHTeNiNG..12Toe-CuRLiNG..13NeRVe-JaNGLiNG..14LiFe-aLTeRiNG..15FaNTaSY-CauSiNG..2 i 3JuST 4SaW 5GoD!..6GoSH, 7DiD 9MY 10CLoTHeS 11FaLL 12oFF?..13YeS, 14i'M 15GLaD 2i 3CaMe 4oN 5iRC..6KiSS 7oN 9Da 10LiPS!!!
  ..kiss&Hug:/ME 12,8gives $$1 a *Super Tight And Really Super Erotic Groping Fondling Earth Shattering Sonic Boom Oh Gawd If You Stop I'll Kill You Better Than Heaven Yet Hotter Than Hell Watch Where Ya Stick Yer Hands Super Huge Oh Gawd Oh Gawd Oh Gawd Don't Ya Dare Stop Touching Me Huggeroonies And A Knees Are Shaking Earth Is Trembling My Heart Has Stop Beating Passionit Knees Shaking Lots A Goosebumps Was That The Ground Moving Passionate Wet Deep Lingering Kiss* ahhhh that should hold her for a minute :)) hehehe..My Place:{ say $$1 Why don't we go back to my place and do the things I'm going to tell people we did anyway }
  ..CyberChat:{ say $$1 Hmmmmm cyber?.....chat?.......cyber?.......chat?....... Feck it HOLD ON Hunny IM CUMING !!!!!!!!!!!!!!!! }
  ..Duck: /say 80.......8_0.......9Wanna play | /say 40.....4>8(12'8)_,0.....9with my | /say 80......8(_~_/0......9rubber ducky | /say 120...12~^~~^~^~^~^~0.....9 $$1??
  ..Coldshower: /me suggests a * * C * O * L * D * * * S * H * O * W * E * R * * for $$1 to take care of that little problem!
  ..Baby oil:/me breaks out the whips, chains, rubber mattress cover and baby oil...look out $$1 , I am feeling mean and kinky tonight. Evil Grin....
  ..Bodyshot:/me looks seductivly into $$1 's eyes, Grabs a bottle of peach schnaps, and starts doing body shots from $$1 's bellybutton
  ..Passby:/me passes by $$1 slyly turns his head and stops dead short, MY GOSH! Look at that Butt!!! I have never seen such firm muscles!
  ..HotMale:/me 12,1 SAYS 8,1  ***** EXTREME CAUTION***** 4,1 Extremely sexy man has entered the channel...12,1** $$1**  4,1 Men....hide your girlfriends...wives...and daughters!!!!! Women.....get in line behind me!!!!!
  ..Pesty Male:/say 12,1 SAYS 8,1 ******EXTREME CAUTION***** 12,1 Man with extreme inclinations to mate with any breathing...walking...(alive?) woman in the channel has now entered this room....4** $$1** ...9,1 WOMAN 12,1....pretend to be dead...9,1 MEN 12,1....give him a WORLD ROCKING WEDGIE!!!!!!
}
;End of menu popups
