;Events
;Help rawmodes written by Redneck
on ^1:rawmode:#: {
  haltdef
  Integrity
  if ($hGet(Integrity,Modes) != h) && ($hGet(General,HelpModes) == 1) {
    var %where.check $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) $iif($chan != $active,on $chan)
    var %modes = $1
    var %names = $2-
    var %temp = $right(%modes,$calc($len(%modes) - 1))
    if (($len(%temp) > 1) && ($len(%temp) == $numtok(%names,32))) {
      var %i = 1
      var %b = $left(%temp,1)
      while (%i < $len(%temp)) {
        inc %i
        if (%b != $mid(%temp,%i,1)) goto next
      }
      var %c = $replacexcs(%b,v,Voice,h,Half-Op,o,Op,a,Protected,q,Founder)
      var %names = $replace(%names,$chr(32),$chr(32) $+ $chr(44))
      if ($left(%modes,1) == $chr(43)) var %change sets
      if ($left(%modes,1) == $chr(45)) var %change removes
      echo %where.check %change mass %c on %names
      goto part2
    }
    :next
    var %i = 0
    var %cname = $null
    var %posmodes = $null
    var %negmodes = $null
    while (%i < $numtok(%names,32)) {
      inc %i
      var %name = $gettok(%names,%i,32)
      if (%name == 0) goto skip
      if (%name) {
        if ((%cname) && (%cname == %name)) {
          var %check = $getmode(%modes,%i)
          if ($left(%check,1) == $chr(43)) var %posmodes = %posmodes $+ $right(%check,1)
          if ($left(%check,1) == $chr(45)) var %negmodes = %negmodes $+ $right(%check,1)
          var %names = $reptok(%names,%name,0,1,32)
        }
        elseif (!%cname) {
          var %cname = %name
          var %check = $getmode(%modes,%i)
          if ($left(%check,1) == $chr(43)) var %posmodes = %posmodes $+ $right(%check,1)
          if ($left(%check,1) == $chr(45)) var %negmodes = %negmodes $+ $right(%check,1)
          var %names = $reptok(%names,%name,0,1,32)
        }
      }
      :skip
    }
    :part2
    if (%cname) {
      if (%posmodes) {
        if (k isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,k) 
          echo %where.check sets %cname as the channel key. 
        }
        if (b isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,b) 
          echo %where.check sets a ban for %cname 
        }
        if (e isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,e) 
          echo %where.check sets an exempt from ban flag for %cname 
        }
        if (I isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,I) 
          echo %where.check sets an exempt from invite flag for %cname 
        }
        if (f isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,f) 
          echo %where.check sets flood protection of %cname 
        }
        if (j isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,j) 
          echo %where.check sets join throttle to %cname 
        }
        if (l isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,l) 
          echo %where.check sets max users to %cname 
        }
        if (L isincs %posmodes) { 
          var %posmodes = $removecs(%posmodes,L) 
          echo %where.check sets %cname as secondary channel, when $chan is full. 
        }
        var %posmodes = $replacexcs(%posmodes,v,$chr(32) $+ Voice $+ $chr(44) $+ $chr(32),h,$chr(32) $+ Half-Op $+ $chr(44) $+ $chr(32),o,$chr(32) $+ Op $+ $chr(44) $+ $chr(32),a,$chr(32) $+ Protected $+ $chr(44) $+ $chr(32),q,$chr(32) $+ Founder $+ $chr(44) $+ $chr(32))
        var %posmodes = $left(%posmodes,$calc($len(%posmodes) - 1))
        echo %where.check sets %posmodes flag on %cname
        elseif ($numtok(%posmodes,32) > 1) {
          echo %where.check sets %posmodes flags on %cname
        }
      }
      if (%negmodes) {
        if (b isincs %negmodes) { 
          var %negmodes = $removecs(%posmodes,b) 
          echo %where.check removes the ban for %cname 
        }
        if (e isincs %negmodes) { 
          var %negmodes = $removecs(%posmodes,e) 
          echo %where.check removes exempt from ban flag for %cname 
        }
        if (I isincs %negmodes) { 
          var %negmodes = $removecs(%posmodes,I) 
          echo %where.check removes exempt from invite flag for %cname 
        }
        var %negmodes = $replacexcs(%negmodes,v,$chr(32) $+ Voice $+ $chr(44) $+ $chr(32),h,$chr(32) $+ Half-Op $+ $chr(44) $+ $chr(32),o,$chr(32) $+ Op $+ $chr(44) $+ $chr(32),a,$chr(32) $+ Protected $+ $chr(44) $+ $chr(32),q,$chr(32) $+ Founder $+ $chr(44) $+ $chr(32))
        var %negmodes = $left(%negmodes,$calc($len(%negmodes) - 1))
        if ($numtok(%negmodes,32) == 1)       {
          echo %where.check removes %negmodes flag on %cname
        }
        elseif ($len(%negmodes) > 1) {
          echo %where.check removes %negmodes flags on %cname
        }
      }
      goto next
    }
    else {
      var %modes = $removecs(%modes,q,a,o,h,v,b,e,I)
      var %change = $null
      var %i = 0
      var %z = 0
      while (%i < $len($1)) {
        inc %i
        var %a = $mid(%modes,%i,1)
        if (%a == $chr(43)) var %change = sets
        elseif (%a == $chr(45)) var %change = removes
        else {
          if (%a === c) echo %where.check %change mIRC color code block mode.
          if (%a === i)       echo %where.check %change invite only mode.
          if (%a === m)       echo %where.check %change moderated mode.
          if (%a === n)       echo %where.check %change no external messages mode.
          if (%a === p)       echo %where.check %change private mode.
          if (%a === r)       echo %where.check %change registered channel mode.
          if (%a === s)       echo %where.check %change secret mode.
          if (%a === t)       echo %where.check %change Only ops set topic mode.
          if (%a === z)       echo %where.check %change SSL user only mode.
          if (%a === A)       echo %where.check %change Server or Net Admin only mode.
          if (%a === C)       echo %where.check %change block channel CTCPs mode.
          if (%a === G)       echo %where.check %change bad word filter mode.
          if (%a === M)       echo %where.check %change only registered or voiced users can talk mode.
          if (%a === K)       echo %where.check %change Block /KNOCK mode.
          if (%a === N)       echo %where.check %change Block nickname changes mode.
          if (%a === O)       echo %where.check %change IRC Operator only mode.
          if (%a === Q)       echo %where.check %change Block kicks mode.
          if (%a === R)       echo %where.check %change registered users only mode.
          if (%a === S)       echo %where.check %change strip mIRC color codes mode.
          if (%a === T)       echo %where.check %change Block notices mode.
          if (%a === V)       echo %where.check %change block /INVITE mode.
          if (%a === u)       echo %where.check %change auditorium mode.
          if (%a === k) && (%change == removes)       echo %where.check removes the channel key.
          if (%a === f) && (%change == removes)       echo %where.check removes flood protection.
          if (%a === j) && (%change == removes)       echo %where.check removes join throttle protection.
          if (%a === l) && (%change == removes)       echo %where.check removes user limit.
          if (%a === L) && (%change == removes)       echo %where.check removes secondary channel link.
        }
      }
    }
    return
  }
  Integrity
}
alias -l getMode {
  var %change = $null
  var %i = 0
  var %z = 0
  while (%i < $len($1)) {
    inc %i
    var %a = $mid($1,%i,1)
    if (%a == $chr(43)) var %change = %a
    elseif (%a == $chr(45)) var %change = %a
    else {
      if (%a === v) || (%a === h) || (%a === o) || (%a === a) || (%a === q) || (%a === k) || (%a === b) || (%a === e) || (%a === I) || (%a === f) || (%a === j) || (%a === l) || (%a === L) {
        inc %z
        if (%z == $2) return %change $+ %a
      }
    }
  } 
}

Events

on *:ERROR:*: { 
  haltdef 
  echo -cstblfrm Other $Brackets1($1 $5-)
  if ($hGet(Sounds,General)) {
    if ($7 == kill)  $snd(Play,Q,Killed.WAV)
    elseif (ping isin $5) $snd(Play,Q,Pinged.WAV)
  }
}

on ^1:CONNECTFAIL: {
  haltef
  echo -cstblfrm Other $Brackets1(Failed) $1-
}

on 1:connect: { 
  echo -cstblfrm Other $Brackets1(connected $server)
}

on *:DISCONNECT: {
  ;  haltdef
  echo -cstblfrm Other $Brackets1(Disconnected from $1-)
  if ($hGet(Sounds,General)) $snd(Play,Q,Disconnected.WAV)
}

on 1:LOGON:*: {
  haltdef
  echo -cstblfrm Other Connecting to $server on the $network network.
}

on ^1:invite:#: { 
  haltdef
  hAdd General ChannelInvJoin $cid
  echo -catblrfm Inv * $nick $iif($hGet(Integrity,UAddress),$address) has invited you to $chan $iif($cid != $activecid,on $network) $Help(To accept push F3 or type /join -i) $iif($hGet(Sounds,General),$snd(Play,Q,Invite.WAV)) 
}

on ^1:join:#: { 
  haltdef
  if (($nick == $me) && ($hGet(General,ChannelStats))) {
    hAdd Channel $$+($network,_,$chan) $ticks
    echo -cblrfm Join $chan * Joined $chan $time(hh:nn dddd) $daytime($ctime)    
  }
  elseif ($hGet(Integrity,Joins) != h) {
    echo $iif($hGet(Integrity,Joins) == s,-cstblrfm Join,-ctblrfm Join $chan) * Joins $nick $iif($hGet(Integrity,UAddress),$address $iif($right($address,3) == .IP,$Help(masked IP)))) $iif($chan != $active,to $chan) 
    if ($network == bitlbee) && ($hGet(Sounds,BitlbeeJoinQuits)) {
      if (!$hGet(Speech,$network)) {
        if (!$hGet(Speech,$+($network,_,$chan))) $snd(Play,Q,BitlbeeJoin.wav)
      }
    }
    elseif ($hGet(Sounds,JoinParts)) && (!$hGet(Speech,$+($network,_,$chan))) $snd(Play,Q,Join.wav)
  }
  ;}
  Integrity
}

on ^1:part:#: { 
  haltdef
  if ($nick == $me) {
    echo -cblrfm Part $chan * Parted $chan $time(h:nn dddd) $daytime($ctime) $iif($1,$Brackets1($1-))
  }
  if (!$hGet(Speech,$+($network,_,$chan)) && ($hGet(Sounds,JoinParts))) $snd(Play,Q,Part.wav)
  if ($hGet(Integrity,Parts) != h) {
    echo $iif($hGet(Integrity,Parts) == s,-cstblrfm Part,-ctblrfm Part $chan) * Parts $nick $iif($hGet(Integrity,UAddress),$address $iif($right($address,3) == .IP,$Help(masked IP))) $iif($1,$Brackets1($1-)) $iif($chan != $active,from $chan) 
  }
  Integrity
}

on ^1:topic:#: { 
  haltdef  
  if ($hGet(Integrity,Topics) != h) {
    echo $iif($hGet(Integrity,Topics) == s,-cstblrfm Topic,-ctblrfm Topic $chan) $Brackets1($chan topic changed by $nick) $iif($hGet(Integrity,UAddress),$address $iif($right($address,3) == .IP,$Help(masked IP))) 
    echo $iif($hGet(Integrity,Topics) == s,-cstblrfm Topic,-ctblrfm Topic $chan) $Brackets1($iif(!$1,No topic has been set,$1-)) 
  }
  integrity
}

on ^1:kick:*: { 
  haltdef
  if ($kNick == $me) echo -cblrfm Kick $chan $timestamp * You were kicked from $chan by $nick $iif($hGet(Integrity,UAddress),$address) $iif($1,$Brackets1($1-)) $iif($hGet(Sounds,OpVoice),$snd(Play,Q,Kick.WAV)) 
  elseif ($hGet(Integrity,Kicks) != h) echo $iif($hGet(Integrity,Kicks) == s,-cstblrfm Kicks,-ctblrfm Kicks $chan) * $kNick was kicked from $chan by $nick $iif($hGet(Integrity,UAddress),$address) $iif($1,$Brackets1($1-))
  Integrity
}

on ^1:ban:#: {
  haltdef
  if (($bnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Q,Bann.WAV)
  if (!$hGet(General,HelpModes)) {
    echo -ctblfrm M $chan * $nick $iif($hGet(Integrity,UAddress),$address) banned $bnick $banmask
  }
  Integrity
}

on ^1:unban:#: {
  haltdef
  if (!$hGet(General,HelpModes)) {
    echo -ctblfrm M $chan * $nick $iif(($hGet(Integrity,UAddress)),$address) unbanned $bnick $banmask
  }
  Integrity

}

on ^1:usermode: {
  haltdef
  echo -cstblfrn m * You $iif($left($1,1) == -,unset,set) user mode $Brackets1($mid($1-,2 for $network))
  integrity  
}

on ^1:mode:#: {
  haltdef
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
    }
  }
  Integrity
}

on ^1:servermode:#: { 
  haltdef 
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      if (($opnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Q,Op.WAV)
    }
  }
  Integrity
}

on ^1:serverop:#: { 
  haltdef
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      if (($opnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Q,Op.WAV)
    }
  }
  Integrity
} 

on ^1:op:#: { 
  haltdef 
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      Integrity
      if (($opnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Q,Op.WAV)}
    }
  }
}

on ^1:deop:#: {
  haltdef 
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      Integrity
      if (($opnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Q,Deop.WAV)
    }
  }
}

on ^1:voice:#: { 
  haltdef 
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      Integrity
      if (($vnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,Voice.WAV)
    }
  }
}

on ^1:devoice:#: { 
  haltdef 
  if (!$hGet(General,HelpModes)) {
    if ($hGet(Integrity,Modes) != h) {
      echo $iif($hGet(Integrity,Modes) == s,-cstblrfm M,-ctblrfm M $chan) * $nick $iif(($hGet(Integrity,UAddress)),$address) set mode: $1- $iif($chan != $active,on $chan) 
      Integrity
      if (($vnick == $me) && ($hGet(Sounds,OpVoice))) $snd(Play,DeVoice.WAV)
    }
  }
}

on 1:serve:*: echo -catblrfn No * [DCC] File serve $nick $+ : $1-

on 1:DCCSERVER:Fserve: echo -catblrfn Other [DCC] $nick wants to connect to your file server

on 1:DCCSERVER:Send: echo -catblrfm Not * [DCC] $nick Wants to dcc you the file $1-

on 1:FILESENT:*: echo -cstblrfm notify * [DCC] $nopath($filename) finished sending to $nick  $iif($hGet(Sounds,General),$snd(Play,Q,DataLink.WAV))

on ^1:FILERcvd:*: { 
  haltdef 
  echo -cstblrfm notify * [DCC] Finished getting $nopath($filename) from $nick $ii($hGet(Sounds,General),$snd(Play,Q,FileComplete.WAV))
}

on ^1:sendfail:*: { 
  haltdef 
  echo -cstblrfm notify * [DCC] Failed to send $nopath($filename) to $nick
}

on 1:getfail:*: echo -cstblfrm notify * [DCC] Failed to get $nopath($filename) from $nick $+ , $7-

on 1:NOSOUND: {
  haltdef
  echo -csblfrn Not $timestamp * $1-
}

on ^1:notify: { 
  haltdef
  echo -csblfrn Not $timestamp * [NOTIFY] $nick is on $network 
}

on ^1:unotify: { 
  haltdef  
  echo -csblfrn Not $timestamp * [NOTIFY] $nick left $network
}

on ^1:wallops:*: { 
  haltdef  
  echo -cstblfrm wallops * Wallops from $nick $+ : $1- 
}

on 1:DCCSERVER:Chat: echo -catblfrm notify & [DCC] $nick wants to dcc chat with you

on 1:dns:{ 
  echo -cstblfrn info * Found $dns(0) addresses for $address
  var %n = $dns(0)
  while (%n > 0) {
    echo -cstblfrn info * %text $+ $dns(%n).addr  resolves to $dns(%n).ip
    dec %n 
  } 
  halt  
}

on ^1:text:*:*: {
  haltdef
  if (!$chan) { 
    /!echo -ctblrfmq Norm $nick $Brackets2($nick) $1- 
    if (($nick != $active) && ($hGet(General,PrivateMessageAlert))) echo -catblfm Highlight $Brackets1($nick $iif($activecid != $cid,on $network) messaged you in private)
    goto talk
  } 
  var %prefix $left($nick($chan,$nick).pnick,1)
  /!echo -ctblrfmq Norm $chan $+($chr(03),$nick($chan,$nick).color,$Brackets2($+($iif(%prefix isin $prefix,%prefix),$nick)),$chr(15))) $1-
  if ((($network == bitlbee) && ($hGet(Sounds,General)) && (yes/no commands  isin $1-) || (wants to add you to his/her buddy list. isin $1-))) $snd(Play,Q,globalMessage.wav)
  goto talk
  :talk
  Talk $iif(%prefix isin $prefix,%prefix) $nick $iif($activecid != $cid,on $network) $iif($iif(#,#,$nick) != $active || $activecid != $cid,$iif(#,on $mid(#,2),in private)) says $lower($1-)
}

on ^1:action:*:*: {
  haltdef
  if (!$chan) { 
    /!echo -ctblrfmq Act $iif($chan,$chan,$nick) $Brackets3($nick) $1- 
    if (($hGet(General,PrivateMessageAlert)) && ($nick != $active))echo -catblfm Highlight $Brackets1($nick $iif($activecid != $cid,on $network) messaged you in private)
    goto talk
  } 
  var %prefix $left($nick($chan,$nick).pnick,1),
  /!echo -ctblrfmq Act $chan $+($chr(03),$nick($chan,$nick).color,$Brackets3($iif(%prefix isin $prefix,%prefix)),$nick,$chr(15))) $1-
  goto talk 
  :talk 
  Talk $iif(%prefix isin $prefix,%prefix) $nick $iif($activecid != $cid,on $network) $iif($iif(#,#,$nick) != $active || $activecid != $cid,$iif(#,on $mid(#,2),in private)) $lower($1-)
}

on 1:start: {
  if (!$readini(mirc.ini,mirc,nick)) .nick $$?="No nickname is set. $crlf $+ Plese type your nick you want:" 
  MWSFunctions
  Max
}

on 1:exit: Integrity

on ^1:quit: { 
  haltdef
  ;Bitlbee quit sound
  if ($network == bitlbee) && ($hGet(Sounds,BitlbeeJoinQuits)) {
    if (!$hGet(Speech,$network)) {
      if (!$hGet(Speech,$+($network,_,$chan))) $snd(Play,Q,BitlbeeQuit.wav)
    }
  }
  ;Netsplit sound
  if (. isin $1) && (. isin $2) && ($3 == $null) && ($hGet(General,Netsplit)) { 
    if (!$network == bitlbee) echo -casblrfm Quit * $timestamp [NETSPLIT] $2 split from $1 $iif($hGet(Sounds,General),$snd(Play,NetSplit.WAV))
    hAdd -mu60 General Netsplit 1
  } 
  ;Get channels
  var %i = 0 
  while (%i < $comchan($nick,0)) { 
    inc %i 
    ;Print quits
    if ($hGet(Integrity,Quits) != h) {
      if (!$hGet(Speech,$network)) {
        if (!$hGet(Speech,$+($network,_,$chan))) echo $iif($hGet(Integrity,quits) == s,-cstblrfm Quit ,-ctblrfm Quit $comchan($nick,%i)) * $Quits($1-) $nick $iif(($hGet(Integrity,UAddress)),$address $iif($right($address,3) == .IP,$Help(masked IP)))) $iif($Quits($1-) == Quits,$1-)
      }
    }
  }
  Integrity
}

on ^1:notice:*:*: {
  haltdef
  var %t1 = $left($target,1), %t2 = $mid($target,2,1), %c = $chantypes
  if (%t1 isin %c) || (%t2 isin %c) {
    if (%t2 isin %c) echo -ctblrfm Notice $mid($target,2) - $+ $replacexcs(%t1,~,Founders,&,Protected Ops,@,Ops,%,Half-Ops,+,Voiced) Notice- $$1-
    else echo -ctblrfm Notice $chan - $+ $chan Notice- $$1-
  }
  else echo $iif($activecid == $cid,-catblrfm,-cstblrfm) Notice - $+ $nick Notice- $$1-
  if ((($nick == Global) && ($hGet(Sounds,General)) && ($hGet(Sounds,Global)))) $snd(Play,Q,GlobalMessage.WAV)
  if ($hGet(Sounds,ServicesSounds)) {
    if (($nick == memoserv) && ($4 == new) || ($5 == new)) $snd(Play,Q,Memo.WAV)
    elseif (($nick == NickServ) && (This nick is owned by someone else. Please choose another one. isin $1-) || (This nickname is registered isin $1-)) $snd(Play,Q,InputPassword.WAV)
    elseif ($nick == chanserv) || ($nick == nickserv) {
      if ($1 == password) {
        if ($2 == accepted) $snd(Play,Q,PasswordAccepted.WAV)
        elseif ($2 == incorrect.) $snd(Play,Q,IncorrectPassword.WAV)
      }     
      elseif (are now identified for isin $1-) $snd(Play,Q,PasswordAccepted.WAV) 
    } 
  } 
  elseif (($1 == [knock]) && ($hGet(Sounds,General))) $snd(Play,Q,Knock.WAV)
} 

on ^1:snotice:*: {
  haltdef
  if ($6 == problems) halt 
  if ((($4 == configuration) && ($6 == without) && ($hGet(Sounds,General)))) $snd(Play,Q,RehashComplete.WAV)
  if (($2 == Global) && ($3 != z:line)) {    
    if (($hGet(Sounds,General)) && ($hGet(Sounds,Global))) $snd(Play,GlobalMessage.WAV)
    if ($dialog(OperView)) did -a OperView 51 $tspanel Global $4-
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1)) echo -cstblfm Wallops Global: $4-
    halt 
  }
  if (($5 == connecting) && ($6 == on)) {
    if ($hGet(ServerNotices,$remove($gettok($10,2,64),$chr(41))) == Ignored) halt 
    elseif ($dialog(OperView)) did -a OperView 11  $tspanel Connecting $9 Ident $remove($gettok($10,1,64),$chr(40)) Address $remove($gettok($10,2,64),$chr(41)) $iif(Secure isin $12, SSL-Port $left($8, $calc($len($8) - 1))) Server $server $+($chr(40),$network,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Join Connecting $9 Ident $remove($gettok($10,1,64),$chr(40)) Address $remove($gettok($10,2,64),$chr(41)) $iif(Secure isin $12, SSL-Port $left($8, $calc($len($8) - 1))) Server $server 
    halt 
  }
  if (($5 == connecting) && ($6 == at)) {
    if ($hGet(ServerNotices,$remove($gettok($9,2,64),$chr(41)))  == Ignored) halt
    if ($dialog(OperView)) did -a OperView 11 $tspanel Connecting $8 Ident $remove($gettok($9,1,64),$chr(40)) Address $remove($gettok($9,2,64),$chr(41)) $iif(Secure isin $10, SSL-Port $left($8, $calc($len($8) - 1))) Server $mid($7,1,$calc($len($7) - 1)) $+($chr(40),$network,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Join Connecting $8 Ident $remove($gettok($9,1,64),$chr(40)) Address $remove($gettok($9,2,64),$chr(41)) $iif(Secure isin $10, SSL-Port $left($8, $calc($len($8) - 1))) Server $mid($7,1,$calc($len($7) - 1))
    halt 
  }  
  if ($5 == exiting:) {
    if ($hGet(ServerNotices,$remove($gettok($7,2,64),$chr(41))) == Ignored) halt
    if ($dialog(OperView)) did -a OperView 11 $tspanel Exiting $6 Ident $remove($gettok($7,1,64),$chr(40)) Address $remove($gettok($7,2,64),$chr(41)) Server $server $+($chr(91),$network,$chr(93)) Reason $8-
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Quit Exiting $6 Ident $remove($gettok($7,1,64),$chr(40)) Address $remove($gettok($7,2,64),$chr(41)) Server $server Reason $8-
    halt 
  }
  if ($5 == exiting) && ($6 == at) {
    if ($hGet(ServerNotices,$remove($gettok($8,2,64),$chr(41))) == Ignored) halt 
    if ($dialog(OperView)) did -a OperView 11 $tspanel Exiting $gettok($8,1,33) Ident $gettok($gettok($8,1,64),2,33) Address $remove($gettok($8,2,64),$chr(41)) Server $mid($7,1,$calc($len($7) - 1)) $+($chr(91),$network,$chr(93)) Reason $9-
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Quit Exiting $gettok($8,1,33) Ident $gettok($gettok($8,1,64),2,33) Address $remove($gettok($8,2,64),$chr(41)) Server $mid($7,1,$calc($len($7) - 1)) Reason $9-
    halt 
  }
  if ($10 == operator) {
    if ($dialog(OperView)) did -a OperView 61 $tspanel OPER $4 Rank $left($9-,$calc($len($9-) - 4)) Server $server $+($chr(40),$network,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Notify OPER $4 Rank $left($9-,$calc($len($9-) - 4)) Server $server 
    halt 
  }
  if ((($left($3,1) == [) && ($right($3,1) == ]) && ($4 == is))) { 
    if ($dialog(OperView)) did -a OperView 61 $tspanel OPER: $1 Rank $left($7-,$calc($len($7-) - 4)) OperUser $remove($3,$chr(91),$chr(93)) $+($chr(40),$network,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Notify OPER: $1 Rank $left($7-,$calc($len($7-) - 4)) Oper user $remove($3,$chr(91),$chr(93))
    halt 
  }
  if ($4 == Failed) {
    if ($dialog(OperView)) did -a OperView 61 $tspanel $gettok($4-,1,40) Reason $10- Server $server
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm hi $gettok($4-,1,40) Reason $10- Server $server
    halt 
  }
  if ($6 == /whois) {
    if ($dialog(OperView)) did -a OperView 31 $tspanel Whois by $2 Ident $remove($gettok($3,1,64),$chr(40)) Address $remove($gettok($3,2,64),$chr(41)) $+($chr(40),$network,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm WH Whois by $2 Ident $remove($gettok($3,1,64),$chr(40)) Address $remove($gettok($3,2,64),$chr(41))
    halt 
  }
  if ($1 == Temporary) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $1-2 added for $6-7 From $9 Reason $remove($10-,$chr(91),$chr(93))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $1-2 added for $6-7 From $9 Reason $remove($10-,$chr(91),$chr(93))
    halt 
  }
  if ($1 == Removed) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $1-3 from $6-7 Set By $9
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $1-3 from $6-7 Set By $9 
    halt 
  }
  if ($3 == added) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2 added for $5 Expires $gettok($18-,1,58) Set $gettok($7-,1,40) From $gettok($14,1,33) Reason $remove($24-,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $2 added for $5 Expires $gettok($18-,1,58) Set $gettok($7-,1,40) From $gettok($14,1,33) Reason $remove($24-,$chr(41))
    halt 
  } 
  if ($2 == Global) && ($4 == added) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2-3 added for $6 Expires $gettok($19-,1,58) Set $gettok($8-,1,40) From $gettok($15,1,33) Reason $remove($25-,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $2-3 added for $6 Expires $gettok($19-,1,58) Set $gettok($8-,1,40) From $gettok($15,1,33) Reason $remove($25-,$chr(41))
    halt 
  }
  if (($2 == Permanent) && ($4 == added)) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2 $3 added for $6 Set $gettok($8-,1,40) From $gettok($15,1,33) Reason $remove($16-,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $2 $3 added for $6 Set $gettok($8-,1,40) From $gettok($15,1,33) Reason $remove($16-,$chr(41))
    halt 
  }
  if ($2 == Expiring) && ($3 != Global) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2 $3 for $mid($4,2,$calc($len($4) - 2)) Set $gettok($10-,2,41) By $gettok($7,1,33) Reason $gettok($9-,1,41)
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo)) ) echo -cstblfm action $2 $3 for $mid($4,2,$calc($len($4) - 2)) Set $gettok($10-,2,41) By $gettok($7,1,33) Reason $gettok($9-,1,41)
    halt 
  }
  if ($2 == Expiring) && ($3 == Global) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2-4 for $mid($5,2,$calc($len($5) - 2)) Set $gettok($11-,2,41) By $gettok($8,1,33) Reason $gettok($10-,1,41)
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo)) ) echo -cstblfm action $2 $+ $3-4 for $mid($5,2,$calc($len($5) - 2)) Set $gettok($11-,2,41) By $gettok($8,1,33) Reason $gettok($10-,1,41)
    halt 
  }
  if ($2 == removed) && ($3 == Global) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $2-$4 for $5 By $gettok($1,1,33) Set $gettok($6-,1,41)
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $3 $4 removed for $5 By $gettok($1,1,33) Set $gettok($6-,1,41)
    halt 
  } 
  if ($2 == removed) {
    if ($dialog(OperView)) did -a OperView 41 $tspanel $3 $2 for $4 By $gettok($1,1,33) Set $gettok($7-,1,45) Reason $remove($14-,$chr(41))
    if ((!$dialog(OperView)) || ($hGet(General,ov.echo) == 1) ) echo -cstblfm Action $3 removed for $4 By: $gettok($1,1,33) Set: $gettok($7-,1,45) Reason: $remove($14-,$chr(41))
    halt 
  }
  if ($4 == forced) {
    echo -casblfm Other $tspanel Sajoin alert $iif($cid != $activecid,on $network)
    if ($7 == 0) .join [ % $+ [ channels. $+ [ $server ] ] ]
    elseif ($6 == join) .part $7 Automated sajoin protection, /invite me and a sound plays.  
    elseif ($6 == part) .timer 1 4 .join $7
  }
  if ($1 == Lag) {
    echo -cstblfm Info * Lag between $4 and $5 $calc(($ctime - $6) / 10) seconds
    halt 
  }
  if ($2 == looking) && ($hGet(Sounds,General)) $snd(Play,Q,Connecting.WAV)
  echo -cstblfm info $remove($1-,$chr(42)) 
}

on 1:open:*: {
  if ($hGet(Sounds,Query)) $snd(Play,Q,Open.WAV)
}

on 1:close:?: {
  if ($hGet(Sounds,Query)) $snd(Play,Q,Close.WAV)
}

ctcp 1:*:*: {
  if ($1 ==   PING) return
  elseif ($1 ==   FINGER) return
  elseif (($1 ==   AWAY) &&  ($away)) .ctcpreply $nick AWAY Reason: $hGet(General,Away) Duration: $duration($calc($ctime - $hGet(General,Away.time)))
  elseif (($1 == DCC) && ($2 == Send)) echo -catblrfm ctcp $Brackets1(Attempting data transfer)
  elseif ($1 == CLIENTINFO) {
    .ctcpreply $nick CLIENTINFO AWAY - Away message and duration
    .ctcpreply $nick CLIENTINFO CLIENTINFO - this response
    .ctcpreply $nick CLIENTINFO  SPEECH - check if user has speech disabled/enabled
    .ctcpreply $nick CLIENTINFO  TIME - fetch users time and GMT
    .ctcpreply $nick CLIENTINFO  UPTIME - system uptime
    .ctcpreply $nick CLIENTINFO  VERSION - mIRC with speech version
    .ctcpreply $nick CLIENTINFO  FINGER email and idle if provided
    .ctcpreply $nick CLIENTINFO PAGE - send a page
    .ctcpreply $nick CLIENTINFO  PING - check if client is alive
    .ctcpreply $nick CLIENTINFO  GI - [obsolete feature ]
  }
  elseif ($1 == GI)  .ctcpreply $nick GI Obsolete feature $Help(Use /Speech $2 SPEECH instead.)
  elseif ($1 == SPEECH) {
    var %SpeechStatus $hGet(Speech,Speech))
    if (%SpeechStatus) .ctcpreply $nick SPEECH Y Speech Enabled
    else {
      if ($hGet(Speech,NoSpeech)) .ctcpreply $nick SPEECH X $nick Does not have Speech
      else .ctcpreply $nick SPEECH N Speech Disabled
    }
  }
  elseif ($1 == TIME) .ctcpReply $nick TIME $asctime(h:nn dddd) $daytime($ctime) $asctime(mmmm doo) GMT $gmt(z)
  elseif ($1 == UPTIME) .ctcpreply $nick UPTIME System up $uptime(system,2)
  if ($uptime(server,3) > 15)   {   
    echo     -catblrfm   ctcp $Brackets1($nick $iif($activecid != $cid,on $network) requested your $iif(%SpeechStatus,Speech Status,clients $1) at $timestamp)      |      if ($1 == VERSION) .CTCPReply $nick VERSION $mws 
  }
  haltdef
}

on 1:ctcpReply:*: {
  var %echo echo $iif($hGet(Integrity,CTCPs) == s,-cstblrfm CTCP,-ctblrfm CTCP $chan) 
  if ($1 == ping) %echo $Brackets1($nick PING reply) $duration($calc($ctime - $2)) 
  elseif ($1 == SPEECH) { 
    if ($2 == Y) %echo $Brackets1($nick has speech enabled)
    elseif ($2 == N) %echo $Brackets1($nick has speech disabled)
    elseif ($2 == X) %echo $Brackets1($nick does not have speech)
  }
  elseif (($1 == GI) && ($2-3 == Obsolete feature ) && ($hGet(General,Help))) Speech $nick
  elseif ($1 == TIME) {
    var %tz1 = $gmt(z),%tz2 = $9,%tdiff = %tz1 - %tz2 
    %echo $Brackets1(CTCP $1 reply from $nick) $2- $iif($8 == GMT,$Help($remove(%tdiff,-) hours $iif($left(%tdiff,1) = -,behind,ahead) of $nick))
  }
  else  %echo $Brackets1(CTCP $1 reply from $nick) $2- 
  haltdef    
}
on 1:chat:*: Talk $nick $iif($nick != $mid($active,2),in DCC chat) says $1-

on ^1:nick: {
  haltdef
  if ($nick == $me) echo -catblrfm Nick * You changed nick from $nick to $newnick $iif($cid != $activecid,on $network) 
  elseif ($hGet(Integrity,Nicks) != h) {
    var %i = 0 
    while (%i < $comchan($newnick,0)) { 
      inc %i 
      echo -ctblrfm Nick $comchan($newnick,%i) * $nick changed nick to $newnick $iif(!$activecid,talk on $network) 
    }
  }
  Integrity 
}

;Talker, Caps lock check, coloured text
on 1:input:*:{
  ;Check if a command is being input
  if (($left($1,1) == $chr(47)) && ($1 != /me)) return
  haltdef
  if ($active == Status Window) {
    echo -cstblfm Info $Brackets1(Can't send text to a Status Window)
    halt
  }

  ; Check for all caps
  if ($isupper($1-) == $true) && ($islower($1-) == $false) { echo -catblfn Info $Brackets1(WARNING! Is caps lock turned on?) }

  ;Talker
  var %talker $1-
  if ($1 == /me) var %talker $2-
  if ($hGet(General,Talker)) { 
    ;;      while ($istokcs(%talker,%talker,32))       var %talker $reptok(%talker,%talker,$ul($hget(talker,%talker)),32) 
    while ($istokcs(%talker,lol,32))       var %talker $reptok(%talker,lol,$ul(laughing out loud),32) 
    while ($istokcs(%talker,omg,32)) var %talker $reptokcs(%talker,omg,$ul(oh my god),32) 
    while ($istokcs(%talker,omfg,32)) var %talker $reptokcs(%talker,omfg,$ul(oh my fucking god),32)
    while ($istokcs(%talker,ofc,32)) var %talker $reptokcs(%talker,ofc,$ul(of course),32)
    while ($istok(%talker,lmao,32)) var %talker $reptok(%talker,lmao,$ul(laughing my ass off),32)
    while ($istok(%talker,lmfao,32)) var %talker $reptok(%talker,lmfao,$ul(laughing my fucking ass off),32)
    while ($istok(%talker,afk,32)) var %talker $reptok(%talker,afk,$ul(away from keyboard),32)
    while ($istok(%talker,nfi,32)) var %talker $reptok(%talker,nfi,$ul(no fucking idea),32)
    while ($istok(%talker,asap,32)) var %talker $reptok(%talker,asap,$ul(as soon as possible),32)
    while ($istok(%talker,btw,32)) var %talker $reptok(%talker,btw,$ul(by the way),32)
    while ($istok(%talker,idk,32)) var %talker $reptok(%talker,idk,$ul(I don't know),32)
    while ($istok(%talker,np,32)) var %talker $reptok(%talker,np,$ul(no problem),32)
    while ($istok(%talker,ty,32)) var %talker $reptok(%talker,ty,$ul(thank you),32)
    while ($istokcs(%talker,afaik,32)) var %talker $reptokcs(%talker,afaik,$ul(as far as I know),32) 
    while ($istok(%talker,yw,32)) var %talker $reptok(%talker,yw,$ul(you're welcome),32)
    while ($istok(%talker,nvr,32)) var %talker $reptok(%talker,nvr,$ul(never),32)
    while ($istok(%talker,thx,32)) var %talker $reptok(%talker,thx,$ul(thanks),32)
    while ($istok(%talker,tbh,32)) var %talker $reptok(%talker,tbh,$ul(to be honest),32)
    while ($istok(%talker,nvm,32)) var %talker $reptok(%talker,nvm,$ul(never mind),32)
    while ($istok(%talker,tc,32)) var %talker $reptok(%talker,tc,$ul(take care),32)
    while ($istok(%talker,wtf,32)) var %talker $reptok(%talker,wtf,$ul(what the fuck),32)
    while ($istok(%talker,hb,32)) var %talker $reptok(%talker,hb,$ul(hurry back),32)
    while ($istok(%talker,gm,32)) var %talker $reptok(%talker,gm,$ul(good morning),32)
    while ($istok(%talker,dmi,32)) var %talker $reptok(%talker,dmi,$ul(don't mention it),32)
    while ($istok(%talker,motd,32)) var %talker $reptok(%talker,motd,$ul(message of the day),32)
    while ($istok(%talker,rotfl,32)) var %talker $reptok(%talker,rotfl,$ul(rolling on the floor laughing),32)
    while ($istok(%talker,mws,32)) var %talker $reptok(%talker,mws,$ul(mIRC With Speech),32)
    while ($istok(%talker,mwos,32)) var %talker $reptok(%talker,mwos,$ul(mIRC Without Speech),32)
    while ($istok(%talker,gn,32)) var %talker $reptok(%talker,gn,$ul(goodnight),32)
    while ($istok(%talker,brb,32)) var %talker $reptok(%talker,brb,$ul(be right back),32)
    while ($istok(%talker,bbs,32)) var %talker $reptok(%talker,bbs,$ul(be back soon),32)
    while ($istok(%talker,bbl,32)) var %talker $reptok(%talker,bbl,$ul(be back later),32)
    while ($istok(%talker,wb,32)) var %talker $reptok(%talker,wb,$ul(welcome back),32)
    while ($istok(%talker,dyk,32)) var %talker $reptok(%talker,dyk,$ul(did you know),32)
    while ($istok(%talker,stfu,32)) var %talker $reptok(%talker,stfu,$ul(shut the fuck up),32)
    while ($istok(%talker,fyi,32)) var %talker $reptok(%talker,fyi,$ul(for your information),32)
    while ($istok(%talker,fiik,32)) var %talker $reptok(%talker,fiik,$ul(fucked if i know),32)
    while ($istok(%talker,irl,32)) var %talker $reptok(%talker,irl,$ul(in real life),32)
    while ($istok(%talker,ttfn,32)) var %talker $reptok(%talker,ttfn,$ul(ta ta for now),32)
    ;End of talker
  }
  if ($hGet(Integrity,ModePrefix)) var %prefix $left($nick($chan,$nick).pnick,1)
  if (%talker) {
    ; Send to server
    $iif($1 == /me,.describe,.msg) $target %talker
    ; Show in window
    if ($1 == /me) { 
      /!echo -ctmnq Own $chan $+($chr(03),$nick($chan,$me).color,$iif(%prefix isin $prefix,%prefix),$me)) $+ $chr(15) %talker
    }
    else {
      /!echo -ctmnq Own $chan $+($chr(03),$nick($chan,$me).color,$Brackets2($+($iif(%prefix isin $prefix,%prefix),$me)))) %talker
    }
    ; Send to speech
    if ($hGet(Speech,InputText)) Talk $iif(%prefix isin $prefix,%prefix) $me $iif($1 != /me,says) $lower(%talker)
  }
}


;nick highlight 
on 1:text:$(* $+ $me $+ *):#: {
  if ($hGet(General,NickHighlight)) {
    if ($nick ison $active) halt
    if ($chan != $active) { 
      if (%highlight.nick != $nick) {
        var %mt = $matchtok($1-,$me,0,32)
        while (%mt > 0) {
          var %match = $remove($matchtok($1-,$me,%mt,32),$me)
          var %cnt = $len(%match)
          var %valid = $true
          while (%cnt > 0) {
            if ($mid(%match,%cnt,1) isalnum) var %valid = $false
            dec %cnt
          }
          if (%valid) { 
            echo -cablfm Hi $timestamp * $nick mentioned your name in $+($chan,$chr(46)) $iif($activecid != $cid,on $network)     $iif($hGet(General,HighlightMessage),Message: $1-) $iif($hGet(Sounds,General),$snd(Play,Q,NickHighlight.WAV))
            var %mt = 1 
          } 
          dec %mt
        }
      } 
      ;Store the nickname for 30 seconds in case of highlight flood
      set -eu30 %highlight.nick $nick
    }
  }
}

on 1:tabcomp:*: { 
  if (($hGet(Speech,NoSpeech)) || (!$hGet(Speech,Software))) return
  var (%tabcomp = $editbox($target) 
  var %tabcnt = 0
  while (%tabcnt < $numtok($1-,32)) { 
    inc %tabCnt 
    var %tabcomp2 = $gettok(%tabcomp,%tabcnt,32)
    if ($gettok($1-,%tabcnt,32) != %tabcomp2) { 
      if ($istok($chr(36) $chr(37),$left(%tabcomp2,1),32)) Talk $eval(%tabcomp2,2)
      else Talk $gettok($1-,%tabcnt,32) 
      return 
    }  
  } 
}

;
;end of events
