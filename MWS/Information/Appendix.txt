mIRC With Speech appendix
This is a working document to act as a reference guide for MWS commands, features and any information that probably doesnt currently have a home.

Sounds
MWS currently supports these event Sounds
If you intend on making your own sound pack to distribute please make sure your files use these event names and retain the case sensativity.
Case sensativity makes it easier for speech users reading filenames.

FileName	Description
acknowledged	Successfully opered
bann	You were banned from a channel
BitlbeeJoin	Someone joined Bitlbee
BitlbeeQuit	Someone left Bitlbee
close	You closed a private query
Connecting	Connecting to server
DataLink	Establishing DCC connection
denied You failed to oper
Deop You were de-opped in a channel
devoice You were de-voiced in a channel
Disconnected	You disconnected from a server
exit Close mIRC
FileComplete DCC file complete
globalMessage Global message
IncorrectPassword Incorrect NickServ or ChanServ password
InputPassword Please identify
Intro.mp3 Startup MWS sound *** must be an mp3 and resides in main soundpack directory
invite You were invited to join a channel
IRCOpDetected You Whoisd an IRCop
join Someone joined the channel
kick You were kicked from a channel
Killed	You were killed from a server
knock Someone wanting to be invited knocked on a channel with a key set
list Channel list complete
Locked Need a password to join that channel
memo You recieved a memo
menu Openned MWS menu
netsplit Network experienced a split between servers
NickHighlight	Someone mentioned your name
Op You were opped in a channel
open Someone openned a private query with you
part Someone parted a channel
PasswordAccepted	correct NickServ or ChanServ password
PermissionDenied	You dont have access for that command
Pinged	You ping timed out from a server
ready	Ready
RehashComplete Server configuration rehashed successfully
SSL You Whoisd someone using an SSL connection to the network
Update New updates available for MWS
voice You were voiced in a channel
