;Tools.mrc

;Server notices in status window, Talker text replacements, Caps lock warning
;Change guest nick to alternate nick, Current file transfers in DCC, Server lag report, nick highlight
;General sounds, Channel join/part sounds, Memo sender, Server status report
;Global who dialog, Switch tool, Server list tool
;Server notices and full dialog for multiple servers written by Redneck
;Service notice panel dialog
dialog OperView {
  title "Server notices panel"
  size -1 -1 510 310
  option dbu
  tab "Clients", 1, 5 5 500 290
  list 11, 10 20 490 275, tab 1, extsel, vsbar, hsbar
  tab "Kills", 2
  list 21, 10 20 490 275, tab 2, extsel, vsbar, hsbar
  tab "Whois", 3
  list 31, 10 20 490 275, tab 3, extsel, vsbar, hsbar
  tab "xLines", 4
  list 41, 10 20 490 275, tab 4, extsel, vsbar, hsbar
  tab "Global", 5
  list 51, 10 20 490 275, tab 5, extsel, vsbar, hsbar
  tab "Oper View", 6
  list 61, 10 20 490 275, tab 6, extsel, vsbar, hsbar
  check "Echo in Status?", 99, 135 298 50 10
  button "Clear", 100, 190 298 35 10
  button "Save", 101, 230 298 35 10
  button "Close", 102, 270 298 35 10, default ok
}
on 1:dialog:OperView:sclick:*: {
  if ($did(OperView,11).visible == $true) var %alist 11
  elseif ($did(OperView,21).visible == $true) var %alist 21
  elseif ($did(OperView,31).visible == $true) var %alist 31
  elseif ($did(OperView,41).visible == $true) var %alist 41
  elseif ($did(OperView,51).visible == $true) var %alist 51
  elseif ($did(OperView,61).visible == $true) var %alist 61
  else { 
    halt 
  }
  if ($did == 100) did -r $dname %alist
  if ($did == 101) {
    var %x = 0
    while (%x < $did($dname,%alist).lines) {
      inc %x
      write $+(Logs\,OV,$did($dname,$calc((%alist - 1)/10)).text,.,$asctime($ctime,yyyymmdd),.log) $did($dname,%alist,%x)
    }
  }
  if ($did == 99) {
    if ($hGet(General,ov.echo) != 1) hAdd -m General ov.echo 1
    else .hAdd -m General ov.echo 0
  }
}
on *:dialog:OperView:init:0: {
  if ($hGet(General,ov.echo) == 1)  { Did -c OperView 99 }
}
alias ul return $+($chr(31),$1-,$chr(31)) 
;Server lag report
;titlebar lag
on ^1:ping: {
  ;  if ($1 == $server) set -eu10 %titlelag $ticks
}
on ^1:pong: {
  haltdef
  if (($1 == $server) && (%lag)) {
    if (%publag) {
      msg %publag � $server lag: $iif($calc(($ticks - %lag) / 1000) < 1,$calc($ticks - %lag) Milliseconds,$calc(($ticks - %lag)/1000) Seconds) 
      halt 
    }
    echo -catblfn info2 * $server lag: $iif($calc(($ticks - %lag) / 1000) < 1,$calc($ticks - %lag) Milliseconds,$calc(($ticks - %lag)/1000) Seconds) 
    halt 
  }
}
alias memosender {
dialog -m memo memo }
dialog memo {
  title "Memo sender by MCJedi"
  size -1 -1 350 180
  text "Nickname(s)", 01, 10 20 70 14
  text "Message", 02, 10 40 70 14
  edit "", 03,80 19 250 20
  edit "", 04,80 39 250 80, multi, vsbar, limit 400
  button "SEND!",05,80 125 80 22
  button "CLEAR",06,165 125 80 22
  button "CLOSE",07,250 125 80 22,cancel
  edit "", 08,80 150 250 20, read
  edit "", 09,10 99 70 20, read
  text "Character left", 10, 10 80 70 14
}
on *:dialog:memo:init:0:{
  did -o memo 08 1 Welcome to Memo sender by MCJedi
  did -o memo 09 1 400
}
on *:dialog:memo:sclick:05:{
  .memoserv send $did(memo,03) $memos
  did -o memo 08 1 Memo sent!
}
on *:dialog:memo:sclick:06:{
  did -r memo 03,04
  did -o memo 08 1 New message.
}
on *:dialog:memo:edit:03:{
  if ($chr(32) isin $did(memo,03)) {
    set -u1 %memowarn Please do not input spaces at the nickname(s) field, separate multiple nickname by comma(,).
    dialog -m memowarn memowarn
  } 
}
alias memos {
  var %memo = $did(memo,04,1) 
  if ($did(memo,04).lines == 1) { return %memo 
  }
  var %memo = %memo $+ $did(memo,04,2) 
  if ($did(memo,04).lines == 2) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,3) 
  if ($did(memo,04).lines == 3) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,4) 
  if ($did(memo,04).lines == 4) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,5) 
  if ($did(memo,04).lines == 5) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,6) 
  if ($did(memo,04).lines == 6) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,7) 
  if ($did(memo,04).lines == 7) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,8) 
  if ($did(memo,04).lines == 8) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,9) 
  if ($did(memo,04).lines == 9) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,10) 
  if ($did(memo,04).lines == 10) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,11) 
  if ($did(memo,04).lines == 11) { 
    return %memo 
  }
  var %memo = %memo $+ $did(memo,04,12) 
  if ($did(memo,04).lines == 12) { 
    return %memo 
  }
}
on *:dialog:memo:edit:04:{
  did -o memo 09 1 $calc(400 - $len($memos))
  if ($len($memos) > 400) {
    set -u1 %memowarn Max length of Memo for DALnut MemoServ are limitted at 400 characters only.
    dialog -m memowarn memowarn
} }
dialog memowarn {
  title "Warning!"
  size -1 -1 240 80
  text "", 01, 10 10 220 50
  button "Close", 02, 60 55 120 22, ok
}
on *:dialog:memowarn:init:0:{
  did -o memowarn 01 1 %memowarn
}
;server list report
alias power say $power2
alias power2 {
  var %i = $scon(0), %@chans, %power, %normalppl, %totalchans
  while (%i) {
    scid $scon(%i)
    var %x = $chan(0)
    inc %totalchans $chan(0)
    while (%x) { 
      if ($me isop $chan(%x)) { 
        inc %@chans 
        inc %power $nick($chan(%x),0) 
      }
      inc %normalppl $nick($chan(%x),0)
      dec %x
    }
    dec %i
  }
  return Of $scon(0) networks, I'm opped in $+($iif(%@chans,$ifmatch,0) of %totalchans) channels and moderrate $+($iif(%power,%power,0) of $iif(%normalppl,$ifmatch,0)) users 
}
;Server tools
;Server list and switch 
;Written by Redneck
alias switch {
  if (!$1) {
    echo -catblfn Other * Active servers $+($chr(40),$scon(0),$chr(41)):
    var %cnt = 0
    while (%cnt < $scon(0)) {
      inc %cnt
      echo -catblfn Other * %cnt $+ . $scon(%cnt).server
    }
    return
  }
  elseif ($1 isnum) {
    var %switchs = $1
  }
  else {
    var %switchs = $null
    var %cnt = 0
    while (%cnt < $scon(0)) {
      inc %cnt
      if ($scon(%cnt).server == $1) var %switchs = %cnt
    } 
  }
  if ((%switchs == $null) || (%switchs > $scon(0))) /echo -castblfn Other * You are not connected to server $1
  else {
    if (!$2) /scon %switchs .window -a $!chan(1)
    elseif ($2 isnum) {
      var %chan = $2
      /scon %switchs .window -a $!chan(%chan)
    }
    else /scon %switchs .window -a $2
  }
  return
  :error
  echo -catblfn Other * /Switch: $error
}

alias serverlist {
  if ($1 == $null) {
    window -alvk0 @ServerList
    clear @Serverlist
    renwin @Serverlist @Serverlist Active servers $+($chr(40),$scon(0),$chr(41)):
  }
  var %cnt = 0
  var %tmr = 100
  while (%cnt < $scon(0)) {
    inc %cnt
    if ($scon(%cnt).server == $null) continue
    if ($1 == -s) echo -cstblfm Other * $scon(%cnt).server
    elseif (($1 == -a) && ($active != @ServerList)) { 
      inc %tmr 200 
      .timer -m 1 %tmr msg $active * $scon(%cnt).server 
    }
    elseif (($1 == -a) && ($active == @ServerList)) { 
      inc %tmr 200 
    .timer -m 1 %tmr msg $lactive $scon(%cnt).server }
    else aline 12 @Serverlist $scon(%cnt).server
    var %x = 0
    while (%x < $scon(%cnt).chans) {
      inc %x
      if ($1 == -s) echo -cstblfn Other * $gettok(%listchans,%x,44)
      elseif (($1 == -a) && ($active != @ServerList)) { 
        inc %tmr 200 
        .timer -m 1 %tmr msg $active * $gettok(%listchans,%x,44) 
      }
      elseif (($1 == -a) && ($active == @ServerList)) { 
        inc %tmr 200 
        .timer -m 1 %tmr msg $lactive * $gettok(%listchans,%x,44) 
      }
      else aline 4 @Serverlist $chr(0160) $gettok(%listchans,%x,44)
    }
  }
  unset %listchans 
}
alias -l chans {
  var %x = 0
  var %y = $chan(0)
  set %listchans $null 
  while (%x < %y) {
    inc %x
    set %listchans $addtok(%listchans,$chan(%x),44) 
  }
  return %y
}
menu @Serverlist {
  dclick:{
    if ($numtok($line($active,$1),32) > 1) { /switch $serv($1) $gettok($line($active,$1),2,32) | /close -@ @Serverlist }
    elseif ($line($active,$1)) { /switch $line($active,$1) 1 | /close -@ @Serverlist }
    else echo -castblfn other * /ServerList: No Selection
  }
  .Refresh list:/serverlist
  .Report in Status window:/serverlist -s
  .Print in channel:/serverlist -a
  .Switch to $iif($2,$2,$1):{
    if ($2) { /switch $serv($sline($active,1).ln) $2 | /close -@ @Serverlist }
    elseif ($1) { /switch $1 1 | /close -@ @Serverlist }
    else echo -castblfn Other * /ServerList: No Selection
  }
  .Close $iif($2,$2,$1):{
    if ($2) { /scon $servid($serv($sline($active,1).ln)) part $2 | serverlist }
    elseif ($1) { /scon $servid($serv($sline($active,1).ln)) quit | serverlist }
    else echo -castblfn Other * /ServerList: No Selection
  }
}
;
alias -l servid {
  var %switchs = $null
  var %cnt = 0
  while (%cnt < $scon(0)) {
    inc %cnt
    if ($scon(%cnt).server == $1) var %switchs = %cnt
  }
  return %switchs
}
;
alias -l serv {
  var %x = $1
  while (%x > 0) {
    if ($numtok($line($active,%x),32) == 1) { 
      var %serv $line($active,$int(%x)) 
      break 
    }
    dec %x
  }
  return %serv
}
;/global /Who dialog
alias -l GlobalWho hAdd -m General GlobalWHO 1
alias global {
  GlobalWho 
  .who $1-
}
dialog DialogWho {
  title "Global /Who..."
  size 200 200 300 350
  text "Current users", 1, 10 5 140 25
  list 2, 5 20 175 245, vsbar sort
  box "" , 3, 185 15 90 230
  button "Open query", 4, 190 25 80 30
  button "Send page", 5, 190 60 80 30
  button "Invite into active channel", 6, 190 95 80 30, multi
  button "Speech Status?", 7, 190 130 80 30
  button "Whois", 8, 190 165 80 30
  button "Close", 9, 190 200 80 30
  button "Show bots", 10, 10 265 80 30
  button "Show IRCops", 11, 95 265 80 30
  button "Show users", 12, 10 300 80 30
  button "Show all", 13, 95 300 80 30
  box "", 14, 5 254 175 80
}
on 1:dialog:dialogWho:sclick:4:{
  var %selected = $did(dialogWho,2).seltext
  if (%selected == $null) {  }
  else {
    query %selected
    dialog -x dialogWho
  }
}
on 1:dialog:dialogWho:sclick:10:{
  GlobalWho
  did -r dialogwho 2
  who +m B
}
on 1:dialog:dialogWho:sclick:11:{
  GlobalWho
  did -r dialogwho 2
  who +m oOCAaNH
}
on 1:dialog:dialogWho:sclick:12:{
  GlobalWho
  did -r dialogwho 2
  who -m oOCAaNBH
}
on 1:dialog:dialogWho:sclick:13:{
  GlobalWho
  did -r dialogwho 2
  who
}
on 1:dialog:dialogWho:sclick:5:{
  var %selected = $did(dialogWho,2).seltext
  if (%selected == $null) { }
  else {
    var %message = $?="Please enter the message you want to page"
    if (%message == $null) { }
    else .CTCP %selected PAGE %message
} }
on 1:dialog:dialogWho:sclick:6:{
  var %selected = $did(dialogWho,2).seltext
  if (%selected == $null) { }
  elseif ($left($active,1) != $chr(35)) { }
  else invite %selected $active 
}
;// Send SPEECH
on 1:dialog:dialogWho:sclick:7:{
  var %selected = $did(dialogWho,2).seltext
  if (%selected == $null) { }
  else .CTCP %selected SPEECH
}
;// Whois
on 1:dialog:dialogWho:sclick:8:{
  var %selected = $did(dialogWho,2).seltext
  if (%selected == $null) { }
  else whois %selected
}
;// Close
on 1:dialog:dialogWho:sclick:9: dialog -x dialogWho
raw 315:*:     hDel General GlobalWho
raw 352:*: { 
  if ($hGet(General,GlobalWHO)) {
    if (!$dialog(dialogWho)) dialog -m dialogWho dialogWho
    did -a dialogWho 2 $6
    GlobalWho
  } 
}
;End of tools.mrc
